package com.media.vidplayer2.mond.zh_friendlist;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class ModelFriendlist {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    private  Boolean group;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    private Boolean block;

    public Date getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(Date time_stamp) {
        this.time_stamp = time_stamp;
    }

    @ServerTimestamp
    private Date time_stamp;

    public Date getIsawmessage() {
        return isawmessage;
    }

    public void setIsawmessage(Date isawmessage) {
        this.isawmessage = isawmessage;
    }

    @ServerTimestamp
    private Date isawmessage;

    public Date getTime_stampN() {
        return time_stampN;
    }

    public void setTime_stampN(Date time_stampN) {
        this.time_stampN = time_stampN;
    }

    @ServerTimestamp
    private Date time_stampN;



    public ModelFriendlist() {
        //empty constructor needed
    }


}
