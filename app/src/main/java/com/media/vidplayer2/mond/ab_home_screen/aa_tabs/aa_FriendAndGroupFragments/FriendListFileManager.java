package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.ListenerRegistration;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.ArrayList;

public class FriendListFileManager {

    public CollectionReference notebookRef=null;
    public FriendListAdapter                                 adapter=null;
    public RecyclerView recyclerView=null;
    public ArrayList<ListenerRegistration> listenersUsers=null;
    public HomeActivity homeActivity=null;

     public boolean searchfriends= false;

    public LinearLayout linearLayoutfavsearchmain =null;




    public FriendListFileManager(Context context  ) {
        this.homeActivity = (HomeActivity)context;
        listenersUsers = new ArrayList<ListenerRegistration>();

    }




    public void setUpVIEW(View rootView ){
        recyclerView = rootView.findViewById(R.id.recycler_viewF);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(homeActivity);
        recyclerView.setLayoutManager(layoutManager);
        if(Config.DUPLI_TEST)
            Log.d("_ATAB_SEL_A", "2 ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

    }

    public void stopFlist()
    {

        if(FirebaseAuth.getInstance().getCurrentUser()!=null) {

            if(adapter!=null)
            {
                adapter.stopListening();
                for (int i = 0; i < homeActivity.friendListFileManager.listenersUsers.size(); i++)
                    if (homeActivity.friendListFileManager.listenersUsers.get(i) != null)
                        homeActivity.friendListFileManager.listenersUsers.get(i).remove();


                homeActivity.friendListFileManager.listenersUsers.clear();

            }


        }

    }

}
