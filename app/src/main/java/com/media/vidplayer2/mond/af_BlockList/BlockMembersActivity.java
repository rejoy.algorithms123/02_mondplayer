package com.media.vidplayer2.mond.af_BlockList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.media.vidplayer2.mond.R;

import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.ArrayList;

public class BlockMembersActivity extends AppCompatActivity {


    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference FriendsList=null;
    public BlockListAdapter adapter;
    RecyclerView recyclerView;
    ListenerRegistration GroupMemberstatus_CurrentList;
    private FirebaseFirestore mFirestore;
    public String GroupName;
    public String CreaterId;


    public String other_user_id;
    public String current_id;
    public String other_user_name;

    public String current_name ;
    public String current_image ;
    public String other_user_image;


    public String current_email;
    public String other_user_email;

    public boolean ami_admin;

    public  boolean ami_creator;



    BlockListFileManager groupListFileManager=null;


    public ArrayList<ListenerRegistration> listenersUsers;
    public ArrayList<ListenerRegistration> listenersNewMessage;
    public ArrayList<Task<QuerySnapshot>> listoftaskschataccess;



    @Override
    public void onStart() {
        super.onStart();


    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.al_b_activity_groupmemb_list);

        listenersUsers = new ArrayList<ListenerRegistration>();
        listenersNewMessage = new ArrayList<ListenerRegistration>();
        listoftaskschataccess = new ArrayList<Task<QuerySnapshot>>();


        groupListFileManager = new BlockListFileManager(this);
        groupListFileManager.searchfriends =false;

        // ((HomeActivity)getActivity()).friendListFileManager.setUpVIEW(rootView);

        //((HomeActivity)getActivity()).friendListFileManager.setUpRecyclerView();

        recyclerView =  findViewById(R.id.recycler_viewG);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager( this);
        recyclerView.setLayoutManager(layoutManager);













        if (Config.DUPLI_TEST) {
            Log.d("GROUP_MEMB_STATUS", GroupName+"::"+ CreaterId+"::"+current_id+"::"+current_name+"::"+current_image+"::"+current_email);

        }



        // setUpVIEW(rootView);

        setUpRecyclerView();




        //setUpRecyclerView();




        EditText edtSearchText = (EditText) findViewById(R.id.edt_search_textGcase);







        /*search btn clicked*/
        edtSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return false;
            }
        });

        /*hide/show clear button in search view*/



        TextWatcher searchViewTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



                if(s.toString().trim().length()==0){
                    //  ivClearText.setVisibility(View.GONE);
                } else {
                    //ivClearText.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {



                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if(Config.DUPLI_TEST)
                    Log.d("SEARCH_WISE", "onTextChanged: "+s);


                if(s.toString().equals(""))
                {
                    groupListFileManager.searchfriends=false;
                    if(Config.DUPLI_TEST)
                        Log.d("SEARCH_WISEG", "onTextChanged 1 : "+s);


                    // com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_firstTime(homeActivity,homeActivity);

                }
                else
                {

                    adapter.searchText =s.toString();
                    groupListFileManager.searchfriends=true;
                    if(Config.DUPLI_TEST)
                        Log.d("YES_CASE", "onTextChanged 2: "+s+"::"+ groupListFileManager.searchfriends);

                    //  com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_firstTimeSearch(homeActivity,homeActivity,s.toString());

                }




                adapter.notifyDataSetChanged();




                //homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_updateUIRecyclerK();


            }
        };

        edtSearchText.addTextChangedListener(searchViewTextWatcher);











    }


    public  void setUpRecyclerView() {
        //Query query = FriendsList.orderBy("priority", Query.Direction.DESCENDING);


        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
            FriendsList = db.collection("GroupLists").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection("Group_User_Freeze_List");

            Query query = FriendsList.orderBy("name", Query.Direction.DESCENDING);
            FirestoreRecyclerOptions<ModelBlocklist> options = new FirestoreRecyclerOptions.Builder<ModelBlocklist>()
                    .setQuery(query, ModelBlocklist.class)
                    .build();

            adapter = new BlockListAdapter(options, this);

            recyclerView.setAdapter(adapter);
            adapter.startListening();

        }





    }





}
