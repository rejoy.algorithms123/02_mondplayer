package com.media.vidplayer2.mond.ad_b_group_Profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.media.vidplayer2.mond.R;

import java.util.HashMap;
import java.util.Map;

public class GroupstatusActvity extends AppCompatActivity {

    private Toolbar mToolbar;

    private TextInputLayout mStatus;
    private Button mSavebtn;


    String GroupName;
    String CreaterId;


    String other_user_id;
    String current_id;
    String other_user_name;

    String current_name ;
    String current_image ;
    String other_user_image;


    public String current_email;
    public String other_user_email;

    public boolean ami_admin;

    public  boolean ami_creator;





    private FirebaseUser mCurrentUser;


    //Progress
    private ProgressDialog mProgress;


    private FirebaseFirestore mFirestore;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aj_c_activity_status_actvity);




        GroupName  = getIntent().getStringExtra("GroupName");
        CreaterId  = getIntent().getStringExtra("CreaterId");



        current_id = getIntent().getStringExtra("current_id");
        current_name = getIntent().getStringExtra("current_name");
        current_image = getIntent().getStringExtra("current_image");
        current_email = getIntent().getStringExtra("current_email");


        ami_admin = getIntent().getBooleanExtra("ami_admin",false);

        ami_creator = getIntent().getBooleanExtra("ami_creator",false);


        //Firebase
        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        String current_uid = mCurrentUser.getUid();






        mToolbar = (Toolbar) findViewById(R.id.status_appBar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Account Status");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();


        String status_value = getIntent().getStringExtra("status_value");

        mStatus = (TextInputLayout) findViewById(R.id.status_input);
        mSavebtn = (Button) findViewById(R.id.status_save_btn);

        mStatus.getEditText().setText(status_value);

        mSavebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Progress
                mProgress = new ProgressDialog(GroupstatusActvity.this);
                mProgress.setTitle("Saving Changes");
                mProgress.setMessage("Please wait while we save the changes");
                mProgress.show();

                String status = mStatus.getEditText().getText().toString();



                String current_id = mAuth.getCurrentUser().getUid();

                Map<String, Object> tokenMap = new HashMap<>();
                tokenMap.put("status", status);

                mFirestore.collection("GroupLists").document(CreaterId).collection("GroupNames").document(GroupName).update(tokenMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mProgress.dismiss();
                        onBackPressed();

                    }
                });




            }
        });










    }
}
