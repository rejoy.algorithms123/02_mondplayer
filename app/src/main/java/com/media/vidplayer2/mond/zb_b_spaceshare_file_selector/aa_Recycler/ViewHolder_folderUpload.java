package com.media.vidplayer2.mond.zb_b_spaceshare_file_selector.aa_Recycler;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.api.services.drive.model.File;
import com.media.vidplayer2.mond.R;


public class ViewHolder_folderUpload extends RecyclerView.ViewHolder {


    public TextView file_name;
    public ImageView thumbnail;
    public LinearLayout slice;
    public Boolean slected=false;



    public ViewHolder_folderUpload(@NonNull View itemView)
    {
        super(itemView);

        file_name = (TextView)itemView.findViewById(R.id.file_name_f_drivelist);
        thumbnail = itemView.findViewById(R.id.video_thumb_drivelist);
        slice  = itemView.findViewById(R.id.folderSlice_drivelist);
      //  file_name.setText("lolo");
    }
}
