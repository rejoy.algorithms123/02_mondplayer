package com.media.vidplayer2.mond.ae_GroupMembers.ab_bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ad_b_group_Profile.MyGroupProfileActivity;
import com.media.vidplayer2.mond.ae_GroupMembers.GroupMembersActivity;
import com.media.vidplayer2.mond.ae_GroupMembers.ModelGrouplist;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_b_chat_group.FireStoreChat.FirestoreGroupActivity;

import java.util.ArrayList;
import java.util.HashMap;

import es.dmoral.toasty.Toasty;

public class BottomSheetG extends BottomSheetDialog {

    private GroupMembersActivity groupMembersActivity;


    BottomSheetG bottomSheetG;
    int TYPE=0;
    int ADMIN_TYPE=0;
    int BLOCK_TYPE=0;

    ModelGrouplist memberdata=null;


    //((HomeActivity)context)


    public BottomSheetG(@NonNull GroupMembersActivity groupMembersActivity, ModelGrouplist memberdata) {
        super(groupMembersActivity);
        this.groupMembersActivity =groupMembersActivity;
        this.memberdata=memberdata;

        bottomSheetG =this;


    }

    public BottomSheetG(@NonNull Context context, int theme) {
        super(context, theme);
    }

    protected BottomSheetG(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = getLayoutInflater().inflate(R.layout.ag_bottom_sheet, null);
        setContentView(view);

        ArrayList<Item> items=new ArrayList<>();


        items.add( new Item(R.drawable.aa_crown, "View Profile") );

        if(memberdata.getCreator())
        {

            TYPE=0;


        }
        else
        {


            if( groupMembersActivity.ami_admin)
            {


                if(!memberdata.getAdmin() || groupMembersActivity.ami_creator || ( groupMembersActivity.current_id.equals(memberdata.getId())) )
                {




                    if(!memberdata.getAdmin())
                    {
                        ADMIN_TYPE=1;
                        items.add( new Item(R.drawable.aa_crown, "Add  Admin Privilege") );

                    }
                    else
                    {
                        ADMIN_TYPE=2;
                        items.add( new Item(R.drawable.aa_crown, " Remove Admin Privilege") );

                    }





                    if(!memberdata.isBlocked())
                    {


                        BLOCK_TYPE=1;
                        items.add( new Item(R.drawable.aa_crown, "Block  User") );
                    }
                    else
                    {
                        BLOCK_TYPE=2;
                        items.add( new Item(R.drawable.aa_crown, "Unblock User") );
                    }





                }



            }












        }







        ItemAdapter adapter = new ItemAdapter( groupMembersActivity, items );

        //ListView for the items
       ListView listView = (ListView) view.findViewById(R.id.list_items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               // Intent it = null;

                switch(position){




                    case 0:
                        bottomSheetG.dismiss();


                                 SendtoSettingsActivity();



                        break;

                    case 1:
                        bottomSheetG.dismiss();

                        switch (ADMIN_TYPE){
                            case 1:
                                AdminPrivilageUpgradtion(true);
                                break;

                            case 2:
                                AdminPrivilageUpgradtion(false);
                                break;
                        }

                        break;
                    case 2:

                        bottomSheetG.dismiss();

                        switch (BLOCK_TYPE){
                            case 1:
                                BlockManagement(true);
                                break;

                            case 2:
                                BlockManagement(false);
                                break;
                        }


                        break;
                    case 6:

                        break;
                }

              //  context.startActivity(it);
            }
        });



    }

    private void BlockManagement(boolean b) {


        ////////////////////////////////////////////////////////
        HashMap<String, Object> groupmembdata = new HashMap<>();

        groupmembdata.put("blocked",  b);

        WriteBatch batch =  FirebaseFirestore.getInstance().batch();


        DocumentReference GroupAdminNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(memberdata.getCreatorid()).collection("GroupMembList-"+groupMembersActivity.GroupName).document(memberdata.getId());
        batch.update(GroupAdminNamesReference, groupmembdata);



        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {

                if(b)
                Toasty.error(groupMembersActivity, "User is blocked" ).show();
                else
                    Toasty.success(groupMembersActivity, "User is unblocked" ).show();






            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



            }
        });

    }



    private void AdminPrivilageUpgradtion(boolean b) {

      //  Toasty.success(blockMembersActivity, "1" ).show();

        ////////////////////////////////////////////////////////
        HashMap<String, Object> groupmembdata = new HashMap<>();

        groupmembdata.put("admin",  b);

        WriteBatch batch =  FirebaseFirestore.getInstance().batch();


        DocumentReference GroupAdminNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(memberdata.getCreatorid()).collection("GroupMembList-"+groupMembersActivity.GroupName).document(memberdata.getId());
        batch.update(GroupAdminNamesReference, groupmembdata);



        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {

                if(b)
                    Toasty.error(groupMembersActivity, "User is changed to admin" ).show();
                else
                    Toasty.success(groupMembersActivity, "User admin power removed" ).show();






            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



            }
        });

    }


    public void SendtoSettingsActivity(){

        Intent intent = new Intent(groupMembersActivity, MyGroupProfileActivity.class);

        intent.putExtra("current_id", memberdata.getId());
        intent.putExtra("current_name", memberdata.getName());
        intent.putExtra("current_image", "null");
        intent.putExtra("current_email", "null");

        intent.putExtra("GroupName", groupMembersActivity.GroupName);
        intent.putExtra("CreaterId", memberdata.getCreatorid());

        intent.putExtra("ami_admin", memberdata.getAdmin());
        intent.putExtra("ami_creator", memberdata.getCreator());


        if(memberdata.getId().equals(groupMembersActivity.current_id))

            intent.putExtra("otherprofile", false);
        else
            intent.putExtra("otherprofile", true);
        groupMembersActivity.startActivity(intent);



    }

}
