package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.ab_bottomsheet;
import android.os.Parcel;
import android.os.Parcelable;

import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;


public class paracelable_Folder_VideoPlayer implements Parcelable {

    public paracelable_Folder_VideoPlayer( int currentIndex) {

        CurrentIndex = currentIndex;

    }



    public  int CurrentIndex=0;


    protected paracelable_Folder_VideoPlayer(Parcel in) {


        CurrentIndex = in.readInt();
    }

    public static final Creator<paracelable_Folder_VideoPlayer> CREATOR = new Creator<paracelable_Folder_VideoPlayer>() {
        @Override
        public paracelable_Folder_VideoPlayer createFromParcel(Parcel in) {
            return new paracelable_Folder_VideoPlayer(in);
        }

        @Override
        public paracelable_Folder_VideoPlayer[] newArray(int size) {
            return new paracelable_Folder_VideoPlayer[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(CurrentIndex);
    }

}
