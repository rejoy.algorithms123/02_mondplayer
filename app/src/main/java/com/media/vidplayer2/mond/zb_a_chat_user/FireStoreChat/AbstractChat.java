package com.media.vidplayer2.mond.zb_a_chat_user.FireStoreChat;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Date;

/**
 * Common interface for chat messages, helps share code between RTDB and Firestore examples.
 */
public abstract class AbstractChat {

    @Nullable
    public abstract Date getTimestamp();

    public abstract void setTimestamp(@Nullable Date timestamp);



    @Nullable
    public abstract String getTimeStampStr();

    public abstract void setTimeStampStr(String timeStampStr) ;


    @Nullable
    public abstract String getName();

    public abstract void setName(@Nullable String name);

    @Nullable
    public abstract String getMessage();

    public abstract void setMessage(@Nullable String message);

    @NonNull
    public abstract String getUid();

    public abstract void setUid(@NonNull String uid);

    @Override
    public abstract boolean equals(@Nullable Object obj);

    @Override
    public abstract int hashCode();

    @Nullable
    public abstract String getMimage_chat() ;

    public abstract void setMimage_chat(String mimage_chat) ;


    @Nullable
    public abstract long getFileSize() ;

    public abstract void setFileSize(long fileSize) ;


    @Nullable
    public abstract String getFilename() ;

    public abstract void setFilename(String filename) ;



}
