package com.media.vidplayer2.mond.za_global;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;


import com.google.android.exoplayer2.BuildConfig;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.database.DatabaseProvider;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.offline.ActionFileUpgradeUtil;
import com.google.android.exoplayer2.offline.DefaultDownloadIndex;
import com.google.android.exoplayer2.offline.DefaultDownloaderFactory;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.media.vidplayer2.mond.AppSignatureHelper;
import com.media.vidplayer2.mond.aa_MainActivity.MainActivity_Start;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.zi_Play.Player.DownloadTracker;
import com.media.vidplayer2.mond.zj_Queue.aa_Recycler.BooVariable;
import com.media.vidplayer2.mond.zj_Queue.aa_Recycler.DirectoryFav;
import com.squareup.leakcanary.AndroidExcludedRefs;
import com.squareup.leakcanary.ExcludedRefs;
import com.squareup.leakcanary.LeakCanary;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


public class DemoApplication extends Application   implements LifecycleObserver
{







    @Override
    public void onCreate()
    {
        super.onCreate();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        AppSignatureHelper appSignatureHelper = new AppSignatureHelper(this);
     // Config.SIGNATURE_HASH_TOKEN = appSignatureHelper.getAppSignatures().get(0);

        Config.SIGNATURE_HASH_TOKEN = appSignatureHelper.getAppSignatures().get(0);

        if(Config.DUPLI_TEST)
            Log.d("SMS_TEST", "App Restarted");



        if (LeakCanary.isInAnalyzerProcess(this))
        {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }

        ExcludedRefs excludedRefs = AndroidExcludedRefs.createAppDefaults()
                .instanceField("android.view.inputmethod.InputMethodManager", "sInstance")
                .instanceField("android.view.inputmethod.InputMethodManager", "mLastSrvView")
                .instanceField("com.android.internal.policy.PhoneWindow$DecorView", "mContext")
                .instanceField("android.support.v7.widget.SearchView$SearchAutoComplete", "mContext")
                .build();



         LeakCanary.install(this);


        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);

        Config.directories = null;
        //Config.directories.clear();
        Config.token = 0;
        Config.directories =  new ArrayList< DirectoryFav>();

        Config.directories.clear();

        Config.bv = new BooVariable();


        //////////////////////////////////

        userAgent = Util.getUserAgent(this, "ExoPlayerDemo");

      



    }


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {

        if(Config.TEST)
        Log.d("MyApp", "App in background from Appication");

        Thread_KilerCommon();
        VideoFileParser.THREAD_STOPPER=false;
        MainActivity_Start.mainActivityStartS =null;
        HomeActivity.homeActivityS=null;
        LoginActivity1.loginActivity1S =null;
        Config.TIME_START = 0;


        set_online_status(false);



    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded()
    {
        if(Config.TEST)
        Log.d("MyApp", "App in foreground from Appication");
        set_online_status(true);

    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private void onAppDestroy()
    {
        if(Config.TEST)
            Log.d("MyAppD", "Destroyed");

    }


    public void Thread_KilerCommon()
    {




        if(Config.TEST)
            Log.d("_#_TT_TEST", " Inside T test");





         ///Threads used in MainActivity_Start
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if(MainActivity_Start.mainActivityStartS !=null)
        {
            //Thread for making random delay at first time running
            if(MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread!=null)
            {

                if(MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread.isAlive())
                {
                    VideoFileParser.THREAD_STOPPER=true;
                }
                while (MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread.isAlive());

            }
        }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////





        if(VideoFileParser.getInstance().threadForStorageReadDirectoryI !=null)
        {

            if(VideoFileParser.getInstance().threadForStorageReadDirectoryI.isAlive())
            {
                VideoFileParser.THREAD_STOPPER=true;
            }



            while (VideoFileParser.getInstance().threadForStorageReadDirectoryI.isAlive())
            {

                // if(Config.TEST)
                //    Log.d("_#_TT_TEST", " threadForStorageReadDirectoryI"+firstTimeThread.isAlive()+"::"+THREAD_STOPPER);
            }


        }


        if(VideoFileParser.getInstance().threadForStorageReadDirectoryE !=null)
        {

            if(VideoFileParser.getInstance().threadForStorageReadDirectoryE.isAlive())
            {
                VideoFileParser.THREAD_STOPPER=true;
            }



            while (VideoFileParser.getInstance().threadForStorageReadDirectoryE.isAlive())
            {
            }


        }




////////////////////////////////////// Home  activity thread /////////////////////////////////////

 //Fav frag thread






     if(HomeActivity.homeActivityS!=null)
     {

         if(HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir !=null)
         {

             if(HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir.isAlive())
             {
                 VideoFileParser.THREAD_STOPPER=true;
             }



             while (HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir.isAlive());



         }

         HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir =null;
     }




        if(MainActivity_Start.mainActivityStartS !=null)
        MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread=null;



        VideoFileParser.THREAD_STOPPER=false;
        VideoFileParser.getInstance().threadForStorageReadDirectoryI =null;
        VideoFileParser.getInstance().threadForStorageReadDirectoryE =null;





    }













    void set_online_status(Boolean online)
    {


        if(  FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
            String current_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

            HashMap<String, Object> UserStat = new HashMap<>();



            if(online){

                UserStat.put("online", true);

            }
            else
            {
                UserStat.put("online", false);

            }

            UserStat.put("time_stamp",  FieldValue.serverTimestamp());


            FirebaseFirestore.getInstance().collection("UsersList").document(current_id).update(UserStat).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {



                }
            });

        }




    }

////////////////////////////////////////////////////////////////////////////////////// DemoApplication


    private static final String TAG = "DemoApplication";
    private static final String DOWNLOAD_ACTION_FILE = "actions";
    private static final String DOWNLOAD_TRACKER_ACTION_FILE = "tracked_actions";
    private static final String DOWNLOAD_CONTENT_DIRECTORY = "downloads";

    protected String userAgent;

    private DatabaseProvider databaseProvider;
    private File downloadDirectory;
    private Cache downloadCache;
    private DownloadManager downloadManager;
    private DownloadTracker downloadTracker;



    /** Returns a {@link DataSource.Factory}. */
    public DataSource.Factory buildDataSourceFactory()
    {
        DefaultDataSourceFactory upstreamFactory =
                new DefaultDataSourceFactory(this, buildHttpDataSourceFactory());
        return buildReadOnlyCacheDataSource(upstreamFactory, getDownloadCache());
    }

    /** Returns a {@link HttpDataSource.Factory}. */
    public HttpDataSource.Factory buildHttpDataSourceFactory() {
        return new DefaultHttpDataSourceFactory(userAgent);
    }

    /** Returns whether extension renderers should be used. */
    public boolean useExtensionRenderers() {
        return "withExtensions".equals(BuildConfig.FLAVOR);
    }

    public RenderersFactory buildRenderersFactory(boolean preferExtensionRenderer) {
        @DefaultRenderersFactory.ExtensionRendererMode
        int extensionRendererMode =
                useExtensionRenderers()
                        ? (preferExtensionRenderer
                        ? DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON)
                        : DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF;
        return new DefaultRenderersFactory(/* context= */ this)
                .setExtensionRendererMode(extensionRendererMode);
    }

    public DownloadManager getDownloadManager() {
        initDownloadManager();
        return downloadManager;
    }

    public DownloadTracker getDownloadTracker() {
        initDownloadManager();
        return downloadTracker;
    }

    protected synchronized Cache getDownloadCache() {
        if (downloadCache == null) {
            File downloadContentDirectory = new File(getDownloadDirectory(), DOWNLOAD_CONTENT_DIRECTORY);
            downloadCache =
                    new SimpleCache(downloadContentDirectory, new NoOpCacheEvictor(), getDatabaseProvider());
        }
        return downloadCache;
    }

    private synchronized void initDownloadManager() {
        if (downloadManager == null) {
            DefaultDownloadIndex downloadIndex = new DefaultDownloadIndex(getDatabaseProvider());
            upgradeActionFile(
                    DOWNLOAD_ACTION_FILE, downloadIndex, /* addNewDownloadsAsCompleted= */ false);
            upgradeActionFile(
                    DOWNLOAD_TRACKER_ACTION_FILE, downloadIndex, /* addNewDownloadsAsCompleted= */ true);
            DownloaderConstructorHelper downloaderConstructorHelper =
                    new DownloaderConstructorHelper(getDownloadCache(), buildHttpDataSourceFactory());
            downloadManager =
                    new DownloadManager(
                            this, downloadIndex, new DefaultDownloaderFactory(downloaderConstructorHelper));
            downloadTracker =
                    new DownloadTracker(/* context= */ this, buildDataSourceFactory(), downloadManager);
        }
    }

    private void upgradeActionFile(
            String fileName, DefaultDownloadIndex downloadIndex, boolean addNewDownloadsAsCompleted) {
        try {
            ActionFileUpgradeUtil.upgradeAndDelete(
                    new File(getDownloadDirectory(), fileName),
                    /* downloadIdProvider= */ null,
                    downloadIndex,
                    /* deleteOnFailure= */ true,
                    addNewDownloadsAsCompleted);
        } catch (IOException e) {
            com.google.android.exoplayer2.util.Log.e(TAG, "Failed to upgrade action file: " + fileName, e);
        }
    }

    private DatabaseProvider getDatabaseProvider() {
        if (databaseProvider == null) {
            databaseProvider = new ExoDatabaseProvider(this);
        }
        return databaseProvider;
    }

    private File getDownloadDirectory() {
        if (downloadDirectory == null) {
            downloadDirectory = getExternalFilesDir(null);
            if (downloadDirectory == null) {
                downloadDirectory = getFilesDir();
            }
        }
        return downloadDirectory;
    }

    protected static CacheDataSourceFactory buildReadOnlyCacheDataSource(
            DataSource.Factory upstreamFactory, Cache cache) {
        return new CacheDataSourceFactory(
                cache,
                upstreamFactory,
                new FileDataSourceFactory(),
                /* cacheWriteDataSinkFactory= */ null,
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR,
                /* eventListener= */ null);
    }


}

