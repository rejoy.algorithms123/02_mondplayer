package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.RecyclerAdapter_folder;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;


/**
 * A simple {@link Fragment} subclass.
 */
public class FolderFragment extends Fragment {


    public static final String                          ARG_PAGE = "ARG_PAGE";
    private int                                                         mPage;
    HomeActivity                                           homeActivity =null;



    public FolderFragment() {
        // Required empty public constructor


    }






    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        homeActivity = ((HomeActivity) getActivity());

        if (Config.TEST)
        {
            Log.d("_AFRAG_FD_CRE_", "OnCreate  Folder fragment HomeActivity");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {


        View rootView = null;

        homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER =false;



        //MainActivity_Start.mainActivityStartS._fileManager_mainActivity.total_folder = new ArrayList<Directory_folder>();




        rootView = inflater.inflate(R.layout.ac_d_fragment_folder, container, false);




      //  MainActivity_Start.mainActivityStartS._fileManager_mainActivity.total_folder.clear();

        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView =  rootView.findViewById(R.id.contact_recycleView_folder);
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView.setHasFixedSize(true);
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerAdapter = new RecyclerAdapter_folder( VideoFileParser.getInstance().total_folder, getContext());


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView.setLayoutManager(linearLayoutManager);
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView.setAdapter( homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerAdapter);



        // SwipeRefreshLayout
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container_folder);
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        //   StartThread_dir_update(false);

        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView.addItemDecoration(new DividerItemDecoration(homeActivity, 0));
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()
            {
                // Your recyclerview reload logic function will be here!!!

                if (Config.TEST)
                {
                    Log.d("_AFRAG_FD_RFRSH", "onRefresh fav fragment  : homeActivity ");

                }

                homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout.setRefreshing(false);


            }
        });


        if (Config.TEST)
        {
            Log.d("_AFRAG_FD_VIEW_", " onCreateView folder fragment  : aa_activity_home_screen");

        }


        if (Config.TEST)
        {
            for (int x = 0; x <  VideoFileParser.getInstance().total_folder.size(); x++)
            {
                Directory_folder directory_folder = VideoFileParser.getInstance().total_folder.get(x);
                Log.d("_ZRECYCLER_CONTENT_F",directory_folder.file_path+":**:"+directory_folder.size+"::"+directory_folder.file_name);

            }

            Log.d("_ZRECYCLER_CONTENT_F", VideoFileParser.getInstance().total_folder.size()+"");


        }


        EditText edtSearchText = (EditText) rootView.findViewById(R.id.edt_search_textfld);

        homeActivity.fileManager_homeActivity.folderFileManager.linearLayoutfavsearchfld = (LinearLayout) rootView.findViewById(R.id.searchviewfld);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.iv_clear_textfld);

        imageView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                edtSearchText.setText("");
                homeActivity.fileManager_homeActivity.favFileManager.searchButton.setVisibility(View.VISIBLE);
                homeActivity.fileManager_homeActivity.folderFileManager.linearLayoutfavsearchfld.setVisibility(View.GONE);
            }
        });




        /*search btn clicked*/
        edtSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return false;
            }
        });

        /*hide/show clear button in search view*/



        TextWatcher searchViewTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



                if(s.toString().trim().length()==0){
                  //  ivClearText.setVisibility(View.GONE);
                } else {
                    //ivClearText.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {



                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if(Config.DUPLI_TEST)
                    Log.d("SEARCH_WISE", "onTextChanged: "+s);


                for (int x = 0; x < VideoFileParser.getInstance().total_folder.size()   ; x++)
                {
                    Directory_folder directory_folder = VideoFileParser.getInstance().total_folder.get(x);

                    directory_folder.hide = false;
                     String normal = directory_folder.file_name;
                    String lower = directory_folder.file_name.toLowerCase();
                    String upper = directory_folder.file_name.toUpperCase();

                    if(s.equals(""))
                    {
                        directory_folder.hide=false;
                    }
                    else
                    {
                        if(normal.contains(s.toString())|| lower.contains(s.toString()) || upper.contains(s.toString()))
                            directory_folder.hide =false;
                        else
                            directory_folder.hide=true;
                    }


                    if(Config.DUPLI_TEST)
                    Log.d("_AFRAG_FD_F",directory_folder.file_path+":**:"+directory_folder.size+"::"+directory_folder.file_name);

                }

                homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_updateUIRecyclerK();


            }
        };

        edtSearchText.addTextChangedListener(searchViewTextWatcher);



        return rootView;


    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {


        //Only once needed
        homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER =true;

        if(Config.TEST)
            Log.d("_AFRAG_FD_VIEW_CTED",  ":: "+homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER+" :: Flag frag created view" );
        //


    }




    @Override
    public void onStop() {

        if(Config.TEST)
        {
            Log.d("_AFRAG_FD_STP_", "Stop executed from frgament folder -- home activity");

        }


        super.onStop();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();



        homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER =false;

        if(Config.TEST)
        {
            Log.d("_AFRAG_FD_VIEW_DEST_",  ":: "+homeActivity.fileManager_homeActivity.folderFileManager.FOLDER_FRAG_READY_TO_UPDATE_FOLDER+" :: Flag frag view destroyed" );


        }
       // homeActivity.fileManager_homeActivity.Folder_Frag_recyclerView.setNestedScrollingEnabled(false);

        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView =null;
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerAdapter =null;
        homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_mSwipeRefreshLayout =null;




    }










}
