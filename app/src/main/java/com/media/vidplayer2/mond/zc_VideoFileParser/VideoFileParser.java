package com.media.vidplayer2.mond.zc_VideoFileParser;

import android.util.Log;

import com.greentoad.turtlebody.mediapicker.core.FileManager;
import com.greentoad.turtlebody.mediapicker.ui.component.folder.image_video.ImageVideoFolder;
import com.greentoad.turtlebody.mediapicker.ui.component.folder.image_video.ImageVideoFolderFragment;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler.DirectoryFav;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.zb_Config.Config;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import io.reactivex.Single;

import static android.os.Build.ID;

public class VideoFileParser {


    public List<DirectoryFav>                                                    names_fav_MEM =null;
    public List<DirectoryFav>                                                     names_fav_SD =null;
    public List<DirectoryFav>                                                     total_SD_MEM =null;
    public List<Directory_folder>                                                 total_folder =null;


    public  static Boolean                                                    video_flag_fav_1=false;
    public  static Integer                                                               count_fav_1;

    public  static Boolean                                                    video_flag_fav_2=false;
    public  static Integer                                                               count_fav_2;


    public static  Boolean                                                     THREAD_STOPPER =false;

    public ThreadForStorageReadDirectory threadForStorageReadDirectoryI =null, threadForStorageReadDirectoryE =null;

    private static VideoFileParser                                           single_instance = null;

    public VideoFileParser() {


        names_fav_MEM            = new ArrayList<DirectoryFav>();
        names_fav_SD             = new ArrayList<DirectoryFav>();
        total_SD_MEM             = new ArrayList<DirectoryFav>();
        total_folder             = new ArrayList<Directory_folder>();

    }

    public void MakeNewThreadsForIntenralAndExternalDirReading() {

        threadForStorageReadDirectoryI =new ThreadForStorageReadDirectory();
        threadForStorageReadDirectoryE = new ThreadForStorageReadDirectory();
    }







    // static method to create instance of Singleton class
    public static VideoFileParser getInstance()
    {
        if (single_instance == null)
            single_instance = new VideoFileParser();

        return single_instance;
    }


    private void getInbox()
    {
        DirectoryFav directoryFav = new DirectoryFav("Inbox","/Inbox","12");
        names_fav_MEM.add(directoryFav);
        directoryFav = new DirectoryFav("Private Folder","/Private Folder","14");
        names_fav_MEM.add(directoryFav);

    }


    private void  fetchImageVideoFolders1()
    {


        /*
        val bucketFetch: Single<ArrayList<ImageVideoFolder>> =
        if(mFileType==MediaPicker.MediaTypes.VIDEO)
            Single.fromCallable<ArrayList<ImageVideoFolder>> { FileManager.fetchVideoFolders(context!!) }
                else
        Single.fromCallable<ArrayList<ImageVideoFolder>> { FileManager.fetchImageFolders(context!!) }

        bucketFetch
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : SingleObserver<ArrayList<ImageVideoFolder>> {

            override fun onSubscribe(@NonNull d: Disposable) {
                frame_progress.visibility = View.VISIBLE
            }

            override fun onSuccess(@NonNull imageVideoFolders: ArrayList<ImageVideoFolder>) {
                mImageVideoFolderList = imageVideoFolders
                mImageVideoFolderAdapter.setData(mImageVideoFolderList)
                frame_progress.visibility = View.GONE
            }

            override fun onError(@NonNull e: Throwable) {
                frame_progress.visibility = View.GONE
                e.printStackTrace()
                info { "error: ${e.message}" }
            }
        })
        */

    }

    public void GenerateDirectortiesLib(File dir, String name) throws InterruptedException
    {


    }

    public void GenerateDirectorties(File dir, String name) throws InterruptedException
    {





        File[] directories=null;

        if(Config.TEST)
            Log.d("_#_DIR_NAME", name+":: File Path Printing:: A " +dir);

        if(name.equals("Thread_1"))
        {
            names_fav_MEM.clear();
            getInbox();
        }


        if(name.equals("Thread_2"))
            names_fav_SD.clear();

        Stack<File> stack = new Stack<File>();
        stack.push(dir);



        while(!stack.isEmpty())
        {
            File child = stack.pop();

            if(name.equals("Thread_1"))
            {
                video_flag_fav_1 =false;
                count_fav_1=0;
            }

            if(name.equals("Thread_2")) {

                video_flag_fav_2 =false;
                count_fav_2=0;

            }





            if(directories!=null)
            {
                directories=null;
            }

            directories = child.listFiles(new FileFilter()
            {



                @Override
                public boolean accept(File pathname)
                {


                    if(pathname.isDirectory())
                        return true;

                    if(Config.TEST){

                        //   Log.d("_#_DIR_NAME", "File Path Printing:: B " +pathname);

                    }



                    if (
                            pathname.getName().endsWith(".3gp")
                                    || pathname.getName().endsWith(".avi")
                                    || pathname.getName().endsWith(".flv")
                                    || pathname.getName().endsWith(".m4v")
                                    || pathname.getName().endsWith(".mkv")
                                    || pathname.getName().endsWith(".mov")

                                    || pathname.getName().endsWith(".mp4")
                                    || pathname.getName().endsWith(".mpeg")
                                    || pathname.getName().endsWith(".mpg")
                                    || pathname.getName().endsWith(".mts")
                                    || pathname.getName().endsWith(".vob")



                                    || pathname.getName().endsWith(".webm")
                                    || pathname.getName().endsWith(".wmv")


                    )



                    {


                        if(Config.TEST){

                            Log.d("_#_DIR_NAME", name+":: File Path Printing Video:: C " +pathname);

                        }

                        if(name.equals("Thread_1"))
                        {
                            video_flag_fav_1 =true;
                            count_fav_1++;
                        }


                        if(name.equals("Thread_2"))
                        {
                            video_flag_fav_2 =true;
                            count_fav_2++;
                        }

                    }




                    return  false ;
                }
            });


            if(video_flag_fav_1 && name.equals("Thread_1"))
            {
                String string_path = child.toString();
                String[] parts = string_path.split("/");



                if(parts[parts.length - 1].charAt(0) != '.')
                {
                    DirectoryFav directoryFav = new DirectoryFav(parts[parts.length - 1],string_path,count_fav_1.toString());
                    names_fav_MEM.add(directoryFav);

                    try{
                        // names_fav_MEM.add(directoryFav);

                    }
                    catch (Exception e)
                    {

                        if(Config.DUPLI_TEST)
                            Log.d("ANDROIDO", "GenerateDirectorties: ."+names_fav_MEM+"::");
                    }


                    if(parts[parts.length - 1].equals("MondInbox"))

                    {

                        Collections.swap(names_fav_MEM, 0, names_fav_MEM.size()-1);


                    }

                }




            }

            if(video_flag_fav_2 && name.equals("Thread_2"))
            {


                String string_path = child.toString();
                String[] parts = string_path.split("/");

                if(parts[parts.length - 1].charAt(0) != '.')
                {
                    DirectoryFav directoryFav = new DirectoryFav(parts[parts.length - 1],string_path,count_fav_2.toString());
                    names_fav_SD.add(directoryFav);
                    try{
                        //  names_fav_SD.add(directoryFav);

                    }
                    catch (Exception e)
                    {

                        if(Config.DUPLI_TEST)
                            Log.d("ANDROIDO", "GenerateDirectorties: ."+names_fav_SD+"::");
                    }

                }






            }


            if(Config.TIME_TEST && name.equals("Thread_1"))
            {



                try
                {
                    Thread.sleep(Config.TEST_DELY_IN_THREAD);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }

                Log.d("_AFRAG_TIME", "New Time : "+Config.TIME_START++);


            }


          if(directories!=null)
            for (int i = 0; (i < directories.length ) && !THREAD_STOPPER; i++)
            {

                stack.push(directories[i]);

            }

            if(THREAD_STOPPER)
            {
                stack.empty();
            }



        }











    }



    public  class ThreadForStorageReadDirectory extends Thread {

        File                                                                         dir;
        String                                                                      name;
        CyclicBarrier                                                            barrier;


        public void SetThreadValue(File dir, String name, CyclicBarrier barrier)
        {
            this.barrier=barrier;
            this.dir =dir;
            this.name=name;
        }


        @Override
        public void run()
        {

            try
            {
                GenerateDirectorties(dir,name );

            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            if(Config.TEST)
            {
                Log.d("_#__GET_VID_ROOT_DIR B", "Root Dir: "+dir.toString()+"_::_");
            }

            try {
                this.barrier.await();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread #" + ID + " passed the barrier.");

        }

    }

}
