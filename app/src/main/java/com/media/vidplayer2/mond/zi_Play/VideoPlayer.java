package com.media.vidplayer2.mond.zi_Play;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.media.vidplayer2.mond.R;

import android.content.Intent;
import android.net.Uri;

import android.util.Log;
import android.view.View;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.Util;

import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoPlayer extends AppCompatActivity
{
    String videoUri;
    int index;
    private boolean playWhenReady = true;
      ExoPlayer player=null;

    @BindView(R.id.player_view)
    PlayerView playerView;




    private void buildMediaSource_org(Uri mUri) {


        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);



        player.prepare(videoSource);
        player.setPlayWhenReady(true);

    }


    private void initializePlayer()
    {









        if (player == null)
        {



            TrackSelection.Factory factory = new AdaptiveTrackSelection.Factory();
            LoadControl loadControl = new DefaultLoadControl();

            DefaultRenderersFactory defaultRenderersFactory = new DefaultRenderersFactory(this);



            player = ExoPlayerFactory.newSimpleInstance(this,defaultRenderersFactory, new DefaultTrackSelector(factory), loadControl);




            playerView.setPlayer(player);
            player.setPlayWhenReady(playWhenReady);

            buildMediaSource_org(Uri.parse(videoUri));

        }










    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zz_testplayer);
        ButterKnife.bind(this);

        Intent intent = getIntent();


          index = intent.getIntExtra("index", 0);



          if(Config.DUPLI_TEST)
              Log.d("VIDEO_CURRENT", "onCreate 1: "+index);


        Directory_folder directory_folder =  VideoFileParser.getInstance().total_folder.get(index);
        // videoUri = paracelable_folder_videoPlayer.getText1();
        videoUri = directory_folder.getFile_path();






    }


    @OnClick({R.id.exo_next})
    public void onclick_next( View view){



        //    CURRENT_INDEX = (CURRENT_INDEX+1)%aa_activity_home_screen.mContext.fileManager_home_folder.directories_folder.size();

        index = (index+1)%VideoFileParser.getInstance().total_folder.size();



        if(Config.DUPLI_TEST)
            Log.d("VIDEO_NEXT", "onCreate 2: "+index+"::"+VideoFileParser.getInstance().total_folder.size());


        Directory_folder directory_folder =  VideoFileParser.getInstance().total_folder.get(index);

        videoUri = directory_folder.getFile_path();
        player.stop();
        player.release();
        player=null;
        initializePlayer();


    }


    @OnClick({R.id.exo_prev})
    public  void onclick_prev(View view)
    {



        if(index==0)
        {
            index= VideoFileParser.getInstance().total_folder.size()-1;
        }
        else

            index = (index-1)%VideoFileParser.getInstance().total_folder.size();

        if(Config.DUPLI_TEST)
            Log.d("VIDEO_PREV", "onCreate 3: "+index+"::"+VideoFileParser.getInstance().total_folder.size());



        Directory_folder directory_folder =  VideoFileParser.getInstance().total_folder.get(index);
        // videoUri = paracelable_folder_videoPlayer.getText1();
        videoUri = directory_folder.getFile_path();
        player.stop();
        player.release();
        player=null;
        initializePlayer();




    }






    @Override
    protected void onStop() {
        super.onStop();
        player.stop();
        player.release();
    }



    @Override
    protected void onResume() {
        super.onResume();
        initializePlayer();
    }
}