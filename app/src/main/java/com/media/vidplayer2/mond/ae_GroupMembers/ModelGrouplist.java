package com.media.vidplayer2.mond.ae_GroupMembers;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class ModelGrouplist {

    public String getCreatorid() {
        return creatorid;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    boolean blocked;

    public void setCreatorid(String creatorid) {
        this.creatorid = creatorid;
    }

    String creatorid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

 Boolean admin;
    Boolean creator;

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Boolean getCreator() {
        return creator;
    }

    public void setCreator(Boolean creator) {
        this.creator = creator;
    }

    String id;

    public ModelGrouplist( ) {

    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    Integer weight;


}
