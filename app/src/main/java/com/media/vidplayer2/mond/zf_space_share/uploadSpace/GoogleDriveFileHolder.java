package com.media.vidplayer2.mond.zf_space_share.uploadSpace;

import android.widget.ImageView;

import com.google.api.client.util.DateTime;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.About;
public class GoogleDriveFileHolder {

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    File file=null;


    public About getAbout() {
        return about;
    }

    public void setAbout(About about) {
        this.about = about;
    }

    About about=null;
}
