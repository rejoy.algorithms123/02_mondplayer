package com.media.vidplayer2.mond.zk_WifiDirect;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;

import static android.net.wifi.p2p.WifiP2pManager.WIFI_P2P_STATE_ENABLED;

public class WiFiDirectBroadcastReceiver   extends BroadcastReceiver {

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private MainActivity mActivity;

    public WiFiDirectBroadcastReceiver(WifiP2pManager mManager, WifiP2pManager.Channel mChannel, MainActivity mActivity)
    {
        this.mManager = mManager;
        this.mChannel = mChannel;
        this.mActivity = mActivity;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {

/*
        String action = intent.getAction();

        if(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action))
        {
            int state=intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE,-1);

            if(state== WIFI_P2P_STATE_ENABLED)
            {
                Toast.makeText(context,"Wifi is ON", Toast.LENGTH_SHORT).show();

                if(Config.DUPLI_TEST)
                    Log.d("WIFI_TEST", "onReceive: "+ "WIFI_P2P_STATE_ENABLED");
            }
            else
            {
                Toast.makeText(context,"Wifi is OFF",Toast.LENGTH_SHORT).show();
                if(Config.DUPLI_TEST)
                    Log.d("WIFI_TEST", "onReceive: "+ "NO WIFI_P2P_STATE_ENABLED");

            }
        }
        else if(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action))
        {
            if(Config.DUPLI_TEST)
                Log.d("WIFI_TEST", "onReceive: "+ "WIFI_P2P_PEERS_CHANGED_ACTION");

        }
        else if(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action))
        {

        }else if(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action))
        {
            //do something
        }
*/


        String action = intent.getAction();
        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action))
        {
            // Determine if Wifi P2P mode is enabled or not, alert
            // the Activity.
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED)
            {
               // mActivity.setIsWifiP2pEnabled(true);

                if(Config.DUPLI_TEST)
                    Log.d("WIFI_TEST", "onReceive: "+ "WIFI_P2P_STATE_ENABLED");
            }
            else
            {
              //  mActivity.setIsWifiP2pEnabled(false);

                if(Config.DUPLI_TEST)
                    Log.d("WIFI_TEST", "onReceive: "+ "NO WIFI_P2P_STATE_ENABLED");
            }
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {

            // The peer list has changed! We should probably do something about
            // that.

        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action))
        {

            // Connection state changed! We should probably do something about
            // that.

        }
        else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action))
        {

            /*
            DeviceListFragment fragment = (DeviceListFragment) mActivity.getFragmentManager()
                    .findFragmentById(R.id.frag_list);
            fragment.updateThisDevice((WifiP2pDevice) intent.getParcelableExtra(
                    WifiP2pManager.EXTRA_WIFI_P2P_DEVICE));

             */

        }
    }
}
