package com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.NewLogin;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.List;

public class FileManagerLoginAcitivity {
    LoginActivity loginActivity;




    public FileManagerLoginAcitivity(Context context) {
        this.loginActivity = (LoginActivity)context;
    }






    private void openSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(loginActivity);
        builder.setTitle("Required Permissions");
        builder.setMessage("This app require permission to use awesome feature. Grant them in app settings.");
        builder.setPositiveButton("Take Me To SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", loginActivity.getPackageName(), null);
                intent.setData(uri);
                loginActivity.startActivityForResult(intent, 101);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    public void requestMultiplePermissionsSMS(){



        Dexter.withActivity(loginActivity)
                .withPermissions(
                        // Manifest.permission.READ_CONTACTS,
                        //  Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.SEND_SMS,Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted())
                        {
                            Toast.makeText(loginActivity.getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();
                            if(Config.DUPLI_TEST)
                            {
                                Log.d("SMS_NUMBER", "Permission Granted");
                            }
                            loginActivity.loadingbutton.showLoading();


                            loginActivity.startSMSListener();


                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(loginActivity.getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();





    }

}
