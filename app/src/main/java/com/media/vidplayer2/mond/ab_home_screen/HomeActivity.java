package com.media.vidplayer2.mond.ab_home_screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.api.services.drive.Drive;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.aa_MainActivity.MainActivity_Start;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments.FriendListFileManager;
import com.media.vidplayer2.mond.za_global.DemoApplication;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.ze_LeakManager.IMMLeaks;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {



    private FirebaseUser                                  current_user=null;
    public FirebaseAuth                                          mAuth=null;
    private FirebaseFirestore                               mFirestore=null;
    public FileManager_HomeActivity          fileManager_homeActivity =null;
    public  MainActivity_Start                       mainActivityStart =null;
    public  HomeActivity                                  homeActivity =null;
    public  static HomeActivity                         homeActivityS =null;

    public Boolean start_list=true;


    public FriendListFileManager friendListFileManager =null;





    @OnClick(R.id.seacrhb)
    public void submit(View view)
    {
        // TODO submit data to server...
     int i=  viewPager.getCurrentItem();

     switch (i)
     {
         case 0:

             if(Config.DUPLI_TEST)
                 Log.d("SEARCH_TEST", "submit: "+0);
             fileManager_homeActivity.favFileManager.searchButton.setVisibility(View.GONE);

             fileManager_homeActivity.frdlistfileManager.linearLayoutfavsearchmain.setVisibility(View.VISIBLE);


             break;

         case 1:
             if(Config.DUPLI_TEST)
                 Log.d("SEARCH_TEST", "submit: "+1);


             fileManager_homeActivity.favFileManager.searchButton.setVisibility(View.GONE);

             fileManager_homeActivity.favFileManager.linearLayoutfavsearchmain.setVisibility(View.VISIBLE);
             break;
         case 2:
             if(Config.DUPLI_TEST)
                 Log.d("SEARCH_TEST", "submit: "+2);

             fileManager_homeActivity.favFileManager.searchButton.setVisibility(View.GONE);
             fileManager_homeActivity.folderFileManager.linearLayoutfavsearchfld.setVisibility(View.VISIBLE);
             break;

     }

    }

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.viewpager)
    public ViewPager viewPager;


    @BindView(R.id.tab_layout)
    public TabLayout tabLayout;






    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_a_activity_home_activity);
        fileManager_homeActivity = new FileManager_HomeActivity( this);

        VideoFileParser.THREAD_STOPPER=false;
        mainActivityStart = MainActivity_Start.mainActivityStartS;

        homeActivity =HomeActivity.homeActivityS=this;
        Config.IS_BACK=true;

        ButterKnife.bind(this);


        fileManager_homeActivity.favFileManager.searchButton = (androidx.appcompat.widget.AppCompatImageButton)(findViewById(R.id.seacrhb));

        friendListFileManager = new FriendListFileManager(this);







        fileManager_homeActivity.enable_tab();
        fileManager_homeActivity.disable_tab(Config.HIDE_TAB,Config.SEL_TAB);


        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        IMMLeaks.fixFocusedViewLeak(getApplication());




     }







    @Override
    public void onStart()
    {
        super.onStart();

        MainActivity_Start.mainActivityStartS = mainActivityStart;

        if(Config.TEST)
            Log.d("_HMyApp", "onStart From main Activity");





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {

        MenuCompat.setGroupDividerEnabled(menu, true);
        MenuInflater infl = getMenuInflater();
        infl.inflate(R.menu.main_menu, menu);
          return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {





        current_user=mAuth.getCurrentUser();


        int id = item.getItemId();

        if(Config.DUPLI_TEST)
            Log.d("SMS_TEST", "logout onOptionsItemSelected: "+id+"::"+current_user+"::"+item.getTitle());


        if (id == R.id.login_out)
        {



            if(Config.DUPLI_TEST)
                Log.d("SIGN_OUT_CHECK", "logout onOptionsItemSelected: "+id+"::"+current_user);

            if(mAuth.getCurrentUser()!=null)
            {


                String current_id = mAuth.getCurrentUser().getUid();

                Map<String, Object> UserStat = new HashMap<>();
                UserStat.put("online", false);
                UserStat.put("time_stamp",  FieldValue.serverTimestamp());
                UserStat.put("device_token", FieldValue.delete());

                mFirestore.collection("UsersList").document(current_id).update(UserStat).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id))
                                .requestScopes(new Scope(Scopes.EMAIL),new Scope(Scopes.PROFILE))
                                .requestEmail()
                                .requestProfile()
                                .build();

                        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(getApplicationContext(), gso);



                        FirebaseAuth firebaseAuth;
                        FirebaseAuth.AuthStateListener authStateListener = new FirebaseAuth.AuthStateListener() {
                            @Override
                            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                                if (firebaseAuth.getCurrentUser() == null){
                                    //Do anything here which needs to be done after signout is complete

                                    mGoogleSignInClient.signOut();

                                    if(Config.DUPLI_TEST)
                                        Log.d("SIGN_OUT_CHECK", "SignOUt: ");
                                }
                                else {
                                }
                            }
                        };

//Init and attach
                        firebaseAuth = FirebaseAuth.getInstance();
                        firebaseAuth.addAuthStateListener(authStateListener);

//Call signOut()
                        firebaseAuth.signOut();












                       // homeActivity.friendListFileManager.adapter.stopListening();

                      //  FriendListFragment friendListFragmentF =(FriendListFragment)aa_home_screen_PagerAdapter.registeredFragments.get(0);

                      //  friendListFragmentF.adapter.stopListening();

                      //  fileManager_homeActivity.GoToLoginActivity();











                    }


                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {


                        Toast.makeText(HomeActivity.this, "Network Error", Toast.LENGTH_SHORT).show();


                    }
                });





                /*
                String current_id = mAuth.getCurrentUser().getUid();

                Map<String, Object> UserStat = new HashMap<>();
                UserStat.put("online", false);
                UserStat.put("time_stamp",  FieldValue.serverTimestamp());

                mFirestore.collection("UsersList").document(current_id).update(UserStat).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid)
                    {






                    }
                });
*/





            }
            else
            {
              //  fileManager_homeActivity.SendtoStartActivity();
            }













            Toast.makeText(getApplicationContext(), "item1 is selected", Toast.LENGTH_SHORT).show();

        }


        if (id == R.id.Setting)
        {

            if(Config.DUPLI_TEST)
                Log.d("FLIST", "Setting");


            if(current_user==null)
            {

                Toast.makeText(HomeActivity.this,"Please Login First to get account settings" , Toast.LENGTH_SHORT).show();



            }
            else
            {
                fileManager_homeActivity.SendtoSettingsActivity();
            }










        }


        if (id == R.id.drive) {




            if(current_user==null)
            {

                Toast.makeText(getApplicationContext(),"Please Login First to get space share" , Toast.LENGTH_SHORT).show();

                fileManager_homeActivity.GoToLoginActivity();

            }
            else
            {
                fileManager_homeActivity.GoToSpaceShareListActivity();
            }










        }

        if (id == R.id.add_friend)
        {




            if(current_user==null)
            {

                Toast.makeText(this,"Please Login First to get account settings" , Toast.LENGTH_SHORT).show();


                if (Config.TEST)
                    Log.d("FRIENDS", "enable_tab : HomeActivity B"+"No login" );


            }
            else
            {
                if (Config.TEST)
                    Log.d("FRIENDS", "enable_tab : HomeActivity B"+"Login" );

                fileManager_homeActivity.GoToAddFriendActivity();

            }










        }

        if (id == R.id.queue)
        {

            if(Config.DUPLI_TEST)
                Log.d("FLIST", "friendlist");


            if(current_user==null)
            {

                Toast.makeText(HomeActivity.this,"Please Login First to get account settings" , Toast.LENGTH_SHORT).show();



            }
            else
            {

                if(Config.DUPLI_TEST)
                    Log.d("FLIST", "queue 0");

                fileManager_homeActivity.SendtoQueueActivity();
            }










        }


        if (id == R.id.create_group)
        {

            if(Config.DUPLI_TEST)
                Log.d("FLIST", "friendlist");


            if(current_user==null)
            {

                Toast.makeText(HomeActivity.this,"Please Login First to create group" , Toast.LENGTH_SHORT).show();



            }
            else
            {

                if(Config.DUPLI_TEST)
                    Log.d("FLIST", "queue 0");

                AlertDialog.Builder builder = new AlertDialog.Builder(homeActivity);
                builder.setTitle("New Name");

// Set up the input
                final EditText input = new EditText(homeActivity);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

// Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {


                        fileManager_homeActivity.addGroup( input.getText().toString());

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();





            }










        }


        if (id == R.id.blocklist)
        {

            if(Config.DUPLI_TEST)
                Log.d("FLIST", "friendlist");


            if(current_user==null)
            {

                Toast.makeText(HomeActivity.this,"Please Login First to View List" , Toast.LENGTH_SHORT).show();



            }
            else {



                fileManager_homeActivity.GoToBlockListActivity();
            }

        }



        return super.onOptionsItemSelected(item);

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        if(Config.TEST)
        Log.d("_HMyAppRESUME", "onRequestPermissionsResult: HomeActivity -- A");

        fileManager_homeActivity.onRequestPermissionsResult(requestCode,  permissions, grantResults);

        if(Config.TEST)
        Log.d("_HMyAppRESUME", "onRequestPermissionsResult: HomeActivity -- B");



    }

    @Override
    public void onStop()
    {
        super.onStop();


        if(Config.TEST)
            Log.d("_HMyApp", "onStop From main Activity");
        //aa_home_screen_PagerAdapter.registeredFragments.clear();


/*
        FriendListFragment friendListFragmentF =(FriendListFragment)aa_home_screen_PagerAdapter.registeredFragments.get(0);

        if(friendListFragmentF!=null)
        friendListFragmentF.stopFlist();
*/


    }


    @Override
    public void onPause()
    {
        super.onPause();


        //Making sure All threads are  cancelled
        ((DemoApplication)   getApplication().getApplicationContext()).Thread_KilerCommon();

        if(Config.TEST)
            Log.d("_HMyApp", "OnPause From main Activity");


    }

    @Override
    public void onResume()
    {
        super.onResume();
      //  unbinder= ButterKnife.bind(this);





        MainActivity_Start.mainActivityStartS = mainActivityStart;
        HomeActivity.homeActivityS=homeActivity ;



        if(Config.TEST)
            Log.d("_HTAB_SEL", Config.HIDE_TAB+"::"+Config.SEL_TAB );




        if(Config.TEST)
            Log.d("_HMyAppRESUME", "Resume From main Activity HOME ACIVITY");

        fileManager_homeActivity.getPermission();

        fileManager_homeActivity.window_toolbar_navbar_bg();




    }


    @Override
    public void onBackPressed()
    {

        if(Config.TEST)
            Log.d("_H_BACKBUTTON", " Inside onBackPressed :   aa_activity_home_screen A");




        switch (Config.SEL_TAB)
        {
            case 0 :
            case 1 :

               // fileManager_home.Thread_KilerCommon();

                while (getFragmentManager().getBackStackEntryCount() > 0)
                {
                    getFragmentManager().popBackStack(); // pop fragment here
                }

                //Make sure you are safe while switching than by minimize
                ((DemoApplication)   getApplication().getApplicationContext()).Thread_KilerCommon();
                 HomeActivity.homeActivityS=null;



                super.onBackPressed(); // after nothing is there default behavior of android works.

                break;


            case 2 :


                fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerView.stopScroll();
                fileManager_homeActivity.disable_tab(2,1);


                break;
        }


        if(Config.TEST)
            Log.d("_HBACKBUTTON", " Inside onBackPressed :   aa_activity_home_screen B");


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_P:
            {




                    if (Config.TEST)
                    {


                        Log.d("DONE_FOLDER", "call started");


                    }


                return true;
            }


        }
        return super.onKeyDown(keyCode, event);
    }




    public void Thread_KilerHome()
    {




        if(Config.TEST)
            Log.d("_#_TT_TEST", " Inside T test");






/////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Thread for making random delay at first time running
        if(MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread!=null)
        {

            if(MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread.isAlive())
            {
                VideoFileParser.THREAD_STOPPER=true;
            }
            while (MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread.isAlive());

        }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////





        if(VideoFileParser.getInstance().threadForStorageReadDirectoryI !=null)
        {

            if(VideoFileParser.getInstance().threadForStorageReadDirectoryI.isAlive())
            {
                VideoFileParser.THREAD_STOPPER=true;
            }



            while (VideoFileParser.getInstance().threadForStorageReadDirectoryI.isAlive())
            {

                // if(Config.TEST)
                //    Log.d("_#_TT_TEST", " threadForStorageReadDirectoryI"+firstTimeThread.isAlive()+"::"+THREAD_STOPPER);
            }


        }


        if(VideoFileParser.getInstance().threadForStorageReadDirectoryE !=null)
        {

            if(VideoFileParser.getInstance().threadForStorageReadDirectoryE.isAlive())
            {
                VideoFileParser.THREAD_STOPPER=true;
            }



            while (VideoFileParser.getInstance().threadForStorageReadDirectoryE.isAlive())
            {
            }


        }




////////////////////////////////////// Home  activity thread /////////////////////////////////////

        //Fav frag thread






        if(HomeActivity.homeActivityS!=null)
        {

            if(HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir !=null)
            {

                if(HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir.isAlive())
                {
                    VideoFileParser.THREAD_STOPPER=true;
                }



                while (HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir.isAlive());



            }

            HomeActivity.homeActivityS.fileManager_homeActivity.folderFileManager.threadForReadingFilesFromDir =null;
        }





        MainActivity_Start.mainActivityStartS._fileManager_mainActivity.firstTimeThread=null;



        VideoFileParser.THREAD_STOPPER=false;
        VideoFileParser.getInstance().threadForStorageReadDirectoryI =null;
        VideoFileParser.getInstance().threadForStorageReadDirectoryE =null;


    }







}
