package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments.ab_bottomsheet.BottomSheetF;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments.ab_bottomsheet.Item;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_a_chat_user.FireStoreChat.FirestoreChatActivity;
import com.media.vidplayer2.mond.zb_a_chat_user.GetTimeAgo;
import com.media.vidplayer2.mond.zb_b_chat_group.FireStoreChat.FirestoreGroupActivity;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;
import com.media.vidplayer2.mond.zh_friendlist.ModelFriendlist;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.ArrayList;
import java.util.HashMap;

public class FriendListAdapter extends FirestoreRecyclerAdapter<ModelFriendlist, FriendListAdapter.NoteHolder> {
    BottomSheetF dialog;
    DocumentReference docRef;
    FutureStudioAppGlideModule futureStudioAppGlideModule ;
    FriendListFragment friendListFragment;

    public  String searchText=null;

   //public  ListenerRegistration listenerRegistration;

   // public  ListenerRegistration listenerRegistrationMessage;

    public static String current_id ;
    public static String current_name ;
    public static String current_image;
    public static String current_email;

    public   String current_uid ;


    public FriendListAdapter(@NonNull FirestoreRecyclerOptions<ModelFriendlist> options, FriendListFragment friendListFragment) {
        super(options);
        futureStudioAppGlideModule = new FutureStudioAppGlideModule();
        this.friendListFragment = friendListFragment;

        this.current_uid = FirebaseAuth.getInstance().getUid();



        ListenerRegistration listenerRegistration = null;
        ListenerRegistration finalListenerRegistration = listenerRegistration;
        listenerRegistration= FirebaseFirestore.getInstance().collection("UsersList").document(FirebaseAuth.getInstance().getUid()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }


                FriendListAdapter.current_id =FirebaseAuth.getInstance().getCurrentUser().getUid();
                FriendListAdapter.current_name= documentSnapshot.getString("name");
                FriendListAdapter.current_image= documentSnapshot.getString("thumb_image");
                FriendListAdapter.current_email= documentSnapshot.getString("email");






            }
        });



    }

    @Override
    protected void onBindViewHolder(@NonNull NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {
        //holder.textViewTitle.setText(model.getId());
     //   holder.textViewDescription.setText(model.getDescription());
      //  holder.textViewPriority.setText(String.valueOf(model.getPriority()));


        if(Config.DUPLI_TEST)
            Log.d("YES_CASE", "onEvent: 1 "+friendListFragment.homeActivity.friendListFileManager.searchfriends);



        if(friendListFragment.homeActivity.friendListFileManager.searchfriends)
        {

            if(Config.DUPLI_TEST)
                Log.d("YES_CASE", "onEvent: 1 "+friendListFragment.homeActivity.friendListFileManager.searchfriends);

            boolean isFound = model.getName().toLowerCase().indexOf(searchText.toLowerCase()) !=-1? true: false; //true


            if(isFound || searchText.equals("") )
            {
                holder.itemView.setVisibility(View.VISIBLE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE_X", "onEvent: 2 "+model.getName()+"::"+searchText);

            }

            else
            {
                holder.itemView.setVisibility(View.GONE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));

                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE_X", "onEvent: 3 "+model.getName()+"::"+searchText);

            }

        }
        else {


            if(Config.DUPLI_TEST)
                Log.d("GROUP_ISSUE_X", "onEvent: 4 "+model.getName()+"::"+searchText);



            holder.itemView.setVisibility(View.VISIBLE);
            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }



        if(model.getBlock())
        {
            holder.itemView.setVisibility(View.GONE);
            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));

        }







        if(Config.DUPLI_TEST)
            Log.d("GROUP_ISSUE", "onEvent: 1 ");





            if((FirebaseAuth.getInstance().getCurrentUser()!=null)  && (model.getTime_stamp()!=null))
            {
                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE", "onEvent: 2 ");

                 if(!model.getGroup())
                 {
                     listeners_users(holder,position,model);
                     listeners_chats(holder,position,model);
                     if(Config.DUPLI_TEST)
                         Log.d("GROUP_ISSUE", "onEvent: 3 ");
                 }
                 else
                 {
                     if(Config.DUPLI_TEST)
                         Log.d("GROUP_ISSUE", "onEvent: 4");
                     listeners_groups(holder,position,model);
                     listeners_groupchats(holder,position,model);
                 }









                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A_FULL", "other user ::  "+model.getId()+"current user::"+FirebaseAuth.getInstance().getCurrentUser().getUid()+"::"+position+"::"+model.getGroup());


                if((FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getId()) || model.getBlock())  )
                {
                    //  holder.recycler_item_frdlist.setVisibility(View.GONE);

                    if(  !model.getGroup() )
                    {
                        holder.itemView.setVisibility(View.GONE);
                        holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
                    }


                }
                else
                {


                    if(Config.DUPLI_TEST)
                        Log.d("DOWNLOAD_LINK_A_FULL", "other user ::  "+model.getId()+"current user::"+FirebaseAuth.getInstance().getCurrentUser().getUid()+"::"+position);


                }

            }



        holder.slice.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(model.getGroup())
                {





                       BottomSheetF dialog = new BottomSheetF( friendListFragment.homeActivity,model,true,holder);
                        dialog.show();





                }
                else
                {

/*
                    ArrayList<String> items=new ArrayList<>();
                    items.add(   "Chat");
                    if(model.getGroup())
                    {


                        if(model.getId().equals(FriendListAdapter.current_id))
                        {
                            items.add( "Freeze Group" );
                        }
                        else
                        {
                            items.add( "Leave Group");
                        }


                    }
                    else
                    {
                        items.add( "Block User" );
                    }



                    final String[] fonts  = new String[items.size()];

                    for(int i=0;i<items.size();i++)
                    {
                        fonts[i]=items.get(i);
                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(friendListFragment.homeActivity);
                    builder.setTitle("Select a text size");
                    builder.setItems(fonts, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if ("Chat".equals(fonts[which])){
                                Toast.makeText(friendListFragment.homeActivity,"Chat", Toast.LENGTH_SHORT).show();

                                Intent chatIntent = new Intent(friendListFragment.homeActivity, FirestoreChatActivity.class);
                                chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                                chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                                chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                                chatIntent.putExtra("current_email", FriendListAdapter.current_email);

                                chatIntent.putExtra("other_user_id", holder.other_user_id);
                                chatIntent.putExtra("other_user_name",holder.other_user_name);
                                chatIntent.putExtra("other_user_image", holder.other_user_image);
                                chatIntent.putExtra("other_user_email", holder.other_user_email);
                                if(Config.DUPLI_TEST)
                                    Log.d("NULL_TEST_CONF", "onClick: "+holder.other_user_id+"::"+holder.other_user_name);


                                friendListFragment.homeActivity.startActivity(chatIntent);


                            }
                            else if ("Medium".equals(fonts[which])){
                                Toast.makeText(friendListFragment.homeActivity,"you cracked it", Toast.LENGTH_SHORT).show();
                            }
                            else if ("Large".equals(fonts[which])){
                                Toast.makeText(friendListFragment.homeActivity,"you hacked it", Toast.LENGTH_SHORT).show();
                            }
                            else if ("Huge".equals(fonts[which])){
                                Toast.makeText(friendListFragment.homeActivity,"you digged it", Toast.LENGTH_SHORT).show();
                            }
                            // the user clicked on colors[which]

                        }
                    });
                    builder.show();

*/
                       dialog = new BottomSheetF( friendListFragment.homeActivity,model,false,holder);
                         dialog.show();
                }


            }
        });






    }



    void listeners_groups(NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {



        ListenerRegistration listenerRegistration;
        listenerRegistration=FirebaseFirestore.getInstance().collection("GroupLists").document(model.getId()).collection("GroupNames").document(model.getName()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }









                String name = documentSnapshot.getString("name");
                String thumb_image =  documentSnapshot.getString("thumb_image");
                String status =  documentSnapshot.getString("status");


                try{



                    Timestamp timestamp    = documentSnapshot.getTimestamp("someonemessagedyou");



                    if(Config.DUPLI_TEST)
                        Log.d("GRP_TEST_CASE", "onEvent: "+name+"::"+thumb_image+"::"+status+"::"+timestamp+"::"+current_id);

                    HashMap<String, Object> FrduserMap_1 = new HashMap<>();



                    FrduserMap_1.put("time_stamp",  timestamp);
                    DocumentReference FriendListReference   =  FirebaseFirestore.getInstance().collection("Friends").document(FirebaseAuth.getInstance().getUid()).collection("MyFriends").document(name);


                    WriteBatch batch =  FirebaseFirestore.getInstance().batch();


                    batch.update(FriendListReference, FrduserMap_1);


                    if(timestamp!=null)
                    {
                        batch.commit();
                    }







                    if(Config.DUPLI_TEST)

                        Log.d("GROUP_ISSUE_CC", "onEvent: "+name+"::"+thumb_image+"::"+status);


                    holder.user_single_name.setText(name );
                    holder.statusfrd.setText(status );



                    holder.time_left.setVisibility(View.GONE);

                    holder.user_single_online_icon.setVisibility(View.GONE);

                    if(!thumb_image.equals("default"))
                    {
                        futureStudioAppGlideModule.add_image_to_view_link(friendListFragment.getContext(),holder.user_single_image,thumb_image);

                    }



                   // BottomSheetF dialog = new BottomSheetF( friendListFragment.homeActivity,model,true);
                    //dialog.show();





/*
                    holder.user_single_name.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {

                            Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreGroupActivity.class);
                            chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                            chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                            chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                            chatIntent.putExtra("current_email", FriendListAdapter.current_email);



                            chatIntent.putExtra("GroupName",model.getName());
                            chatIntent.putExtra("CreaterId",model.getId());

                            friendListFragment.getActivity().startActivity(chatIntent);


                        }
                    });
                    holder.statusfrd.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {


                            Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreGroupActivity.class);
                            chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                            chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                            chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                            chatIntent.putExtra("current_email", FriendListAdapter.current_email);

                            chatIntent.putExtra("GroupName",model.getName());
                            chatIntent.putExtra("CreaterId",model.getId());

                            friendListFragment.getActivity().startActivity(chatIntent);


                        }
                    });



                    holder.user_single_image.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view) {

                            Intent chatIntent = new Intent(friendListFragment.getActivity(), TestCase.class);
                            chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                            chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                            chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                            chatIntent.putExtra("current_email", FriendListAdapter.current_email);

                            chatIntent.putExtra("GroupName",model.getName());
                            chatIntent.putExtra("CreaterId",model.getId());

                            friendListFragment.getActivity().startActivity(chatIntent);

                        }
                    });

                    */


                }catch (Exception e1) {
                    e1.printStackTrace();
                }









            }
        });
        if(friendListFragment.listenersUsers.size()>this.getItemCount())
        {
            friendListFragment.listenersUsers.get(position).remove();
        }
        friendListFragment.listenersUsers.add(listenerRegistration);

    }

    void listeners_users(NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {

        ListenerRegistration listenerRegistration;
        listenerRegistration= FirebaseFirestore.getInstance().collection("UsersList").document(model.getId()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }




                if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getId()) )
                {
                    //  holder.recycler_item_frdlist.setVisibility(View.GONE);

                    FriendListAdapter.current_id =FirebaseAuth.getInstance().getCurrentUser().getUid();
                    FriendListAdapter.current_name= documentSnapshot.getString("name");
                    FriendListAdapter.current_image= documentSnapshot.getString("thumb_image");
                    FriendListAdapter.current_email= documentSnapshot.getString("email");

                }
                else
                {

                    holder.other_user_id=documentSnapshot.getString("id");
                    holder.other_user_name=documentSnapshot.getString("name");
                    holder.other_user_image=documentSnapshot.getString("thumb_image");
                    holder.other_user_email=documentSnapshot.getString("email");
                }





                String id = documentSnapshot.getString("id");
                String status = documentSnapshot.getString("status");
                String thumb_image =  documentSnapshot.getString("thumb_image");
                String name =  documentSnapshot.getString("name");
                Boolean online =  documentSnapshot.getBoolean("online");
                Timestamp timestamp = documentSnapshot.getTimestamp("time_stamp");






                holder.user_single_name.setText(name );
                holder.statusfrd.setText(status );







                if(!thumb_image.equals("default"))
                {
                    futureStudioAppGlideModule.add_image_to_view_link(friendListFragment.getContext(),holder.user_single_image,thumb_image);

                }





                if( online )
                {
                    holder.user_single_online_icon.setVisibility(View.VISIBLE);
                    holder.time_left.setText("Online");
                }
                else
                {
                    holder.user_single_online_icon.setVisibility(View.GONE);
                    holder.user_single_online_icon.setVisibility(View.GONE);
                    GetTimeAgo getTimeAgo = new GetTimeAgo();
                    holder.time_left.setText("Online");
                    String lastSeenTime=null;
                    if(timestamp!=null)
                        lastSeenTime = getTimeAgo.getTimeAgo(timestamp.toDate().getTime(), friendListFragment.getActivity());
                    holder.time_left.setText(lastSeenTime);

                }



                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A", "status "+status+"thumb_image "+thumb_image+"name :: "+name);

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A", "error "+ e);



              //  BottomSheetF dialog = new BottomSheetF( friendListFragment.homeActivity,model,false);
              //  dialog.show();



                /*


                holder.user_single_name.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        if(holder.other_user_id == null ||holder.other_user_name == null ||holder.other_user_image == null ||holder.other_user_email == null )
                        {

                            CharSequence text = "Loading info please wait";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(friendListFragment.getActivity(), text, duration);
                            toast.show();
                        }
                        else
                        {
                            //  Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                            //Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreGroupActivity.class);



                            ListenerRegistration listenerRegistration;
                            listenerRegistration= FirebaseFirestore.getInstance().collection("UsersList").document(FirebaseAuth.getInstance().getUid()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
                                @Override
                                public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                                    @Nullable FirebaseFirestoreException e) {


                                    if (e != null) {
                                        System.err.println("Listen failed:" + e);
                                        return;
                                    }


                                    FriendListAdapter.current_id =FirebaseAuth.getInstance().getCurrentUser().getUid();
                                    FriendListAdapter.current_name= documentSnapshot.getString("name");
                                    FriendListAdapter.current_image= documentSnapshot.getString("thumb_image");
                                    FriendListAdapter.current_email= documentSnapshot.getString("email");

                                    Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreChatActivity.class);
                                    chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                                    chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                                    chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                                    chatIntent.putExtra("current_email", FriendListAdapter.current_email);

                                    chatIntent.putExtra("other_user_id", holder.other_user_id);
                                    chatIntent.putExtra("other_user_name",holder.other_user_name);
                                    chatIntent.putExtra("other_user_image", holder.other_user_image);
                                    chatIntent.putExtra("other_user_email", holder.other_user_email);
                                    if(Config.DUPLI_TEST)
                                        Log.d("NULL_TEST_CONF", "onClick: "+holder.other_user_id+"::"+holder.other_user_name);


                                    friendListFragment.getActivity().startActivity(chatIntent);



                                }
                            });



                        }




                    }
                });
                holder.statusfrd.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                        //SendtoAllUsersProfleActivity(user_id);
                        //Log.d("Clicked id", user_id);

                        if(holder.other_user_id == null ||holder.other_user_name == null ||holder.other_user_image == null ||holder.other_user_email == null )
                        {

                            CharSequence text = "Loading info please wait";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(friendListFragment.getActivity(), text, duration);
                            toast.show();
                        }
                        else
                        {
                            //  Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                            //Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreGroupActivity.class);



                            ListenerRegistration listenerRegistration;
                            listenerRegistration= FirebaseFirestore.getInstance().collection("UsersList").document(FirebaseAuth.getInstance().getUid()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
                                @Override
                                public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                                    @Nullable FirebaseFirestoreException e) {


                                    if (e != null) {
                                        System.err.println("Listen failed:" + e);
                                        return;
                                    }


                                    FriendListAdapter.current_id =FirebaseAuth.getInstance().getCurrentUser().getUid();
                                    FriendListAdapter.current_name= documentSnapshot.getString("name");
                                    FriendListAdapter.current_image= documentSnapshot.getString("thumb_image");
                                    FriendListAdapter.current_email= documentSnapshot.getString("email");

                                    Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreChatActivity.class);
                                    chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                                    chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                                    chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                                    chatIntent.putExtra("current_email", FriendListAdapter.current_email);

                                    chatIntent.putExtra("other_user_id", holder.other_user_id);
                                    chatIntent.putExtra("other_user_name",holder.other_user_name);
                                    chatIntent.putExtra("other_user_image", holder.other_user_image);
                                    chatIntent.putExtra("other_user_email", holder.other_user_email);
                                    if(Config.DUPLI_TEST)
                                        Log.d("NULL_TEST_CONF", "onClick: "+holder.other_user_id+"::"+holder.other_user_name);


                                    friendListFragment.getActivity().startActivity(chatIntent);



                                }
                            });



                        }







                    }
                });



                holder.user_single_image.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                        //SendtoAllUsersProfleActivity(user_id);
                        //Log.d("Clicked id", user_id);

                        CharSequence options[] = new CharSequence[]{"Open Profile", "Remove","Block"};

                        final AlertDialog.Builder builder = new AlertDialog.Builder(friendListFragment.getActivity());

                        builder.setTitle("Select Options");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {


                                switch(i)
                                {
                                    case 0:

                                        //   Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                                        Intent profileintent = new Intent(friendListFragment.getActivity(), MyUserProfileActivity.class);
                                        profileintent.putExtra("otherprofile", true);
                                        profileintent.putExtra("other_user_id", holder.other_user_id);



                                        friendListFragment.getActivity().startActivity(profileintent);


                                        break;

                                    case 1:





                                        WriteBatch batch =  FirebaseFirestore.getInstance().batch();


                                        batch.delete(holder.FriendListReference_1);
                                        batch.delete(holder.FriendListReference_2);

                                        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {


                                            }


                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {





                                            }
                                        });


                                        //docRef.delete();
                                        break;
                                    case 2:


                                        HashMap<String, Object> FrduserMap_1 = new HashMap<>();



                                        FrduserMap_1.put("block",  true);
                                        HashMap<String, Object> FrduserMap_2 = new HashMap<>();



                                        FrduserMap_2.put("block",  true);
                                        WriteBatch batch1 =  FirebaseFirestore.getInstance().batch();


                                        batch1.update(holder.FriendListReference_1, FrduserMap_1);
                                        batch1.update(holder.FriendListReference_2, FrduserMap_2);

                                        batch1.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {



                                            }


                                        }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {





                                            }
                                        });

                                        break;
                                }


                            }
                        });

                        builder.show();

                    }
                });
*/






            }
        });
        if(friendListFragment.listenersUsers.size()>this.getItemCount())
        {
            friendListFragment.listenersUsers.get(position).remove();
        }
        friendListFragment.listenersUsers.add(listenerRegistration);

    }

    void listeners_chats(NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {
          final int size = this.getItemCount();

           ListenerRegistration listenerRegistrationMessage=  FirebaseFirestore.getInstance().collection("Chats").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection(model.getId()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("ERROR", "Listen failed.", e);
                    return;
                }



                Timestamp timestamp = new Timestamp(model.getTime_stamp());
                if(Config.DUPLI_TEST)
                    Log.d("_AFRAG_FRDID", "Chat Changed "+model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);


                OnSuccessListener onSuccessListener = new OnSuccessListener<QuerySnapshot>()
                {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots)
                    {


                        if(Config.DUPLI_TEST)
                            Log.d("_AFRAG_FRDID", "Chat Changed 1 "+model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);


                        if (queryDocumentSnapshots.size() > 0)
                        {


                            try {
                                // holder.noti_newmsg.setText("( " +queryDocumentSnapshots.size()+" )");;
                                holder.badge.setVisibility(View.VISIBLE);
                                holder.badge.setNumber(queryDocumentSnapshots.size());
                                holder.badge.getAnimationEnabled();
                                holder.badge.setAnimationDuration(1000);
                                holder.badge.animate();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if(Config.DUPLI_TEST)
                                Log.d("_AFRAG_FRDID", "Chat Changed 2 "+model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);


                        }
                        else
                        {


                            if(Config.DUPLI_TEST)
                                Log.d("_AFRAG_FRDID", "Chat Changed 3 "+model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);


                            try {
                                holder.badge.setVisibility(View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }




                        }



                    }
                };


                Task<QuerySnapshot> task = FirebaseFirestore.getInstance().collection("Chats").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection(model.getId()).whereGreaterThanOrEqualTo("timestamp",timestamp).get();







                if(friendListFragment.listoftaskschataccess.size()>size)
                {
                    friendListFragment.listoftaskschataccess.get(position).addOnSuccessListener(null);
                }
                task.addOnSuccessListener(onSuccessListener);

                friendListFragment.listoftaskschataccess.add(task);



            }
        });
        if(friendListFragment.listenersNewMessage.size()>this.getItemCount())
        {
            friendListFragment.listenersNewMessage.get(position).remove();
        }
        friendListFragment.listenersNewMessage.add( listenerRegistrationMessage);


    }

    void listeners_groupchats(NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {
        final int size = this.getItemCount();

        ListenerRegistration listenerRegistrationMessage=  FirebaseFirestore.getInstance().collection("GroupChats").document(model.getId()).collection(model.getName()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot value,
                                @Nullable FirebaseFirestoreException e)
            {
                if (e != null) {
                    Log.w("ERROR", "Listen failed.", e);
                    return;
                }

                Timestamp timestamp;

                    try{
                          timestamp = new Timestamp(model.getIsawmessage());

                        if(Config.DUPLI_TEST)
                            Log.d("_AFRAG_FRDIDDDD", "Chat Changed --"+model.getIsawmessage()+":--:"+model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);

                        OnSuccessListener onSuccessListener = new OnSuccessListener<QuerySnapshot>()
                        {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots)
                            {


                                if(Config.DUPLI_TEST)
                                    Log.d("_AFRAG_FRDID", "Chat Changed 1 "+model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);


                                if (queryDocumentSnapshots.size() > 0)
                                {


                                    try {
                                        // holder.noti_newmsg.setText("( " +queryDocumentSnapshots.size()+" )");;
                                        holder.badge.setVisibility(View.VISIBLE);
                                        holder.badge.setNumber(queryDocumentSnapshots.size());
                                        holder.badge.getAnimationEnabled();
                                        holder.badge.setAnimationDuration(1000);
                                        holder.badge.animate();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if(Config.DUPLI_TEST)
                                        Log.d("_AFRAG_FRDID", "Chat Changed 2 "+model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);


                                }
                                else
                                {


                                    if(Config.DUPLI_TEST)
                                        Log.d("_AFRAG_FRDID", "Chat Changed 3 "+model.getId()+"::"+model.getBlock()+"::"+model.getTime_stamp()+"::"+position);


                                    try {
                                        holder.badge.setVisibility(View.GONE);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }




                                }



                            }
                        };
                        Task<QuerySnapshot> task = FirebaseFirestore.getInstance().collection("GroupChats").document(model.getId()).collection(model.getName()).whereGreaterThanOrEqualTo("timestamp",timestamp).get();
                        if(friendListFragment.listoftaskschataccess.size()>size)
                        {
                            friendListFragment.listoftaskschataccess.get(position).addOnSuccessListener(null);
                        }
                        task.addOnSuccessListener(onSuccessListener);

                        friendListFragment.listoftaskschataccess.add(task);



                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }







            }
        });
        if(friendListFragment.listenersNewMessage.size()>this.getItemCount())
        {
            friendListFragment.listenersNewMessage.get(position).remove();
        }
        friendListFragment.listenersNewMessage.add( listenerRegistrationMessage);


    }

    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.am_friendlist_item,
                parent, false);
        return new NoteHolder(v);
    }

    public class NoteHolder extends RecyclerView.ViewHolder {

        ImageView user_single_image;
        TextView user_single_name;
        TextView statusfrd;
        TextView noti_newmsg;

        NotificationBadge badge;

        LinearLayout slice;

        LinearLayout recycler_item_frdlist;
        ImageView user_single_online_icon;
        ImageView more;
        ImageView chatF;
        TextView time_left;
        public DocumentReference FriendListReference_1 ;
        public  DocumentReference FriendListReference_2  ;


        public   String other_user_id;
        public   String other_user_name;
        public   String other_user_image;
        public   String other_user_email;


        public NoteHolder(View itemView) {
            super(itemView);
            user_single_image = itemView.findViewById(R.id.user_single_image);

            time_left = itemView.findViewById(R.id.time_left);

            user_single_online_icon = itemView.findViewById(R.id.user_single_online_icon);
            user_single_name = itemView.findViewById(R.id.user_single_name);
            statusfrd = itemView.findViewById(R.id.statusfrd);
            badge = itemView.findViewById(R.id.badge);


            slice = itemView.findViewById(R.id.recycler_item_frdlist);
            recycler_item_frdlist = itemView.findViewById(R.id.recycler_item_frdlist);
        }


    }

}



