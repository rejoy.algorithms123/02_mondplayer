package com.media.vidplayer2.mond.aa_MainActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;


import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class FileManagerMainActivity {

    public FirstTimeThread                                                  firstTimeThread = null;

    //0 for none  1 - for ad_back  2 for intent
    int                                                                                    TOKEN =0;


    public int                                                                 ONBACK_FIRST_DONE =1;
    public int                                                                 INTENT_FIRST_DONE =2;
    final  int                                                                  numberOfPermits = 1;
    public                                                                      Semaphore semaphore;










    public  static Boolean                                                   video_flag_fav_1=false;
    public  static Integer                                                              count_fav_1;

    public  static Boolean                                                   video_flag_fav_2=false;
    public  static Integer                                                              count_fav_2;


    public FileManagerMainActivity()
    {
        this.firstTimeThread     = new FirstTimeThread();
        semaphore                = new Semaphore(numberOfPermits, true);

    }


    public boolean isFirstTime(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean ranBefore = preferences.getBoolean("Permission", false);

        if(Config.TEST)
            Log.d("MyApp_first", "isFirstTime shared pref : aa_activity_StartUp " + ranBefore);

        return !ranBefore;
    }



    //////////////////////////////////////////////////////////////////////////////////////////////////
    public static class FirstTimeThread extends Thread {
        @Override
        public void run()
        {

            if(Config.TEST)
                Log.d("_##_FIRST_TIME", " Inside FirstTimeThread Thread :  aa_activity_StartUp A");


            long   length =(long) (Config.TIME_DELAY_START_SCREEN/Config.TIME_DELAY_START_SCREEN_STEP);


            try
            {
                for(long i = 0; i<Config.TIME_DELAY_START_SCREEN_STEP ; i++)
                {
                    Thread.sleep(length);
                    if (Config.TEST)
                        Log.d("_##_FIRST_TIME", " CS :   aa_activity_StartUp  :: " +i);
                }
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }


            try
            {
                MainActivity_Start.mainActivityStartS._fileManager_mainActivity.semaphore.acquire();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }



            if(MainActivity_Start.mainActivityStartS._fileManager_mainActivity.TOKEN!= MainActivity_Start.mainActivityStartS._fileManager_mainActivity.ONBACK_FIRST_DONE)
            {
                RedirectToHomePage_Intent_From_FirstTime();
                MainActivity_Start.mainActivityStartS._fileManager_mainActivity.TOKEN = MainActivity_Start.mainActivityStartS._fileManager_mainActivity.INTENT_FIRST_DONE;
            }


            MainActivity_Start.mainActivityStartS._fileManager_mainActivity.semaphore.release();

            if( Config.TEST)
                Log.d("_##_FIRST_TIME", " Inside FirstTimeThread Thread :  aa_activity_StartUp B");


        }

    }

    private static void RedirectToHomePage_Intent_From_FirstTime()
    {

        if(Config.TEST)
            Log.d("_###_REDIR_FIRST_SCREEN", "RedirectToHomePage_Intent_From_FirstTime()B1 : aa_activity_StartUp");

            Intent intent = new Intent(MainActivity_Start.mainActivityStartS, HomeActivity.class );
            MainActivity_Start.mainActivityStartS.startActivity(intent);


        if(Config.TEST)
            Log.d("_###_REDIR_FIRST_SCREEN", "RedirectToHomePage_Intent_From_FirstTime()B2 : aa_activity_StartUp");

    }
    //////////////////////////////////////////////////////////////////////////////////////////////////



    //////////////////////////////////////////////////////////////////////////////////////////////////
    private static void RedirectToHomePage_Intent_From_SecondTime()
    {

        if(Config.TEST)
        {
            Log.d("_#REDIR_SPLASH_SCREEN", "RedirectToHomePage_Intent_From_SecondTime() A : aa_activity_StartUp"+ MainActivity_Start.mainActivityStartS);

        }



        Intent intent = new Intent(MainActivity_Start.mainActivityStartS, HomeActivity.class );


       MainActivity_Start.mainActivityStartS.startActivity(intent);



        if(Config.TEST)
            Log.d("_#REDIR_SPLASH_SCREEN", "RedirectToHomePage_Intent_From_SecondTime() B : aa_activity_StartUp");





    }
    public void ParseVideoOnSecondTime(String activity_name)
    {
        VideoFileParser.getInstance().names_fav_MEM.clear();
        VideoFileParser.getInstance().names_fav_SD.clear();
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        File dir1 = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        Runnable barrierAction = new Runnable() { public void run()
        {





            try
            {
                semaphore.acquire();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }


            VideoFileParser.getInstance().total_SD_MEM = new ArrayList<>(VideoFileParser.getInstance().names_fav_MEM.size() + VideoFileParser.getInstance().names_fav_SD.size());
            VideoFileParser.getInstance().total_SD_MEM.addAll(VideoFileParser.getInstance().names_fav_MEM);
            //total_SD_MEM.addAll(names_fav_SD);

            if(TOKEN!= ONBACK_FIRST_DONE)
            {
                RedirectToHomePage_Intent_From_SecondTime();
                TOKEN =INTENT_FIRST_DONE;
            }

            if(Config.TEST)
            {



                if (Config.TEST)
                {
                    Log.d("_#FULL_ARRAY", "granted onRequestPermissionsResult : Second Run" +VideoFileParser.getInstance().total_SD_MEM);
                    for (int i =0 ;i<VideoFileParser.getInstance().total_SD_MEM.size();i++)
                    {
                        Log.d("Array Value","Array Value"+VideoFileParser.getInstance().total_SD_MEM.get(i).dir_name+"::"+VideoFileParser.getInstance().total_SD_MEM.get(i).ID +"::"+VideoFileParser.getInstance().total_SD_MEM.get(i).video_count);
                    }
                }



            }




            semaphore.release();

        }};


        CyclicBarrier barrier = new CyclicBarrier(2, barrierAction);
        VideoFileParser.getInstance().MakeNewThreadsForIntenralAndExternalDirReading();
        VideoFileParser.getInstance().threadForStorageReadDirectoryI.SetThreadValue(dir,"Thread_1",barrier);
        VideoFileParser.getInstance().threadForStorageReadDirectoryE.SetThreadValue(dir,"Thread_2",barrier);
        VideoFileParser.getInstance().threadForStorageReadDirectoryI.start();
        VideoFileParser.getInstance().threadForStorageReadDirectoryE.start();

    }


}
