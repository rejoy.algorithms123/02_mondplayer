package com.media.vidplayer2.mond.zf_space_share.spaceList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import com.media.vidplayer2.mond.zb_b_spaceshare_file_selector.SpaceShareChatUploadActivity;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.zf_space_share.spaceList.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zf_space_share.spaceList.ab_bottomsheet.BottomSheet;
import com.media.vidplayer2.mond.zf_space_share.uploadSpace.GoogleDriveFileHolder;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static android.os.Environment.*;

public class FileListSpaceShareActivity {

    private Executor mExecutor = Executors.newSingleThreadExecutor();
    public Drive mDriveService=null;
    public ListSpaceShareActivity listSpaceShareActivity =null;

    public BottomSheet dialog=null;

    private Context context=null;
    public  List<GoogleDriveFileHolder> list_of_files= new ArrayList<>();
    FileList result=null;
    private  String foldername ="MondPlayerDataFiles";

    public FileListSpaceShareActivity(Drive mDriveService, Context context) {
        this.mDriveService = mDriveService;
        this.context = context;
        listSpaceShareActivity = (ListSpaceShareActivity)this.context;
        dialog = new BottomSheet(context);

    }






    public Task<List<GoogleDriveFileHolder>> queryFiles(@Nullable final String folderId)
    {
        return Tasks.call(mExecutor, new Callable<List<GoogleDriveFileHolder>>() {
                    @Override
                    public List<GoogleDriveFileHolder> call() throws Exception
                    {
                        List<GoogleDriveFileHolder> googleDriveFileHolderList = new ArrayList<>();
                        String parent = "root";

                        if (folderId != null) {
                            parent = folderId;
                        }

                        FileList result = mDriveService.files().list().setQ("'" + parent + "' in parents").setFields("files/*").setSpaces("drive").execute();

                        for (int i = 0; i < result.getFiles().size(); i++)
                        {

                            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();
                            googleDriveFileHolder.setFile(result.getFiles().get(i));


                            googleDriveFileHolderList.add(googleDriveFileHolder);

                        }


                        return googleDriveFileHolderList;


                    }
                }
        ).addOnSuccessListener(new OnSuccessListener<List<GoogleDriveFileHolder>>() {
            @Override
            public void onSuccess(List<GoogleDriveFileHolder> googleDriveFileHolders) {




                list_of_files.addAll(googleDriveFileHolders) ;
                for(int i=1;i<list_of_files.size();i++){


                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_LIST", "no ::1  "+ list_of_files.get(i).getFile().getName() );


                }

                listSpaceShareActivity.Folder_Frag_recyclerAdapter.setRecyclerAdapter(list_of_files, context);
                 listSpaceShareActivity.Folder_Frag_recyclerAdapter.notifyItemRangeChanged(0,list_of_files.size());
                listSpaceShareActivity.Folder_Frag_recyclerAdapter.notifyDataSetChanged();

              //  dialog .hide();


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Upload  success   :: 6 " );





            }
        });

    }


    public Task<GoogleDriveFileHolder> ListFilesDrive_B()
    {

        return Tasks.call(mExecutor, () -> {

            result=null;

            result = mDriveService.files().list()
                    .setQ("mimeType = '" + "application/vnd.google-apps.folder"+ "' and name = '" + foldername + "' ")
                    .setSpaces("drive")
                    .execute();
            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();


            if(result.getFiles().size() > 0)
            {
                googleDriveFileHolder.setFile(result.getFiles().get(0));

            }


            return googleDriveFileHolder;
        }).addOnSuccessListener(new OnSuccessListener<GoogleDriveFileHolder>() {
            @Override
            public void onSuccess(GoogleDriveFileHolder googleDriveFileHolder) {



                if (!(result.getFiles().size() > 0) )
                {
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_LIST", " Folder already not exists   :: 2 " );






                }
                else
                {




                      queryFiles(googleDriveFileHolder.getFile().getId());




                    //uploadFilePart(  name,   uri,   mime_type,    result.getFiles().get(0).getId());

                }

            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_LIST", " Folder creation/search failed   :: 1 "+e);




            }
        });




    }


    public Task<GoogleDriveFileHolder> ListFilesDrive_A()
    {

        return Tasks.call(mExecutor, () -> {
            About about=null;
            try {
                  about =  mDriveService.about().get().setFields("user, storageQuota").execute();
            } catch (IOException e) {
                e.printStackTrace();

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_LIST_Q",   e+"::");

            }
            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();
            googleDriveFileHolder.setAbout(about);

            return googleDriveFileHolder;
        }).addOnSuccessListener(new OnSuccessListener<GoogleDriveFileHolder>() {
            @Override
            public void onSuccess(GoogleDriveFileHolder googleDriveFileHolder) {

                list_of_files.clear();
                list_of_files.add(googleDriveFileHolder);


                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_LIST_Q",  googleDriveFileHolder.getAbout().getStorageQuota()+"::");

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_LIST_Q",  list_of_files.get(0).getAbout().getStorageQuota()+"::");



                ListFilesDrive_B();
            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_LIST", " Folder creation/search failed   :: 1 "+e);




            }
        });




    }









    public Task<String> delete_file_Case(String stringId)
    {
        return Tasks.call(mExecutor, () -> {

            mDriveService.files().delete(stringId).execute();





            return stringId;
        }).addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String s) {

                if(Config.DUPLI_TEST)
                    Log.d("DELETION_CASE", "Deletion :: "+s);
               ListFilesDrive_A();


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if(Config.DUPLI_TEST)
                    Log.d("DELETION_CASE", "Deletion Failed :: "+e);


            }
        });
    }



}
