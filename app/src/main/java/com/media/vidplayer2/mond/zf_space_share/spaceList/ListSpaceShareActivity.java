package com.media.vidplayer2.mond.zf_space_share.spaceList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.media.vidplayer2.mond.R;


import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.ze_LeakManager.IMMLeaks;
import com.media.vidplayer2.mond.zf_space_share.spaceList.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.zf_space_share.spaceList.aa_Recycler.RecyclerAdapter_folder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1.RC_SIGN_IN;

public class ListSpaceShareActivity extends AppCompatActivity {

    public FileListSpaceShareActivity mDriveServiceHelper=null;
    public GoogleAccountCredential credential;
   public GoogleSignInAccount acct;
    @BindView(R.id.contact_recycleView_folder_spacelist)
    public RecyclerView Folder_Frag_recyclerView;
    public List<Directory_folder> total_folder =null;
    public  RecyclerAdapter_folder Folder_Frag_recyclerAdapter=null;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.af_spaceshare_list_folder);



        ButterKnife.bind(this);


       GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

       GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


      //  Intent signInIntent = mGoogleSignInClient.getSignInIntent();
       // startActivityForResult(signInIntent, RC_SIGN_IN);






        credential =
                GoogleAccountCredential.usingOAuth2(
                        this, Collections.singleton(DriveScopes.DRIVE_FILE));



       // AuthCredential credential1 = GoogleAuthProvider.getCredential(acct.getIdToken(), null);


          acct = GoogleSignIn.getLastSignedInAccount(ListSpaceShareActivity.this);

          if(Config.DUPLI_TEST)
              Log.d("DRIVE_UPLOAD_LIST", "Account: "+acct+acct.getEmail()+acct.getIdToken());




         credential.setSelectedAccount(acct.getAccount());
         Drive googleDriveService =
                new Drive.Builder(
                        AndroidHttp.newCompatibleTransport(),
                        new GsonFactory(),
                        credential)
                        .setApplicationName("Drive API Migration")
                        .build();

        // The DriveServiceHelper encapsulates all REST API and SAF functionality.
        // Its instantiation is required before handling any onClick actions.
        mDriveServiceHelper = new FileListSpaceShareActivity(googleDriveService, ListSpaceShareActivity.this);





        //  MainActivity_Start.mainActivityStartS._fileManager_mainActivity.total_folder.clear();

          Folder_Frag_recyclerView.setHasFixedSize(true);
          Folder_Frag_recyclerAdapter = new RecyclerAdapter_folder(  mDriveServiceHelper.list_of_files, ListSpaceShareActivity.this);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ListSpaceShareActivity.this);
        Folder_Frag_recyclerView.setLayoutManager(linearLayoutManager);
         Folder_Frag_recyclerView.setAdapter( Folder_Frag_recyclerAdapter);



        IMMLeaks.fixFocusedViewLeak(getApplication());



    }


    @Override
    public void onResume(){
        super.onResume();
        // put your code here...

      //  mDriveServiceHelper.query();
          mDriveServiceHelper.ListFilesDrive_A();
    }


    private   void printAbout(Drive service) {

        /*
        try {
            About about = service.about().get().execute();

            System.out.println("Current user name: " + about.getStorageQuota().getLimit());

            System.out.println("Root folder ID: " + about.getRootFolderId());
            System.out.println("Total quota (bytes): " + about.getQuotaBytesTotal());
            System.out.println("Used quota (bytes): " + about.getQuotaBytesUsed());
        } catch (IOException e) {
            System.out.println("An error occurred: " + e);
        }*/
    }


}
