package com.media.vidplayer2.mond.zi_Play;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.rubensousa.previewseekbar.PreviewLoader;
import com.github.rubensousa.previewseekbar.exoplayer.PreviewTimeBar;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.util.Util;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;
import com.media.vidplayer2.mond.zc_Glide.GlideApp;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Random;


public class ExoPlayerManager implements PreviewLoader {

    private ExoPlayerMediaSourceBuilder mediaSourceBuilder;
    private PlayerView playerView;
    private ExoPlayer player;
    private PreviewTimeBar previewTimeBar;
    private String thumbnailsUrl;
    private ImageView imageView;
    private Context context;
    private Player.EventListener eventListener = new Player.DefaultEventListener() {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (playbackState == Player.STATE_READY && playWhenReady) {
                previewTimeBar.hidePreview();
            }
        }
    };

    public ExoPlayerManager(PlayerView playerView,
                            PreviewTimeBar previewTimeBar, ImageView imageView,
                            String thumbnailsUrl,ExoPlayer player,Context context) {
        this.playerView = playerView;
        this.imageView = imageView;
        this.previewTimeBar = previewTimeBar;
        this.mediaSourceBuilder = new ExoPlayerMediaSourceBuilder(playerView.getContext());
        this.thumbnailsUrl = thumbnailsUrl;

        this.player= player;
        this.context=context;

        GlideApp.with(context)

                .load(thumbnailsUrl)
                // .apply(new RequestOptions().override(100, 70))

                .thumbnail(0.1f)
                .error(GlideApp.with(context).load(R.drawable.af_thumb_image_missing))
                // .override(150, 150) // resizes the image to these dimensions (in pixel)
                //    .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.

                .into(imageView);

    }

    public void play(Uri uri) {
        mediaSourceBuilder.setUri(uri);
    }

    public void onStart() {
        if (Util.SDK_INT > 23) {
            createPlayers();
        }
    }

    public void onResume() {
        if (Util.SDK_INT <= 23) {
            createPlayers();
        }
    }

    public void onPause() {
        if (Util.SDK_INT <= 23) {
            releasePlayers();
        }
    }

    public void onStop() {
        if (Util.SDK_INT > 23) {
            releasePlayers();
        }
    }

    public void stopPreview(long position) {
        player.seekTo(position);
        player.setPlayWhenReady(true);
    }

    private void releasePlayers() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void createPlayers() {

    }

    private ExoPlayer createFullPlayer() {
        TrackSelection.Factory videoTrackSelectionFactory
                = new AdaptiveTrackSelection.Factory(new DefaultBandwidthMeter());
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();

        player.setPlayWhenReady(true);

        player.addListener(eventListener);
        return player;
    }

    @Override
    public void loadPreview(long currentPosition, long max)
    {
        //player.setPlayWhenReady(false);







        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        Uri myUri = Uri.parse(thumbnailsUrl);
        Bitmap image = retriever.getFrameAtTime(currentPosition, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);

        if(Config.DUPLI_TEST)
            Log.d("SEEKBAR_CHANGED", "loadPreview: "+ new Random().nextInt()+"::"+currentPosition+"::"+thumbnailsUrl+"::"+image+"::"+imageView);


        long interval = currentPosition * 1000;
        RequestOptions options = new RequestOptions().frame(interval);
        Glide.with(context).asBitmap()
                .load(thumbnailsUrl)
                .apply(options)
                .into(imageView);



     //   imageView.setImageResource(R.drawable.exo_icon_next);

/*

        //give YourVideoUrl below
        retriever.setDataSource(thumbnailsUrl, new HashMap<String, String>());
// this gets frame at 2nd second
 //use this bitmap image



        imageView.setImageBitmap(image);
         */


    }
}