package com.media.vidplayer2.mond.zh_friendlist;




import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;

import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_a_chat_user.FireStoreChat.FirestoreChatActivity;
import com.media.vidplayer2.mond.zb_a_chat_user.GetTimeAgo;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;

import java.util.HashMap;

public class FriendListAdapter extends FirestoreRecyclerAdapter<ModelFriendlist, FriendListAdapter.NoteHolder> {

    DocumentReference docRef;
    FutureStudioAppGlideModule futureStudioAppGlideModule ;
    FriendListActvity friendListActvity;

    public  String searchText=null;



    public static String current_id ;
    public static String current_name ;
    public static String current_image;
    public static String current_email;

    public   String current_uid ;


    public FriendListAdapter(@NonNull FirestoreRecyclerOptions<ModelFriendlist> options, FriendListActvity friendListActvity) {
        super(options);
        futureStudioAppGlideModule = new FutureStudioAppGlideModule();
        this.friendListActvity = friendListActvity;

        this.current_uid = FirebaseAuth.getInstance().getUid();

    }

    @Override
    protected void onBindViewHolder(@NonNull NoteHolder holder, int position, @NonNull ModelFriendlist model)
    {
        //holder.textViewTitle.setText(model.getId());
     //   holder.textViewDescription.setText(model.getDescription());
      //  holder.textViewPriority.setText(String.valueOf(model.getPriority()));



        if(friendListActvity.searchfriends)
        {


            boolean isFound = model.getName().toLowerCase().indexOf(searchText.toLowerCase()) !=-1? true: false; //true


            if(isFound || searchText.equals("") )
            {
                holder.itemView.setVisibility(View.VISIBLE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE_X", "onEvent: 2 "+model.getName()+"::"+searchText);

            }

            else
            {
                holder.itemView.setVisibility(View.GONE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));

                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE_X", "onEvent: 3 "+model.getName()+"::"+searchText);

            }

        }
        else {


            if(Config.DUPLI_TEST)
                Log.d("GROUP_ISSUE_X", "onEvent: 4 "+model.getName()+"::"+searchText);



            holder.itemView.setVisibility(View.VISIBLE);
            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }





        if(Config.DUPLI_TEST)
            Log.d("DOWNLOAD_LINK_A_FULL", "other user ::  "+model.getId()+"current user::"+FirebaseAuth.getInstance().getCurrentUser().getUid());












        ListenerRegistration listenerRegistration;



        listenerRegistration= FirebaseFirestore.getInstance().collection("UsersList").document(model.getId()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }




                if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getId()) )
                {
                    //  holder.recycler_item_frdlist.setVisibility(View.GONE);

                    FriendListAdapter.current_id =FirebaseAuth.getInstance().getCurrentUser().getUid();
                    FriendListAdapter.current_name= documentSnapshot.getString("name");
                    FriendListAdapter.current_image= documentSnapshot.getString("thumb_image");
                    FriendListAdapter.current_email= documentSnapshot.getString("email");


                }
                else
                {

                    holder.other_user_id=documentSnapshot.getString("id");
                    holder.other_user_name=documentSnapshot.getString("name");
                    holder.other_user_image=documentSnapshot.getString("thumb_image");
                    holder.other_user_email=documentSnapshot.getString("email");


                }









                String id = documentSnapshot.getString("id");
                String status = documentSnapshot.getString("status");
                String thumb_image =  documentSnapshot.getString("thumb_image");
                String name =  documentSnapshot.getString("name");
                Boolean online =  documentSnapshot.getBoolean("online");

                Timestamp timestamp = documentSnapshot.getTimestamp("time_stamp");

              //  chat.getTimestamp()





                holder.user_single_name.setText(name );
                holder.statusfrd.setText(status );









                if(!thumb_image.equals("default"))
                {
                    futureStudioAppGlideModule.add_image_to_view_link(friendListActvity,holder.user_single_image,thumb_image);

                }




                if( online )
                {
                    holder.user_single_online_icon.setVisibility(View.VISIBLE);
                    holder.time_left.setText("Online");
                }
                else {
                    holder.user_single_online_icon.setVisibility(View.GONE);



                        GetTimeAgo getTimeAgo = new GetTimeAgo();



                    holder.time_left.setText("Online");



                        String lastSeenTime = getTimeAgo.getTimeAgo(timestamp.toDate().getTime(), friendListActvity);

                    holder.time_left.setText(lastSeenTime);






                }



                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A", "status "+status+"thumb_image "+thumb_image+"name :: "+name);

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A", "error "+ e);












            }
        });

        if(friendListActvity.listenersUsers.size()>this.getItemCount())
        {
            friendListActvity.listenersUsers.get(position).remove();
        }

        friendListActvity.listenersUsers.add(listenerRegistration);




        if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getId()) || model.getBlock() || model.getGroup())
        {
          //  holder.recycler_item_frdlist.setVisibility(View.GONE);
            holder.itemView.setVisibility(View.GONE);
            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));

            /*
            holder.itemView.setVisibility(View.VISIBLE);
holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
             */
        }

        else
        {



          ;






            holder.user_single_image.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                    //SendtoAllUsersProfleActivity(user_id);
                    //Log.d("Clicked id", user_id);

                    CharSequence options[] = new CharSequence[]{"Open Profile", "Send an_b_message","Remove","Block"};

                    final AlertDialog.Builder builder = new AlertDialog.Builder(friendListActvity);

                    builder.setTitle("Select Options");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {


                            switch(i)
                            {
                                case 0:

                                    //   Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                                    Intent profileintent = new Intent(friendListActvity, FirestoreChatActivity.class);
                                    profileintent.putExtra("otherprofile", true);
                                    profileintent.putExtra("other_user_id", holder.other_user_id);



                                    friendListActvity.startActivity(profileintent);


                                    break;
                                case 1:


                                    //   Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                                    Intent chatIntent = new Intent(friendListActvity, FirestoreChatActivity.class);
                                    chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                                    chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                                    chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                                    chatIntent.putExtra("current_email", FriendListAdapter.current_email);

                                    chatIntent.putExtra("other_user_id", holder.other_user_id);
                                    chatIntent.putExtra("other_user_name",holder.other_user_name);
                                    chatIntent.putExtra("other_user_image", holder.other_user_image);
                                    chatIntent.putExtra("other_user_email", holder.other_user_email);

                                    friendListActvity.startActivity(chatIntent);


                                    break;
                                case 2:





                                    WriteBatch batch =  FirebaseFirestore.getInstance().batch();


                                    batch.delete(holder.FriendListReference_1);
                                    batch.delete(holder.FriendListReference_2);

                                    batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {


                                        }


                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {





                                        }
                                    });


                                    //docRef.delete();
                                    break;
                                case 3:


                                    HashMap<String, Object> FrduserMap_1 = new HashMap<>();



                                    FrduserMap_1.put("block",  true);
                                    HashMap<String, Object> FrduserMap_2 = new HashMap<>();



                                    FrduserMap_2.put("block",  true);
                                    WriteBatch batch1 =  FirebaseFirestore.getInstance().batch();


                                    batch1.update(holder.FriendListReference_1, FrduserMap_1);
                                    batch1.update(holder.FriendListReference_2, FrduserMap_2);

                                    batch1.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {



                                        }


                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {





                                        }
                                    });

                                    break;
                            }


                        }
                    });

                    builder.show();

                }
            });

            holder.slice.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    //Toast.makeText(UsersActivity.this, String.valueOf(position), Toast.LENGTH_SHORT).show();
                    //SendtoAllUsersProfleActivity(user_id);
                    //Log.d("Clicked id", user_id);


                    Intent intent = friendListActvity.getIntent();
                    intent.putExtra("id", model.getId());
                    intent.putExtra("name", model.getName());
                    friendListActvity.setResult(friendListActvity.RESULT_OK, intent);
                    friendListActvity.finish();

                }
            });
        }




    }


    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.am_friendlist_item,
                parent, false);
        return new NoteHolder(v);
    }

    class NoteHolder extends RecyclerView.ViewHolder {
        ImageView user_single_image;
        TextView user_single_name;
        TextView statusfrd;
        LinearLayout slice;
        TextView time_left;

        LinearLayout recycler_item_frdlist;
        ImageView user_single_online_icon;


        public DocumentReference FriendListReference_1 ;
        public  DocumentReference FriendListReference_2  ;


        public   String other_user_id;
        public   String other_user_name;
        public   String other_user_image;
        public   String other_user_email;


        public NoteHolder(View itemView) {
            super(itemView);
            user_single_image = itemView.findViewById(R.id.user_single_image);
            user_single_online_icon = itemView.findViewById(R.id.user_single_online_icon);
            user_single_name = itemView.findViewById(R.id.user_single_name);
            time_left = itemView.findViewById(R.id.time_left);
            statusfrd = itemView.findViewById(R.id.statusfrd);
            slice = itemView.findViewById(R.id.recycler_item_frdlist);
            recycler_item_frdlist = itemView.findViewById(R.id.recycler_item_frdlist);
        }


    }
}



