package com.media.vidplayer2.mond.zb_b_chat_group.FireStoreChat;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ad_b_group_Profile.MyGroupProfileActivity;
import com.media.vidplayer2.mond.ae_GroupMembers.GroupMembersActivity;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_b_spaceshare_file_selector.SpaceShareChatUploadActivity;

import com.media.vidplayer2.mond.zc_Glide.GlideApp;
import com.media.vidplayer2.mond.zh_friendlist.FriendListActvity;


import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;


public class FirestoreGroupActivity extends AppCompatActivity
        implements FirebaseAuth.AuthStateListener
{
    private static final String TAG = "FirestoreGroupActivity";
    private boolean update_timing = false;
    public int GET_FILE_FOR_SHARE_IN_CHAT = 1;

    public static final int FRIEND_REQUEST_CODE = 1000;

    Boolean blocked=false;





    private CollectionReference SenderChatCollection;



    private DocumentReference SenderChatCollectionD;
    private   DocumentReference ReceiverChatCollectionD;

    Boolean ami_creator=false,ami_admin=false;

    Query SenderChatQuery;
    Query ReceiverChatQuery;
    /** Get the last 50 chat messages ordered by timestamp . */

    /*
    private     Query SenderChatQuery =
            SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING).limit(50);

            */

    static {
        FirebaseFirestore.setLoggingEnabled(true);
    }

    RecyclerView mRecyclerView;
    Button mSendButton ;


    //   @BindView(R.id.messageEdit)

    EditText mMessageEdit ;


    TextView mEmptyListMessage  ;

    String GroupName;
    String CreaterId;


    String other_user_id;
    public String current_id;
    String other_user_name;

    String current_name ;
    String current_image ;
    String other_user_image;


    public String current_email;
    public String other_user_email;

    /*



           mChatToolbar = (Toolbar) findViewById(R.id.chat_app_bar);
     */


    TextView mTitleView;


    TextView mLastSeenView;


    ImageView mProfileImage;

    @BindView(R.id.chat_app_bar)
    Toolbar mChatToolbar;





    private FirebaseAuth mAuth;

    private FirebaseFirestore mFirestore;
    ListenerRegistration UsersList_registration_CurrentList;
    ListenerRegistration GroupMemberstatus_CurrentList;

    @OnClick(R.id.chat_add_btn_upload)
    public void chat_add_btn_upload(View view)
    {



        //  Intent status_intent = new Intent(FirestoreGroupActivity.this, SpaceShareChatUploadActivity.class);
        //  status_intent.putExtra("status_value", status_value);
        //   Log.d("#STOP_DET", "inside status Button ::" +CLOSING);
        ///   startActivity(status_intent);

        Intent i = new Intent(FirestoreGroupActivity.this, SpaceShareChatUploadActivity.class);
        i.putExtra("current_email", current_email);
        i.putExtra("other_user_email", other_user_email);
        startActivityForResult(i, GET_FILE_FOR_SHARE_IN_CHAT);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GET_FILE_FOR_SHARE_IN_CHAT) {
            if (resultCode == RESULT_OK) {

                ;
                String image = data.getStringExtra("image");
                long fileSize = data.getLongExtra("fileSize", 0);

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_TEST", "size: "+fileSize);
                String filename = data.getStringExtra("filename");
                onSendClickI(image,fileSize,filename);
            }
        }



        if (requestCode == FRIEND_REQUEST_CODE  && resultCode  == RESULT_OK) {

            String friendid = data.getStringExtra("id");
            String name = data.getStringExtra("name");
            addfriend(friendid,name);

            if(Config.DUPLI_TEST)
                Log.d("ADD_FRIENDLIST", "onActivityResult: "+friendid);
        }
    }

    public void leavefriend(  )
    {



        WriteBatch batch =  FirebaseFirestore.getInstance().batch();





        // Name, email address, and profile photo Url
        String name  =null;
        String email =null ;
        Uri photoUrl =null ;


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
        {
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)
                String providerId = profile.getProviderId();

                // UID specific to the provider
                String uid = profile.getUid();


                // Name, email address, and profile photo Url
                name = profile.getDisplayName();
                email = profile.getEmail();
                photoUrl = profile.getPhotoUrl();



            }
        }









        ////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////


        DocumentReference GroupAdminNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(CreaterId).collection("GroupMembList-"+GroupName).document(current_id);
        batch.delete(GroupAdminNamesReference);


        //////////////////////////////////////////////////////////////////////





        DocumentReference GroupFriendlistReference = FirebaseFirestore.getInstance().collection("Friends").document(current_id).collection("MyFriends").document(GroupName);;
        //batch.set(GroupFriendlistReference, FrduserMap);

        batch.delete(GroupFriendlistReference);







        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {

                Context context =  getApplicationContext();
                CharSequence text = "Group Created";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                UsersList_registration_CurrentList.remove();
                onBackPressed();

            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



            }
        });




    }
    public void GroupFriends(  )
    {




        Intent intent = new Intent(FirestoreGroupActivity.this, GroupMembersActivity.class);



        intent.putExtra("current_id", current_id);
        intent.putExtra("current_name", current_name);
        intent.putExtra("current_image", current_image);
        intent.putExtra("current_email", current_email);

        intent.putExtra("GroupName", GroupName);
        intent.putExtra("CreaterId", CreaterId);

        intent.putExtra("ami_admin", ami_admin);
        intent.putExtra("ami_creator", ami_creator);


        startActivity(intent);








    }

    public void addfriend(String Friendid,String name1 ){



        WriteBatch batch =  FirebaseFirestore.getInstance().batch();





        // Name, email address, and profile photo Url
        String name;
        String email =null ;
        Uri photoUrl =null ;


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
        {
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)
                String providerId = profile.getProviderId();

                // UID specific to the provider
                String uid = profile.getUid();


                // Name, email address, and profile photo Url
                name = profile.getDisplayName();
                email = profile.getEmail();
                photoUrl = profile.getPhotoUrl();



            }
        }









        ////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////
        HashMap<String, Object> groupmembdata = new HashMap<>();
        //creator id

        groupmembdata.put("name",  name1);
        groupmembdata.put("id",  Friendid);

        groupmembdata.put("creatorid", CreaterId );
        groupmembdata.put("admin",  false);
        groupmembdata.put("weight",  0);
        groupmembdata.put("creator",  false);
        groupmembdata.put("blocked",  false);


        DocumentReference GroupAdminNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(CreaterId).collection("GroupMembList-"+GroupName).document(Friendid);
        batch.set(GroupAdminNamesReference, groupmembdata);


        //////////////////////////////////////////////////////////////////////


        HashMap<String, Object> FrduserMap = new HashMap<>();
        FrduserMap.put("id",CreaterId);
        FrduserMap.put("block",  false);
        FrduserMap.put("time_stamp",   FieldValue.serverTimestamp());
        FrduserMap.put("time_stampN",   FieldValue.serverTimestamp());
        FrduserMap.put("group",  true);
        FrduserMap.put("name",GroupName);



        DocumentReference GroupFriendlistReference = FirebaseFirestore.getInstance().collection("Friends").document(Friendid).collection("MyFriends").document(GroupName);;
        batch.set(GroupFriendlistReference, FrduserMap);







        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {

                Context context =  getApplicationContext();
                CharSequence text = "Group Created";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();




            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



            }
        });




    }



    @Override
    public void onBackPressed()
    {


        Context firestoreChatActivity = (FirestoreGroupActivity)(FirestoreGroupActivity.this);

        HashMap<String, Object> FrduserMap_1 = new HashMap<>();
        HashMap<String, Object> GrpuserMap_2 = new HashMap<>();


        FrduserMap_1.put("isawmessage",   FieldValue.serverTimestamp());
        DocumentReference FriendListReference   =  FirebaseFirestore.getInstance().collection("Friends").document(current_id).collection("MyFriends").document(GroupName);

        GrpuserMap_2.put("someonemessagedyou",   FieldValue.serverTimestamp());
        DocumentReference GroupRef  =  FirebaseFirestore.getInstance().collection("GroupLists").document(CreaterId).collection("GroupNames").document(GroupName);



        WriteBatch batch =  FirebaseFirestore.getInstance().batch();


        batch.update(FriendListReference, FrduserMap_1);




        if(update_timing)
        {


            batch.update(GroupRef, GrpuserMap_2);


        }


         batch.commit();




        super.onBackPressed();
        NavUtils.navigateUpFromSameTask(this);















    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.an_a_activity_chat);
        ButterKnife.bind(this);
        update_timing = false;
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setReverseLayout(true);
        manager.setStackFromEnd(true);




        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(R.layout.an_e_chat_custom_bar, null);
         actionBar.setCustomView(action_bar_view);


        mTitleView = (TextView) action_bar_view.findViewById(R.id.custom_bar_title1);
        mLastSeenView = (TextView) action_bar_view.findViewById(R.id.custom_bar_seen1);
        mProfileImage = (ImageView) action_bar_view.findViewById(R.id.custom_bar_image1);



///////////////////////////////////////////////////////////////////////////////////////////////

        GroupName  = getIntent().getStringExtra("GroupName");
        CreaterId  = getIntent().getStringExtra("CreaterId");



        current_id = getIntent().getStringExtra("current_id");
        current_name = getIntent().getStringExtra("current_name");
        current_image = getIntent().getStringExtra("current_image");
        current_email = getIntent().getStringExtra("current_email");


        registerForContextMenu(mProfileImage);

        if(Config.DUPLI_TEST)
            Log.d("CHAT_CASE", current_id+" : "+current_name+":"+current_image+":"+current_email);



        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();



       if(Config.DUPLI_TEST)
           Log.d("PATH_ISSUE_CASE", "onCreate: "+CreaterId+"::"+GroupName);


        SenderChatCollection =
                FirebaseFirestore.getInstance().collection("GroupChats").document(CreaterId).collection(GroupName);





        SenderChatQuery =
                SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING);
        ReceiverChatQuery =
                SenderChatCollection.orderBy("timestamp", Query.Direction.DESCENDING);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //  @BindView(R.id.messagesList)
        mRecyclerView = (RecyclerView)findViewById(R.id.messagesList);


        RecyclerView.ItemAnimator animator = mRecyclerView.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        // @BindView(R.id.sendButton)
        mSendButton = (Button)findViewById(R.id.sendButton);


        //   @BindView(R.id.messageEdit)

        mMessageEdit = (EditText)findViewById(R.id.messageEdit);


        //mEmptyListMessage = (TextView)findViewById(R.id.emptyTextView);


        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(manager);

        mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int left, int top, int right, int bottom,
                                       int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    mRecyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mRecyclerView.smoothScrollToPosition(0);
                        }
                    }, 100);
                }
            }
        });



        mMessageEdit.setClickable(true);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendClick();
                update_timing= true;
            }
        });



        Map<String, Object> UserStat = new HashMap<>();
        UserStat.put("seen", true);




    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);





        menu.add(0, v.getId(), 0, "Add Friend");
        menu.add(0, v.getId(), 0, "Group Members");
        menu.add(0, v.getId(), 0, "Profile");
        menu.add(0, v.getId(), 0, "Leave Group");

        //if(current_id != CreaterId)


       // menu.setHeaderTitle("Context Menu");


    }

    public void SendtoSettingsActivity(Boolean otherprofile){

        Intent intent = new Intent(this, MyGroupProfileActivity.class);

        intent.putExtra("current_id", current_id);
        intent.putExtra("current_name", current_name);
        intent.putExtra("current_image", current_image);
        intent.putExtra("current_email", current_email);

        intent.putExtra("GroupName", GroupName);
        intent.putExtra("CreaterId", CreaterId);

        intent.putExtra("ami_admin", ami_admin);
        intent.putExtra("ami_creator", ami_creator);
        if(otherprofile)
            intent.putExtra("otherprofile", true);
        startActivity(intent);



    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Toast.makeText(this, "Selected Item: " +item.getTitle(), Toast.LENGTH_SHORT).show();


        if(item.getTitle().equals("Profile"))
        {


            if(ami_creator || ami_admin)
            {
                SendtoSettingsActivity(false);
            }
            else {

                SendtoSettingsActivity(true);
                Toasty.error(FirestoreGroupActivity.this, "Only admin can add memebers" ).show();

            }

        }

         if(item.getTitle().equals("Add Friend"))
         {

             if(ami_creator || ami_admin)
             {
                 Intent intent = new Intent(FirestoreGroupActivity.this, FriendListActvity.class);
                 startActivityForResult(intent , FRIEND_REQUEST_CODE);
             }
             else {
                 Toasty.error(FirestoreGroupActivity.this, "Only admin can add memebers" ).show();

             }


         }


        if(item.getTitle().equals("Leave Group"))
        {
            leavefriend();
        }


        if(item.getTitle().equals("Group Members"))
        {
            GroupFriends();
        }


        return true;
    }




    @Override
    public void onStart() {
        super.onStart();



        GroupMemberstatus_CurrentList = mFirestore.collection("GroupLists").document(CreaterId).collection("GroupMembList-"+GroupName).document(current_id).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }




                blocked     = documentSnapshot.getBoolean("blocked");
                ami_creator = documentSnapshot.getBoolean("creator");

                ami_admin = documentSnapshot.getBoolean("admin");


                if(blocked)
                {
                    Toasty.error(FirestoreGroupActivity.this, "You are blocked" ).show();


                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            onBackPressed();
                        }
                    }, 500);



                }




            }
        });



        UsersList_registration_CurrentList = mFirestore.collection("GroupLists").document(CreaterId).collection("GroupNames").document(GroupName).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }


                Boolean freeze = documentSnapshot.getBoolean("freeze");
                if(freeze)
                {
                    Toasty.error(FirestoreGroupActivity.this, "Admin disabled group" ).show();


                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            onBackPressed();
                        }
                    }, 500);



                }

                String name = documentSnapshot.getString("name");
                String creator = documentSnapshot.getString("creator");
                String status = documentSnapshot.getString("status");
                String image = documentSnapshot.getString("thumb_image");

                mLastSeenView.setVisibility(View.GONE);
                mTitleView.setText(name);









                if(!image.equals("default"))
                {


                   // GlideApp.with(FirestoreGroupActivity.this).load(image).into(mProfileImage);


                }


                // ...
            }
        });


        if (isSignedIn()) { attachRecyclerViewAdapter(); }
        FirebaseAuth.getInstance().addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FirebaseAuth.getInstance().removeAuthStateListener(this);
        UsersList_registration_CurrentList.remove();
        GroupMemberstatus_CurrentList.remove();

        mRecyclerView.setAdapter(null);
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth auth) {
        mSendButton.setEnabled(isSignedIn());
        mMessageEdit.setEnabled(isSignedIn());

        if (isSignedIn()) {
            attachRecyclerViewAdapter();
        } else {
            Toast.makeText(this, R.string.signing_in, Toast.LENGTH_SHORT).show();
            //  auth.signInAnonymously().addOnCompleteListener(new SignInResultNotifier(this));
        }
    }

    private boolean isSignedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    private void attachRecyclerViewAdapter() {
        final RecyclerView.Adapter adapter = newAdapter();

        // Scroll to bottom on new messages
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                mRecyclerView.smoothScrollToPosition(0);
            }
        });

        mRecyclerView.setAdapter(adapter);
    }





    public void onSendClick() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String name = current_name;

        onAddMessage(new Chat(name, mMessageEdit.getText().toString(), uid, FieldValue.serverTimestamp().toString()));

        mMessageEdit.setText("");
    }

    public void onSendClickI(String Image,long fileSize,String filename) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String name = current_name;
        mMessageEdit.setText(filename);
        onAddMessage(new Chat(name, mMessageEdit.getText().toString(), uid, FieldValue.serverTimestamp().toString(),Image,fileSize,filename));


    }

    @NonNull
    private RecyclerView.Adapter newAdapter() {
        FirestoreRecyclerOptions<Chat> options =
                new FirestoreRecyclerOptions.Builder<Chat>()
                        .setQuery(SenderChatQuery, Chat.class)
                        .setLifecycleOwner(this)
                        .build();

        return new FirestoreRecyclerAdapter<Chat, ChatHolder>(options) {
            @NonNull
            @Override
            public ChatHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


                ChatHolder chatHolder = new ChatHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.an_b_message, parent, false));
                chatHolder.firestoreGroupActivity= FirestoreGroupActivity.this;

                return chatHolder;
            }

            @Override
            protected void onBindViewHolder(@NonNull ChatHolder holder, int position, @NonNull Chat model) {
                holder.bind(model);
                // holder.firestoreGroupActivity=FirestoreGroupActivity.this;


            }

            @Override
            public void onDataChanged() {
                // If there are no chat messages, show a view that invites the user to add a an_b_message.
                //  mEmptyListMessage.setVisibility(getItemCount() == 0 ? View.VISIBLE : View.GONE);
            }
        };
    }


    private void onAddMessage(@NonNull Chat chat) {



        WriteBatch batch =  FirebaseFirestore.getInstance().batch();



        if(Config.CHAT_TEST)
            Log.d("SEND_MSG", "sendMessage:  1");

        Map<String, Object> chat_status_1 = new HashMap<>();
        chat_status_1.put("an_b_message",chat.getMessage());
        chat_status_1.put("name",chat.getName());
        chat_status_1.put("mTimestamp",   FieldValue.serverTimestamp());
        chat_status_1.put("uid",chat.getUid());
        chat_status_1.put("TimeStampStr",chat.getTimeStampStr());



        //batch.set(SenderChatCollectionD, chat_status_1);
        //  batch.set(ReceiverChatCollectionD, chat_status_1);


        batch.set(SenderChatCollection.document(), chat);
//        batch.set(ReceiverChatCollection.document(), chat);

        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                mMessageEdit.setText("");


                Toast.makeText(FirestoreGroupActivity.this, "Your Message sent", Toast.LENGTH_SHORT).show();

                if(Config.CHAT_TEST)
                    Log.d("SEND_MSG", "sendMessage from batch:  ");


            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



                if(Config.CHAT_TEST)
                    Log.d("SEND_MSG", "sendMessage from batch :: "+ e.getMessage());

            }
        });





    }
}
