package com.media.vidplayer2.mond.zf_space_share.spaceList.ab_bottomsheet;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.About;

import java.io.IOException;
// ...

public class MyClass {

    // ...

    /**
     * Print information about the current user along with the Drive API
     * settings.
     *
     * @param service Drive API service instance.
     */
    private static void printAbout(Drive service) {
        try {
            About about = service.about().get().execute();


            System.out.println("Root folder ID: " + about.getStorageQuota());

        } catch (IOException e) {
            System.out.println("An error occurred: " + e);
        }
    }

    // ...
}