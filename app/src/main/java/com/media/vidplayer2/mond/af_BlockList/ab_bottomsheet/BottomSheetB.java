package com.media.vidplayer2.mond.af_BlockList.ab_bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments.FriendListAdapter;
import com.media.vidplayer2.mond.ad_b_group_Profile.MyGroupProfileActivity;
import com.media.vidplayer2.mond.af_BlockList.BlockListAdapter;
import com.media.vidplayer2.mond.af_BlockList.BlockMembersActivity;
import com.media.vidplayer2.mond.af_BlockList.ModelBlocklist;

import java.util.ArrayList;
import java.util.HashMap;

import es.dmoral.toasty.Toasty;

public class BottomSheetB extends BottomSheetDialog {

    private  BlockListAdapter.NoteHolder holder;
    private  Boolean group;
    private BlockMembersActivity blockMembersActivity;


    BottomSheetB bottomSheetB;


    ModelBlocklist memberdata=null;


    //((HomeActivity)context)


    public BottomSheetB(@NonNull BlockMembersActivity blockMembersActivity, ModelBlocklist memberdata) {
        super(blockMembersActivity);
        this.blockMembersActivity = blockMembersActivity;
        this.memberdata=memberdata;

        bottomSheetB =this;


    }

    public BottomSheetB(@NonNull Context context, int theme) {
        super(context, theme);
    }

    protected BottomSheetB(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public BottomSheetB(@NonNull BlockMembersActivity homeactivity, ModelBlocklist memberdata, Boolean group, BlockListAdapter.NoteHolder holder) {
        super(homeactivity);
        this.blockMembersActivity = homeactivity;
        this.memberdata=memberdata;
        this.group = group;
        this.holder=holder;

        bottomSheetB=this;


    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = getLayoutInflater().inflate(R.layout.ag_bottom_sheet, null);
        setContentView(view);

        ArrayList<Item> items=new ArrayList<>();




        if(memberdata.getGroup())
        {

            items.add( new Item(R.drawable.aa_crown, "Unfreeze Group") );


        }
        else
        {



            items.add( new Item(R.drawable.aa_crown, "Unblock User") );










        }







        ItemAdapter adapter = new ItemAdapter(blockMembersActivity, items );

        //ListView for the items
       ListView listView = (ListView) view.findViewById(R.id.list_items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               // Intent it = null;

                switch(position){




                    case 0:
                        bottomSheetB.dismiss();


                        if(memberdata.getGroup())
                        {
                            HashMap<String, Object> groupdata = new HashMap<>();

                            //creator id

                            groupdata.put("freeze",  false);


                            WriteBatch batch =  FirebaseFirestore.getInstance().batch();

                            DocumentReference GroupNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection("GroupNames").document(memberdata.getName());
                            batch.update(GroupNamesReference, groupdata);


                            HashMap<String, Object> FrduserMap = new HashMap<>();

                            FrduserMap.put("block",  false);




                            DocumentReference GroupFriendlistReference = FirebaseFirestore.getInstance().collection("Friends").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection("MyFriends").document(memberdata.getName());;
                            batch.update(GroupFriendlistReference, FrduserMap);





                            DocumentReference Freze = FirebaseFirestore.getInstance().collection("GroupLists").document(FriendListAdapter.current_id).collection("Group_User_Freeze_List").document(memberdata.getName());

                            batch.delete(Freze);


                            batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid)
                                {


                                    CharSequence text = "Group is unfreezed. Open friendlist  again";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(blockMembersActivity, text, duration);
                                    toast.show();



                                }


                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {



                                }
                            });

                        }
                        else
                        {

                            HashMap<String, Object> FrduserMap_1 = new HashMap<>();




                            DocumentReference FriendListReference_1 = FirebaseFirestore.getInstance().collection("Friends").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection("MyFriends").document( holder.other_user_id);

                            DocumentReference FriendListReference_2 = FirebaseFirestore.getInstance().collection("Friends").document( holder.other_user_id).collection("MyFriends").document(FirebaseAuth.getInstance().getCurrentUser().getUid());



                            FrduserMap_1.put("block", false);







                            WriteBatch batch =  FirebaseFirestore.getInstance().batch();
                            batch.update(FriendListReference_1, FrduserMap_1);
                            batch.update(FriendListReference_2, FrduserMap_1);



                            HashMap<String, Object> FMap = new HashMap<>();



                            DocumentReference Freze = FirebaseFirestore.getInstance().collection("GroupLists").document(FirebaseAuth.getInstance().getCurrentUser().getUid()).collection("Group_User_Freeze_List").document(memberdata.getName());

                            batch.delete(Freze);



                            batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid)
                                {


                                    CharSequence text = "User unBlocked Visit Friend list ";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(blockMembersActivity, text, duration);
                                    toast.show();



                                }


                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {



                                }
                            });

                        }






                        break;

                    case 1:
                        bottomSheetB.dismiss();


                        break;
                    case 2:

                        bottomSheetB.dismiss();




                        break;
                    case 6:

                        break;
                }

              //  context.startActivity(it);
            }
        });



    }


}
