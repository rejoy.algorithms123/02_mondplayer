package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments;
public class ModelFriendlist {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    private Boolean block;

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    private Boolean group;
    public ModelFriendlist() {
        //empty constructor needed
    }


}
