package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.zb_Config.Config;

import com.media.vidplayer2.mond.zh_friendlist.ModelFriendlist;

import java.util.ArrayList;


public class FriendListFragment extends Fragment {





    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference FriendsList=null;
    public FriendListAdapter adapter;
    RecyclerView recyclerView;




    public ArrayList<ListenerRegistration> listenersUsers;
    public ArrayList<ListenerRegistration> listenersNewMessage;
    public ArrayList<Task<QuerySnapshot>> listoftaskschataccess;


    public FriendListFragment()
    {

    }


    public static final String                                                ARG_PAGE = "ARG_PAGE";
    private int                                                                               mPage;

    public HomeActivity homeActivity;





    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);

        homeActivity = ((HomeActivity) getActivity());
        listenersUsers = new ArrayList<ListenerRegistration>();
        listenersNewMessage = new ArrayList<ListenerRegistration>();
        listoftaskschataccess = new ArrayList<Task<QuerySnapshot>>();

        if (Config.DUPLI_TEST)
        {
            Log.d("_AFRAG_SC_CRE_", "onCreate space fragment  : aa_activity_home_screen__ " );

        }




    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {









        if (Config.DUPLI_TEST) {
           Log.d("_AFRAG_SC_VIEW_", " onCreateView space fragment  : aa_activity_home_screen__A " + mPage);

        }



        View rootView = inflater.inflate(R.layout.al_fragment_friendlist, container, false);



       // ((HomeActivity)getActivity()).friendListFileManager.setUpVIEW(rootView);

        //((HomeActivity)getActivity()).friendListFileManager.setUpRecyclerView();

        recyclerView = rootView.findViewById(R.id.recycler_viewF);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);














        if (Config.DUPLI_TEST) {
            Log.d("_AFRAG_SC_VIEW_", " onCreateView space fragment  : aa_activity_home_screen " );

        }



       // setUpVIEW(rootView);

       // setUpRecyclerView();



        setUpRecyclerView();

        homeActivity.fileManager_homeActivity.frdlistfileManager.linearLayoutfavsearchmain = (LinearLayout) rootView.findViewById(R.id.searchviewflistf);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.iv_clear_textf);





        EditText edtSearchText = (EditText) rootView.findViewById(R.id.edt_search_textf);



        imageView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                edtSearchText.setText("");
                homeActivity.fileManager_homeActivity.favFileManager.searchButton.setVisibility(View.VISIBLE);
                homeActivity.fileManager_homeActivity.frdlistfileManager.linearLayoutfavsearchmain.setVisibility(View.GONE);
                homeActivity.fileManager_homeActivity.frdlistfileManager.searchfriends=false;
            }
        });




        /*search btn clicked*/
        edtSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return false;
            }
        });

        /*hide/show clear button in search view*/



        TextWatcher searchViewTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



                if(s.toString().trim().length()==0){
                    //  ivClearText.setVisibility(View.GONE);
                } else {
                    //ivClearText.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {



                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if(Config.DUPLI_TEST)
                    Log.d("SEARCH_WISE", "onTextChanged: "+s);
                new  com.media.vidplayer2.mond.zc_VideoFileParser.Util();


                if(s.toString().equals(""))
                {
                    homeActivity.friendListFileManager.searchfriends=false;
                    if(Config.DUPLI_TEST)
                        Log.d("SEARCH_WISEG", "onTextChanged 1 : "+s);


                   // com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_firstTime(homeActivity,homeActivity);

                }
                else
                {

                           adapter.searchText =s.toString();
                            homeActivity.friendListFileManager.searchfriends=true;
                    if(Config.DUPLI_TEST)
                        Log.d("YES_CASE", "onTextChanged 2: "+s+"::"+ homeActivity.fileManager_homeActivity.frdlistfileManager.searchfriends);

                  //  com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_firstTimeSearch(homeActivity,homeActivity,s.toString());

                }




                 adapter.notifyDataSetChanged();




                //homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_updateUIRecyclerK();


            }
        };

        edtSearchText.addTextChangedListener(searchViewTextWatcher);












        return rootView;
    }


    public  void setUpRecyclerView() {
        //Query query = FriendsList.orderBy("priority", Query.Direction.DESCENDING);


        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
            FriendsList = db.collection("Friends").document(FirebaseAuth.getInstance().getUid()).collection("MyFriends");

            Query query = FriendsList.orderBy("time_stamp", Query.Direction.DESCENDING);
            FirestoreRecyclerOptions<ModelFriendlist> options = new FirestoreRecyclerOptions.Builder<ModelFriendlist>()
                    .setQuery(query, ModelFriendlist.class)
                    .build();

            adapter = new FriendListAdapter(options,this);

            recyclerView.setAdapter(adapter);
            adapter.startListening();

        }





    }

    public void stopFlist()
    {

        if(FirebaseAuth.getInstance().getCurrentUser()!=null) {

            if(adapter!=null)
            {

                adapter.stopListening();

                for (int i = 0; i < listenersUsers.size(); i++)
                    if (listenersUsers.get(i) != null)
                        listenersUsers.get(i).remove();






                listenersUsers.clear();


                for (int i = 0; i < listenersNewMessage.size(); i++)
                    if (listenersNewMessage.get(i) != null)
                        listenersNewMessage.get(i).remove();


                listenersNewMessage.clear();


                for (int i = 0; i <  listoftaskschataccess.size(); i++)
                    if (listoftaskschataccess.get(i) != null)
                        listoftaskschataccess.get(i).addOnSuccessListener(null);


                listoftaskschataccess.clear();



                FriendsList=null;

               // adapter=null;
            }


        }

    }




    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {


        if(Config.DUPLI_TEST)
            Log.d("_AFRAG_SC_VIEW_CTED", " After Fragment Space View Creation --  homeActivity " );

    }



    @Override
    public void onDestroyView() {



        stopFlist();



        if(adapter!=null)
        {
            adapter.stopListening();
            recyclerView.setAdapter(null);
            adapter=null;
            recyclerView=null;
        }
        super.onDestroyView();


        if(Config.DUPLI_TEST)
        {
            Log.d("_AFRAG_SPC_DEST_", "Destroy executed from  Space -- home activity");

        }

       // stopFlist();

    }


    @Override
    public void onStart() {
        super.onStart();



    }

    @Override
    public void onResume() {
        super.onResume();

    }

// Fragment is active




    @Override
    public void onStop() {
        super.onStop();


        if(Config.DUPLI_TEST)
        {
            Log.d("_AFRAG_SPC_STOP_", "onStop executed from  Space -- home activity");

        }


    }


}
