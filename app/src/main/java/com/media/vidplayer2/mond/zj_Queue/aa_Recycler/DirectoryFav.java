package com.media.vidplayer2.mond.zj_Queue.aa_Recycler;

public class DirectoryFav {


    public DirectoryFav(String name, Long size, Long progress) {
        this.name = name;
        this.size = size;
        this.progress = progress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getProgress() {
        return progress;
    }

    public void setProgress(Long progress) {
        this.progress = progress;
    }

    public String name;
    public long size;
    public long progress;
}
