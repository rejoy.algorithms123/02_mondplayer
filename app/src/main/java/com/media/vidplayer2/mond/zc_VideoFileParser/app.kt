package com.media.vidplayer2.mond.zc_VideoFileParser

import com.greentoad.turtlebody.mediapicker.core.CursorHelper
import com.greentoad.turtlebody.mediapicker.core.MediaConstants



import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.greentoad.turtlebody.mediapicker.MediaPicker
import com.greentoad.turtlebody.mediapicker.core.FileManager
import com.greentoad.turtlebody.mediapicker.ui.component.folder.image_video.ImageVideoFolder
import com.greentoad.turtlebody.mediapicker.ui.component.media.video.VideoModel
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler.DirectoryFav
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.Directory_folder
import com.media.vidplayer2.mond.zb_Config.Config
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*


class Util
fun getVideoFilesInFolder(context: Context, folderId: String): ArrayList<VideoModel> {
    val fileItems = ArrayList<VideoModel>()

    val VIDEO_FILE1 = arrayOf(
            MediaStore.Video.Media._ID,
            MediaStore.Video.Media.DISPLAY_NAME,
            MediaStore.Video.Media.SIZE,
            MediaStore.Video.Media.DATA,
            MediaStore.Video.Media.BUCKET_ID,
            MediaStore.Video.Thumbnails.DATA,
            MediaStore.Video.Media.DURATION,
            MediaStore.Video.Media.DURATION)

   // val projection = VIDEO_FILE1
    val projection = MediaConstants.Projection.VIDEO_FILE

    //Create the cursor pointing to the SDCard
    val cursor: Cursor? = CursorHelper.getImageVideoFileCursor(context, folderId, MediaPicker.MediaTypes.VIDEO)

    cursor?.let {
        val columnIndexFileId = it.getColumnIndexOrThrow(projection[0])
        val columnIndexFileName = it.getColumnIndexOrThrow(projection[1])
        val columnIndexFileSize = it.getColumnIndexOrThrow(projection[2])
        val columnIndexFilePath = it.getColumnIndexOrThrow(projection[3])
        val columnIndexFileThumbPath = it.getColumnIndexOrThrow(projection[5])
        val columnIndexFileDuration = it.getColumnIndexOrThrow(projection[6])

        val columnIndexcontenturi = it.getColumnIndexOrThrow(projection[7])




        while (it.moveToNext()) {
            val fileItem = VideoModel(it.getString(columnIndexFileId),
                    it.getString(columnIndexFileName),
                    it.getInt(columnIndexFileSize),
                    it.getString(columnIndexFilePath),
                    it.getString(columnIndexcontenturi),
                  //  it.getString(columnIndexFileThumbPath),
                    it.getString(columnIndexFileDuration), false)
            fileItems.add(fileItem)
        }
        cursor.close()
    }
    return fileItems
}



fun fetchImageVideoFolders_fromFolder(context: Context,homeactivity:HomeActivity , folderId: String,file: Boolean,filename:String)
{
    val bucketFetch: Single<ArrayList<VideoModel>> =

            Single.fromCallable<ArrayList<VideoModel>> { FileManager.getVideoFilesInFolder(context!!,folderId) }


    bucketFetch
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<ArrayList<VideoModel>> {

                override fun onSubscribe(@NonNull d: Disposable)
                {

                }

                override fun onSuccess(@NonNull imageVideoFolders: ArrayList<VideoModel>)
                {

                    VideoFileParser.getInstance().total_folder.clear()
                    for (i in imageVideoFolders.indices)
                    {






                        val directory_folder =  Directory_folder(imageVideoFolders[i].filePath,imageVideoFolders[i].size.toString(),imageVideoFolders[i].name)

                        directory_folder.setID((imageVideoFolders[i].id).toInt())


                        if(file)
                        {
                            directory_folder.setFrom_search(true)

                        }
                        else
                        {
                            directory_folder.setFrom_search(false)
                        }

                        VideoFileParser.getInstance().total_folder.add(  directory_folder)


                        if(imageVideoFolders[i].name.equals(filename) && file)
                        {
                             Collections.swap(VideoFileParser.getInstance().total_folder, 0, i);

                        }

                        if(Config.DUPLI_TEST)

                            Log.d("FILE_FETCH_D",imageVideoFolders[i].name+"::"+imageVideoFolders[i].filePath+"::"+imageVideoFolders[i].thumbnailPath+"::"+VideoFileParser.getInstance().total_folder.size)




                    }



                     (context as HomeActivity).fileManager_homeActivity.folderFileManager.Folder_Frag_updateUIRecyclerK()


                }

                override fun onError(@NonNull e: Throwable)
                {

                }
            })
}




 fun fetchImageVideoFolders_secondTime(context: Context)
{
    val bucketFetch: Single<ArrayList<ImageVideoFolder>> =

                Single.fromCallable<ArrayList<ImageVideoFolder>> { FileManager.fetchVideoFolders(context!!) }
 

    bucketFetch
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<ArrayList<ImageVideoFolder>> {

                override fun onSubscribe(@NonNull d: Disposable)
                {

                }

                override fun onSuccess(@NonNull imageVideoFolders: ArrayList<ImageVideoFolder>)
                {

                    VideoFileParser.getInstance().total_SD_MEM.clear()
                    for (i in imageVideoFolders.indices) {

                        var delimiter = "/"

                        val parts = imageVideoFolders[i].coverImageFilePath.split(delimiter)

                        val oldValue =  "/"+parts[parts.size-1]
                        val newValue = ""



                        var count : Int = imageVideoFolders[i].contentCount
                        var counts: String
                           counts = count.toString()
                        val output = imageVideoFolders[i].coverImageFilePath.replace(oldValue, newValue)


                        val directoryFav = DirectoryFav(imageVideoFolders[i].name,  imageVideoFolders[i].id.toString(), counts    )

                        directoryFav.setFile(false)

                        directoryFav.setIDI(imageVideoFolders[i].id.toInt())
                        directoryFav.setFile_name(parts[parts.size-1].toString())
                        VideoFileParser.getInstance().total_SD_MEM.add(  directoryFav)





                        // var str = "Kotlin TutorialsepTutorial KartsepExamples"

                        /*



                        //var str = "Kotlin Tutorial - Replace String - Programs"



*/
                     //   if(Config.DUPLI_TEST)
                       // Log.d("FILE_FETCHF","id -- "+imageVideoFolders[i].id+"::ID--"+imageVideoFolders[i].coverImageFilePath+"::name--"+imageVideoFolders[i].name+"::output--"+output+"::size--"+VideoFileParser.getInstance().total_SD_MEM.size)




                    }
                    val intent = Intent(context, HomeActivity::class.java)

                    context.startActivity(intent)
                }

                override fun onError(@NonNull e: Throwable)
                {

                }
            })
}


fun fetchImageVideoFolders_firstTime(context: Context,homeactivity:HomeActivity )
{
    val bucketFetch: Single<ArrayList<ImageVideoFolder>> =

            Single.fromCallable<ArrayList<ImageVideoFolder>> { FileManager.fetchVideoFolders(context!!) }


    bucketFetch
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<ArrayList<ImageVideoFolder>> {

                override fun onSubscribe(@NonNull d: Disposable)
                {

                }

                override fun onSuccess(@NonNull imageVideoFolders: ArrayList<ImageVideoFolder>)
                {

                    VideoFileParser.getInstance().total_SD_MEM.clear()
                    for (i in imageVideoFolders.indices) {

                        var delimiter = "/"

                        val parts = imageVideoFolders[i].coverImageFilePath.split(delimiter)

                        val oldValue =  "/"+parts[parts.size-1]
                        val newValue = ""


                        var count : Int = imageVideoFolders[i].contentCount
                        var counts: String
                        counts = count.toString()
                        val output = imageVideoFolders[i].coverImageFilePath.replace(oldValue, newValue)

                        VideoFileParser.getInstance().total_SD_MEM.add(  DirectoryFav(imageVideoFolders[i].name,  imageVideoFolders[i].id.toString(), counts    ))


                        // var str = "Kotlin TutorialsepTutorial KartsepExamples"

                        /*



                        //var str = "Kotlin Tutorial - Replace String - Programs"



*/

                        if(Config.DUPLI_TEST)

                        Log.d("FILE_SEARCH_CASE",imageVideoFolders[i].coverImageFilePath+"::"+imageVideoFolders[i].name+"::"+output+"::"+VideoFileParser.getInstance().total_SD_MEM.size)




                    }


                       //total_SD_MEM.addAll(names_fav_SD);
                    (context as HomeActivity).fileManager_homeActivity.favFileManager.Fav_Frag_updateUIRecycler()

                    if(Config.DUPLI_TEST)
                        Log.d("FILE_SEARCH_CASE",  "Done search folder")

                }

                override fun onError(@NonNull e: Throwable)
                {

                }
            })
}


fun fetchVideoFoldersDD(context: Context,searchstring:String): ArrayList<VideoModelS>
{
    val folders = ArrayList<ImageVideoFolder>()
    val folderFileCountMap = HashMap<String, Int>()

    val projection = MediaConstants.Projection.VIDEO_FOLDER

    //Create the cursor pointing to the SDCard
    val cursor = CursorHelper.getImageVideoFolderCursor(context, MediaPicker.MediaTypes.VIDEO)

    val fileItems = ArrayList<VideoModelS>()

    cursor?.let {
        val columnIndexFolderId = it.getColumnIndexOrThrow(projection[0])
        val columnIndexFolderName = it.getColumnIndexOrThrow(projection[1])
        val columnIndexFilePath = it.getColumnIndexOrThrow(projection[2])

        while (it.moveToNext())
        {
            val folderId = it.getString(columnIndexFolderId)
            val name = it.getString(columnIndexFolderName)
            val path = it.getString(columnIndexFilePath)




            if (folderFileCountMap.containsKey(folderId))
            {


            }
            else
            {
                folderFileCountMap[folderId] = 1
                if(Config.DUPLI_TEST)
                {
                    //  Log.d("FILE_SEARCH_CASE",  "Folder Name :: "+name +"::"+folderId+"::"+ID)

                    Log.d("FILE_SEARCH_CASE",  "Folder Name :: "+name +"/::"+folderId+"/::"+path)

                }


                val projection = MediaConstants.Projection.VIDEO_FILE

                //Create the cursor pointing to the SDCard
                val cursor1: Cursor? = CursorHelper.getImageVideoFileCursor(context, folderId, MediaPicker.MediaTypes.VIDEO)

                cursor1?.let {
                    val columnIndexFileId = it.getColumnIndexOrThrow(projection[0])
                    val columnIndexFileName = it.getColumnIndexOrThrow(projection[1])
                    val columnIndexFileSize = it.getColumnIndexOrThrow(projection[2])
                    val columnIndexFilePath = it.getColumnIndexOrThrow(projection[3])
                    val columnIndexFileThumbPath = it.getColumnIndexOrThrow(projection[5])
                    val columnIndexFileDuration = it.getColumnIndexOrThrow(projection[6])

                    while (it.moveToNext())
                    {
                        val fileItem = VideoModelS(it.getString(columnIndexFileId),
                                it.getString(columnIndexFileName),
                                it.getInt(columnIndexFileSize),
                                it.getString(columnIndexFilePath),
                                it.getString(columnIndexFileThumbPath),
                                it.getString(columnIndexFileDuration), false,folderId)

                        if(Config.DUPLI_TEST)
                            Log.d("FILE_SEARCH_CASE",  fileItem.filePath+"::"+fileItem.name+"::"+searchstring)


                        val normal = fileItem.name
                        val lower = fileItem.name.toLowerCase()
                        val upper = fileItem.name.toUpperCase()


                        if (normal.contains(searchstring.toString()) || lower.contains(searchstring.toString()) || upper.contains(searchstring.toString()))
                        {
                            fileItems.add(fileItem)
                            if(Config.DUPLI_TEST)
                                Log.d("FILE_SEARCH_CASE",  "Hit")

                        }






                    }
                    cursor1.close()
                }
            }








            }
        for (fdr in folders)
        {
            fdr.contentCount = folderFileCountMap[fdr.id]!!
        }
        cursor.close()



    }
    return fileItems
}

fun fetchImageVideoFolders_firstTimeSearch(context: Context,homeactivity:HomeActivity,searchstring:String )
{
    val bucketFetch: Single<ArrayList<VideoModelS>> =

            Single.fromCallable<ArrayList<VideoModelS>> {  fetchVideoFoldersDD(context!!,searchstring) }


    bucketFetch
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<ArrayList<VideoModelS>> {

                override fun onSubscribe(@NonNull d: Disposable)
                {

                }

                override fun onSuccess(@NonNull imageVideoFolders: ArrayList<VideoModelS>)
                {

                    VideoFileParser.getInstance().total_SD_MEM.clear()
                    for (i in imageVideoFolders.indices)
                    {



                        var delimiter = "/"

                        val parts = imageVideoFolders[i].filePath.split(delimiter)

                        val oldValue =  "/"+parts[parts.size-1]
                        val newValue = ""


                        var count : Int = 1
                        var counts: String
                        counts = count.toString()
                        val output = imageVideoFolders[i].filePath.replace(oldValue, newValue)

                        val directoryFav = DirectoryFav(imageVideoFolders[i].name,  imageVideoFolders[i].fid.toString(), counts    )

                        directoryFav.setFile(true)
                        directoryFav.setFile_name(parts[parts.size-1].toString())
                        VideoFileParser.getInstance().total_SD_MEM.add(  directoryFav)



                    }

                    if(Config.DUPLI_TEST)
                        Log.d("FILE_SEARCH_CASE",  "Done search file")


                  //  VideoFileParser.getInstance().total_SD_MEM.addAll(VideoFileParser.getInstance().total_SD_MEM);
                    (context as HomeActivity).fileManager_homeActivity.favFileManager.Fav_Frag_updateUIRecycler()


                }

                override fun onError(@NonNull e: Throwable)
                {

                }
            })
}


