package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.ab_bottomsheet.BottomSheet;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;


import java.io.File;
import java.util.List;

import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;

public class RecyclerAdapter_folder extends RecyclerView.Adapter<ViewHolder_folder>
{

    private List<Directory_folder> directories;
    private Context context;
    private  HomeActivity homeActivity;

     VideoPlayerThread  videoPlayerThread=null;

    private FirebaseUser current_user=null;



    public RecyclerAdapter_folder(List<Directory_folder> directories, Context context)
    {
        this.directories = directories;
        this.context = context;

        homeActivity =   ((HomeActivity) context);
    }

    public void setRecyclerAdapter(List<Directory_folder> directories, Context context)
    {
        this.directories = directories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder_folder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ac_e_fragment_folder_slice_recycler_item, viewGroup, false);


        return new ViewHolder_folder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder_folder viewHolderFolder, final int i)
    {






        Directory_folder directory = VideoFileParser.getInstance().total_folder.get(i);
        directory.setIndex(i);

        if(directory.getFrom_search() && i==0)
        {
            viewHolderFolder.file_name.setTextColor(Color.parseColor("#567845"));
             viewHolderFolder.file_name.setAllCaps(true);

            if(Config.DUPLI_TEST)
            {

                    Log.d("RECYCLER_CONTENTC", i +"::"+directory.file_path+"::"+viewHolderFolder.file_name.getText()+"::/"+directory.getFrom_search()+"::"+directories.size());

            }

            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(50); //You can manage the blinking time with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            viewHolderFolder.file_name .startAnimation(anim);

        }
        else
        {
             viewHolderFolder.file_name.setTextColor(Color.parseColor("#000000"));

        }


        String string_path = directory.file_name;
        String[] parts = string_path.split(".");
        if(Config.DUPLI_TEST)
        {
            Log.d("RECYCLER_CONTENT", directory.file_path+"::"+viewHolderFolder.file_name.getText()+"::"+string_path+"::"+directories.size());
        }

        viewHolderFolder.file_name.setText(string_path);
      //  viewHolderFolder.file_name.setText( i+"_"+string_path);




        long interval = 1 * 200;

        RequestOptions options = new RequestOptions();
        options.fitCenter();


        FutureStudioAppGlideModule futureStudioAppGlideModule = new FutureStudioAppGlideModule();



        futureStudioAppGlideModule.add_image_to_view(context,viewHolderFolder,Uri.fromFile( new File( directory.file_path ) ));

        viewHolderFolder.slice.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                current_user=FirebaseAuth.getInstance().getCurrentUser();

               // if(current_user==null)
                {

                 //   ( (HomeActivity)context).fileManager_homeActivity.GoToLoginActivity();
                }
               // else
                {
                    BottomSheet dialog = new BottomSheet(context,directory,null,0);
                    dialog.show();
                }









                return true;
            }
        });



        if(directory.hide) {
            viewHolderFolder.itemView.setVisibility(View.GONE);
            viewHolderFolder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
        }
        else
        {
            viewHolderFolder.itemView.setVisibility(View.VISIBLE);
            viewHolderFolder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }






    }





    public static class VideoPlayerThread extends Thread {
        @Override
        public void run()
        {

            if(Config.TEST)
                Log.d("_####_CALL_VIDEO", " Inside VideoPlayerThread Thread :  aa_activity_StartUp A");





            if(Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "Video_redirect()B1 : aa_activity_StartUp");








            if(Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "Video_redirect()B2 : aa_activity_StartUp");


            ///////////////////////////////////////////////////////////////////////////////////////////



            if(Config.TEST)
                Log.d("_####_CALL_VIDEO", " Inside VideoPlayerThread Thread :  aa_activity_StartUp B");


        }

    }


    @Override
    public int getItemCount()
    {
        return directories.size();
    }
}
