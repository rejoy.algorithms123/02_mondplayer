package com.media.vidplayer2.mond.ab_home_screen;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.api.services.drive.DriveScopes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments.FriendListFileManager;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_home_screen_PagerAdapter;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.FavFileManager;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.FolderFileManager;

import com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.NewLogin.LoginActivity;
import com.media.vidplayer2.mond.ad_a_user_Profile.MyUserProfileActivity;
import com.media.vidplayer2.mond.af_BlockList.BlockMembersActivity;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zf_space_share.spaceList.ListSpaceShareActivity;
import com.media.vidplayer2.mond.zg_contactlist.ContactActivity;
import com.media.vidplayer2.mond.zh_friendlist.FriendListActvity;
import com.media.vidplayer2.mond.zj_Queue.TestCase;
import com.media.vidplayer2.mond.zzzz_TestClass.DeleteDBActivity;

import java.util.HashMap;
import java.util.List;


public class FileManager_HomeActivity
{



    public final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 20;
    aa_home_screen_PagerAdapter aa_home_screen_pagerAdapter = null;
    Context context = null;

    public FavFileManager favFileManager=null;
    public FolderFileManager folderFileManager=null;

    public FriendListFileManager frdlistfileManager;

    public Thread Thread_Full_FOLDER =null;
    public Handler directory_handler_folder =null;



    HomeActivity homeActivity;

    public FileManager_HomeActivity(Context context)
    {

        this.context = context;
        aa_home_screen_pagerAdapter = new aa_home_screen_PagerAdapter(((HomeActivity) context).getSupportFragmentManager(),
                context);

        homeActivity=(HomeActivity)this.context;



        favFileManager = new FavFileManager(context);
        folderFileManager = new FolderFileManager(context);
        frdlistfileManager = new FriendListFileManager(context);

    }


    private void openSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(homeActivity);
        builder.setTitle("Required Permissions");
        builder.setMessage("This app require permission to use awesome feature. Grant them in app settings.");
        builder.setPositiveButton("Take Me To SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", homeActivity.getPackageName(), null);
                intent.setData(uri);
                homeActivity.startActivityForResult(intent, 101);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void  requestMultiplePermissions(){



        Dexter.withActivity(homeActivity)
                .withPermissions(
                       // Manifest.permission.READ_CONTACTS,
                      //  Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted())
                        {
                            Toast.makeText(homeActivity.getApplicationContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                            if(Config.FIRST_TIME_RUN)
                            {
                                 //favFileManager.Fav_Frag_ReadDirByThread();

                                new  com.media.vidplayer2.mond.zc_VideoFileParser.Util();
                                com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_firstTime(homeActivity,homeActivity);



                                setFirstTime_permission();
                            }


                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(homeActivity.getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();





    }


    public void getPermission()
    {


        requestMultiplePermissions();

/*
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {



            if (Config.TEST)
            {
                Log.d("_HMyAppRESUME_PERRAE", "gotPermission --getPermission : HomeActivity -- A");
            }


             if(Config.FIRST_TIME_RUN)
                favFileManager.Fav_Frag_ReadDirByThread();

            if (Config.TEST) {
                Log.d("_HMyAppRESUME_PERRAE", "gotPermission --getPermission : HomeActivity -- B");
            }

        }
        else
        {


            if (ActivityCompat.shouldShowRequestPermissionRationale((HomeActivity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {


                permission_snack_message();
                if (Config.TEST) {
                    Log.d("_#NO_PERMISSION_PREV", "shouldShowRequestPermissionRationale --getPermission : aa_activity_home_scree");
                }

            }
            else
            {

                if (Config.TEST) {
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : HomeActivity -- A");
                }

                ActivityCompat.requestPermissions((HomeActivity) context,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_SETTINGS},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                if (Config.TEST) {
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : HomeActivity -- B");
                }


            }

        }
*/

    }

    public void permission_snack_message()
    {


        Snackbar.make(((HomeActivity) context).findViewById(android.R.id.content),
                "Needs permission to read  video",
                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ActivityCompat.requestPermissions((HomeActivity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                        if (Config.TEST) {
                            Log.d("_#SNACK_BAR", "permission_snack_message : HomeActivity");
                        }

                    }
                }).show();
    }

    private void setFirstTime_permission() {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        // first time
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Permission", true);
        editor.commit();
        boolean ranBefore = preferences.getBoolean("Permission", false);

        if (Config.TEST)
            Log.d("_#FIRST_FLAG", "isFirstTime shared pref : HomeActivity " + ranBefore);

    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        /*

        switch (requestCode) {


            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:


                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //                    grantResult[0] means it will check for the first postion permission which is READ_EXTERNAL_STORAGE
                    //                    grantResult[1] means it will check for the Second postion permission which is CAMERA
                    Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show();

                    if (Config.TEST) {
                        Log.d("_HMyAppRESUME_PERRE", "Permission by req : HomeActivity -- A");
                    }




                    setFirstTime_permission();
                   // ReadDirFromHomeOnceVisitedOrFirstTimeVisited();

                    if (Config.TEST) {
                        Log.d("_HMyAppRESUME_PERRE", "Permission by req : HomeActivity -- B");
                    }


                } else {
                    permission_snack_message();

                }

             break;





            case MY_PERMISSIONS_REQUEST_SEND_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.



                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

        }

        */


    }




    public void window_toolbar_navbar_bg() {
        //
        Window window = ((HomeActivity) context).getWindow();
        Drawable background = ((HomeActivity) context).getResources().getDrawable(R.drawable.ad_toolbar_bg);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(((HomeActivity) context).getResources().getColor(android.R.color.transparent));
        window.setNavigationBarColor(((HomeActivity) context).getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);

        ((HomeActivity) context).setSupportActionBar(((HomeActivity) context).toolbar);
        ((HomeActivity) context).getSupportActionBar().setDisplayShowTitleEnabled(false);


    }


    public void enable_tab() {

        if (Config.TEST)
            Log.d("_A#ENABLE_TAB", "enable_tab  : HomeActivity A");

        ((HomeActivity) context).viewPager.setAdapter(aa_home_screen_pagerAdapter);
        ((HomeActivity) context).tabLayout.setupWithViewPager(((HomeActivity) context).viewPager);



        ((HomeActivity) context).viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                switch (position) {

                    case 0:

                        Config.SEL_TAB =position;
                        Config.HIDE_TAB=Config.HIDE_TAB;

                        if(FirebaseAuth.getInstance().getCurrentUser()==null)
                        {

                            GoToLoginActivity();

                        }
                        else
                         {
                            if (!GoogleSignIn.hasPermissions(
                                    GoogleSignIn.getLastSignedInAccount(homeActivity), new Scope(DriveScopes.DRIVE_FILE))) {
                                GoToLoginActivity();
                            }
                            else {

                                if(Config.DUPLI_TEST)
                                    Log.d("_ATAB_SEL_A", "1 ----"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

/*
                                if(homeActivity.start_list)
                                {
                                    FriendListFragment friendListFragmentF =(FriendListFragment)aa_home_screen_PagerAdapter.registeredFragments.get(0);

                                    if(friendListFragmentF!=null)
                                    friendListFragmentF.setUpRecyclerView();
                                    homeActivity.start_list=false;

                                }
                                */

                                }
                        }
                        if(Config.DUPLI_TEST)
                            Log.d("_ATAB_SEL_A", "2"+Config.HIDE_TAB+"::"+Config.SEL_TAB );




                        break;

                    case 1:

                         Config.SEL_TAB =position;
                         Config.HIDE_TAB=2;



                        if(Config.DUPLI_TEST)
                            Log.d("_ATAB_SEL_A", "3"+Config.HIDE_TAB+"::"+Config.SEL_TAB );

                       // FriendListFragment friendListFragmentF =(FriendListFragment)aa_home_screen_PagerAdapter.registeredFragments.get(0);

                     //   friendListFragmentF.stopFlist();


                        // homeActivity.friendListFileManager.stopFlist();
                        break;
                    case 2:
                        Config.SEL_TAB =position;
                        Config.HIDE_TAB=1;
                        if(Config.DUPLI_TEST)
                            Log.d("_ATAB_SEL_A", Config.HIDE_TAB+"::"+Config.SEL_TAB );
                      //  FriendListFragment friendListFragmentF1 =(FriendListFragment)aa_home_screen_PagerAdapter.registeredFragments.get(0);

                        //friendListFragmentF1.stopFlist();

                        break;


                }


                if (Config.TEST)
                    Log.d("_A#ENABLE_TAB", "enable_tab : HomeActivity B" + position);


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        if (Config.TEST)
            Log.d("_A#ENABLE_TAB", "enable_tab : HomeActivity B");

        for (int i = 0; i < ((HomeActivity) context).tabLayout.getTabCount(); i++) {


            ViewGroup slidingTabStrip = (ViewGroup) ((HomeActivity) context).tabLayout.getChildAt(0);
            View tab = slidingTabStrip.getChildAt(i);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();

            switch (i) {
                case 0:
                    layoutParams.weight = 1;
                    break;
                case 1:
                    layoutParams.weight = 1;
                    break;

                default:
                    layoutParams.weight = 0;
                    break;
            }


            tab.setLayoutParams(layoutParams);

        }


    }

    public void disable_tab(int hide_tab_no, int sel_tab_no)
    {

        View tab;
        LinearLayout.LayoutParams layoutParams;

        ViewGroup slidingTabStrip = (ViewGroup) ((HomeActivity) context).tabLayout.getChildAt(0);



        for(int i=0;i<3;i++)
        {
            if(hide_tab_no==i)
            {


                tab = slidingTabStrip.getChildAt(hide_tab_no);
                layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                layoutParams.weight = 0;
                tab.setLayoutParams(layoutParams);
            }
            else{


                tab = slidingTabStrip.getChildAt(i);
                layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                layoutParams.weight = 1;
                tab.setLayoutParams(layoutParams);

            }
        }






        ((HomeActivity) context).viewPager.setCurrentItem(sel_tab_no);

    }





    public void addGroup(String GroupName){


        String user_id  = FirebaseAuth.getInstance().getUid();

        FirebaseFirestore.getInstance().collection("GroupLists").document(user_id).collection("GroupNames").whereEqualTo("name", GroupName).get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots)
                    {




                        if (queryDocumentSnapshots.size() > 0)
                        {

                            if (Config.DUPLI_TEST)
                                Log.d("GRP_CASE", "onSuccess >size "+queryDocumentSnapshots.size() );


                            Context context = homeActivity.getApplicationContext();
                            CharSequence text = "Group Name is already Taken!";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();


                        }
                        else
                        {
                            if (Config.DUPLI_TEST)
                                Log.d("GRP_CASE", "onSuccess less size"+queryDocumentSnapshots.size() );

                            Context context = homeActivity.getApplicationContext();
                            CharSequence text = "Group Name available.. Group is preparing ..";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();



                            WriteBatch batch =  FirebaseFirestore.getInstance().batch();





                            // Name, email address, and profile photo Url
                            String name  =null;
                            String email =null ;
                            Uri photoUrl =null ;


                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            if (user != null)
                            {
                                for (UserInfo profile : user.getProviderData()) {
                                    // Id of the provider (ex: google.com)
                                    String providerId = profile.getProviderId();

                                    // UID specific to the provider
                                    String uid = profile.getUid();


                                    // Name, email address, and profile photo Url
                                    name = profile.getDisplayName();
                                    email = profile.getEmail();
                                    photoUrl = profile.getPhotoUrl();



                                }
                            }





                            HashMap<String, Object> groupdata = new HashMap<>();

                             //creator id
                            groupdata.put("name",  GroupName);
                            groupdata.put("freeze",  false);
                            groupdata.put("status",  "your group Status");
                            groupdata.put("thumb_image", "default");
                            groupdata.put("creator", FirebaseAuth.getInstance().getUid());
                            groupdata.put("time_stamp",   FieldValue.serverTimestamp());
                            groupdata.put("someonemessagedyou",   FieldValue.serverTimestamp());




                            DocumentReference GroupNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(user_id).collection("GroupNames").document(GroupName);
                            batch.set(GroupNamesReference, groupdata);



                            ////////////////////////////////////////////////////////
                            HashMap<String, Object> groupmembdata = new HashMap<>();
                            groupmembdata.put("name", FirebaseAuth.getInstance().getCurrentUser().getDisplayName() );

                            groupmembdata.put("id",  user_id);
                            groupmembdata.put("creatorid", user_id );
                            groupmembdata.put("admin",  true);
                            groupmembdata.put("creator",  true);
                            groupmembdata.put("blocked",  false);
                            groupmembdata.put("weight",  500);


                            DocumentReference GroupAdminNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(user_id).collection("GroupMembList-"+GroupName).document(user_id);
                            batch.set(GroupAdminNamesReference, groupmembdata);

                            ////////////////////////////////////////////////////////



                            //////////////////////////////////////////////////////////////////////


                            HashMap<String, Object> FrduserMap = new HashMap<>();
                            FrduserMap.put("id",user_id);
                            FrduserMap.put("block",  false);
                            FrduserMap.put("time_stamp",   FieldValue.serverTimestamp());
                            FrduserMap.put("time_stampN",   FieldValue.serverTimestamp());
                            FrduserMap.put("isawmessage",   FieldValue.serverTimestamp());
                            FrduserMap.put("someonemessagedyou",   FieldValue.serverTimestamp());
                            FrduserMap.put("group",  true);
                            FrduserMap.put("name",GroupName);



                            DocumentReference GroupFriendlistReference = FirebaseFirestore.getInstance().collection("Friends").document(user_id).collection("MyFriends").document(GroupName);;
                            batch.set(GroupFriendlistReference, FrduserMap);







                            batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid)
                                {

                                    Context context = homeActivity.getApplicationContext();
                                    CharSequence text = "Group Created";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(context, text, duration);
                                    toast.show();
                                    if (Config.DUPLI_TEST)
                                        Log.d("GRP_CASE", "batch onSuccess less size"+queryDocumentSnapshots.size() );





                                }


                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {

                                    if (Config.DUPLI_TEST)
                                        Log.d("GRP_CASE", "batch failure less size"+queryDocumentSnapshots.size() );


                                }
                            });




                            // contactMessage.setText("This contact has no Mond Account. Please Make sure you have Entered <Country Code><Mobile number> format properly");



                        }


                        // ...
                    }


                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (Config.DUPLI_TEST)
                    Log.d("GRP_CASE", "onFailures less size" );

            }
        });



    }


    public void initialize()
    {















    }

    public void GoToLoginActivity(){

        Intent intent = new Intent((HomeActivity)context, LoginActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }


    public void GoToBlockListActivity(){

        Intent intent = new Intent((HomeActivity)context, BlockMembersActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }


    public void GoToSpaceShareListActivity(){

        Intent intent = new Intent((HomeActivity)context, ListSpaceShareActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }

    public void SendtoSettingsActivity(){

        Intent intent = new Intent((HomeActivity)context, MyUserProfileActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }

    public void SendtoFriendlistActivity(){

        if(Config.DUPLI_TEST)
            Log.d("FLIST", "Friend list 1");


      //  Intent intent = new Intent((HomeActivity)context, FriendListsAcitivty.class);
        Intent intent = new Intent((HomeActivity)context, FriendListActvity.class);
        ((HomeActivity)context).startActivity(intent);



    }
    public void SendtoQueueActivity(){

        if(Config.DUPLI_TEST)
            Log.d("FLIST", "Friend list 1");


        //  Intent intent = new Intent((HomeActivity)context, FriendListsAcitivty.class);
        Intent intent = new Intent((HomeActivity)context, TestCase.class);
        ((HomeActivity)context).startActivity(intent);



    }

    public void SendtoDeleteDBActivity(){

        if(Config.DUPLI_TEST)
            Log.d("FLIST", "Friend list 1");


        //  Intent intent = new Intent((HomeActivity)context, FriendListsAcitivty.class);
        Intent intent = new Intent((HomeActivity)context, DeleteDBActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }


    public void GoToAddFriendActivity(){

        Intent intent = new Intent((HomeActivity)context, ContactActivity.class);
        ((HomeActivity)context).startActivity(intent);



    }












}


























