package com.media.vidplayer2.mond.zzzz_TestClass;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;

import java.util.ArrayList;
import java.util.List;

public class DeleteDBActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zzzz_test);

        CollectionReference collectionReference1 = FirebaseFirestore.getInstance().collection("Friends");

        CollectionReference collectionReference2 = FirebaseFirestore.getInstance().collection("GroupList");
        CollectionReference collectionReference3 = FirebaseFirestore.getInstance().collection("UsersList");

        Button btn = findViewById(R.id.logoutb);

        View.OnClickListener button_click = new View.OnClickListener() {

            @Override
            public void onClick(View v) {



                FirebaseAuth.getInstance().signOut();





            }
        };
        btn.setOnClickListener(button_click);
















    }
}
