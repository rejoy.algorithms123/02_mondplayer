package com.media.vidplayer2.mond.zf_space_share.uploadSpace;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.ze_LeakManager.IMMLeaks;

import java.io.File;
import java.util.Collections;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;
import com.media.vidplayer2.mond.zj_Queue.TestCase;

import static com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.MimeFinder.getMimeType;

public class UploadSpaceActivity extends AppCompatActivity {

    GoogleAccountCredential credential=null;

    //

    @OnClick(R.id.add_test)
    public void add_test(View view) {

        if (Config.DUPLI_TEST)
        {

            tokenVal =  LoginActivity1.getRandomNumberInRange( 0,100000);
            while (insert_Random(tokenVal)){
                tokenVal =  LoginActivity1.getRandomNumberInRange( 0,100000);
            }

            Config.mapUpload.put(tokenVal.toString(),tokenVal);



        }

    }


    @OnClick(R.id.del_test)
    public void del_test(View view) {

        if (Config.DUPLI_TEST)
        {


            Config.deleteHashUpload(speed_download_test.getText().toString());

            for (Map.Entry<String, Integer> entry : Config.mapUpload.entrySet())
            {

                Log.d("TOKEN_ARRAY",entry.getKey()+"::"+entry.getValue());
            }


        }
    }


    @OnClick(R.id.exist_test)
    public void exist_test(View view) {

        if (Config.DUPLI_TEST)
        {





                try {
                    Log.d("TOKEN_ARRAY", "Key exits: "+Config.KeyExistUpload(speed_download_test.getText().toString()));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }






        }
    }

    @OnClick(R.id.show_test)
    public void show_test(View view) {

        if (Config.DUPLI_TEST)
        {




            for (Map.Entry<String, Integer> entry : Config.mapUpload.entrySet())
            {

                Log.d("TOKEN_ARRAY",entry.getKey()+"::"+entry.getValue());
            }





        }
    }




    /////////////////////////////////////

    Integer tokenVal;

    @BindView(R.id.speed_download_test)
    public EditText speed_download_test;


    @BindView(R.id.text_id_space_u)
    public TextView text_id_space_u;


    @BindView(R.id.speed_download_u)
    public TextView speed_download_u;

    @BindView(R.id.percentage_size_u)
    public TextView percentage_size_u;


    @BindView(R.id.statU)
    public LinearLayout statU;

    @BindView(R.id.progress_circular_space_u)
    public ProgressBar progress_circular_space_u;

    @BindView(R.id.video_thumb_s)
    public ImageView video_thumb_s;
    String mime_type;
    Uri uri;
    String name;
    String path;
    public double first_time =0;
    FileManagerUploadSpace mDriveServiceHelper;
    public Handler handler;
    @OnClick(R.id.go_home)
    public void go_home(View view) {




    }

    @Override
    public void onBackPressed() {


        try {
            Config.semaphoreUpload.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        handler.removeCallbacksAndMessages(null);

        Config.deleteHashUpload(tokenVal.toString());
        mDriveServiceHelper.uploadSpaceActivity=null;
        Config.semaphoreUpload.release();



        super.onBackPressed();
    }

    @OnClick(R.id.upload)
    public void upload(View view)
    {

        statU.setVisibility(View.VISIBLE);
        text_id_space_u.setText(name);

        uri = Uri.parse(path);

        if(Config.DUPLI_TEST)
        {
            Log.d("DRIVE_CASE_TEST", "ID"+"::"+path+"::"+mime_type+"--"+name);
        }

      //  mDriveServiceHelper.uploadFile(name, uri,mime_type, UploadSpaceActivity.this);
       // mDriveServiceHelper.createFolder("Mond", "lolohhdd");
        Toasty.success(UploadSpaceActivity.this, "added to download queue and redirect to download queue" ).show();

        mDriveServiceHelper.UploadTotalOperation_Dir_Upload(  name,   uri,   mime_type );

        SendtoQueueActivity();




    }

    public void SendtoQueueActivity(){

        if(Config.DUPLI_TEST)
            Log.d("FLIST", "Friend list 1");


        //  Intent intent = new Intent((HomeActivity)context, FriendListsAcitivty.class);
        Intent intent = new Intent(UploadSpaceActivity.this, TestCase.class);
         startActivity(intent);



    }


    Boolean insert_Random(Integer val )
    {

        Integer integer=val;

        while(true &&  Config.mapUpload.size()>=0)
        {

            for (Map.Entry<String, Integer> entry : Config.mapUpload.entrySet())
            {

                if(entry.getValue()==integer)
                    return true;
            }

            return  false;

        }
        return  false;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);



        tokenVal =  LoginActivity1.getRandomNumberInRange( 0,100000);
        while (insert_Random(tokenVal))
        {
            tokenVal =  LoginActivity1.getRandomNumberInRange( 0,100000);
        }

        Config.mapUpload.put(tokenVal.toString(),tokenVal);



        Bundle b = getIntent().getExtras();
        path = b.getString("ID");
        name = b.getString("name");
        setContentView(R.layout.ae_activity_upload_space);
        ButterKnife.bind(this);
        handler= new Handler(Looper.getMainLooper());

        mime_type = getMimeType(UploadSpaceActivity.this, Uri.fromFile( new File(path) ));

        statU.setVisibility(View.GONE);
        text_id_space_u.setText(name);

        if(Config.DUPLI_TEST)
        {
            Log.d("DRIVE_UPLOAD_TEST", "ID 11 "+"::"+path+"::"+mime_type+"--"+name+"::"+tokenVal);
        }

        GoogleAccountCredential credential =
                GoogleAccountCredential.usingOAuth2(
                        getApplicationContext(), Collections.singleton(DriveScopes.DRIVE_FILE));




        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

        if(Config.DUPLI_TEST)
        {
            Log.d("DRIVE_UPLOAD_TEST", "ID 12 "+"::"+acct+"::"+acct.getEmail());
        }

        credential.setSelectedAccount(acct.getAccount());
        Drive googleDriveService =
                new Drive.Builder(
                        AndroidHttp.newCompatibleTransport(),
                        new GsonFactory(),
                        credential)
                        .setApplicationName("Drive API Migration")
                        .build();

        mDriveServiceHelper = new FileManagerUploadSpace(googleDriveService, UploadSpaceActivity.this,tokenVal);

        uri =  Uri.fromFile( new File(path));
        FutureStudioAppGlideModule futureStudioAppGlideModule = new FutureStudioAppGlideModule();
        futureStudioAppGlideModule.add_image_to_view_linkS(UploadSpaceActivity.this,video_thumb_s, uri );
        IMMLeaks.fixFocusedViewLeak(getApplication());


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_P:
            {




                if (Config.DUPLI_TEST)
                {


                    Log.d("STACK_TRACE",  Config.getAppTaskState(UploadSpaceActivity.this));


                }


                return true;
            }













        }



        return super.onKeyDown(keyCode, event);
    }



}
