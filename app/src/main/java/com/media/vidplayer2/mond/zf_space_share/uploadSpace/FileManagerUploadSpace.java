
package com.media.vidplayer2.mond.zf_space_share.uploadSpace;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zj_Queue.aa_Recycler.DirectoryFav;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import java.util.concurrent.TimeUnit;


public class FileManagerUploadSpace
{
    private  Executor mExecutor = Executors.newSingleThreadExecutor();
    private  Drive mDriveService=null;
    public UploadSpaceActivity uploadSpaceActivity =null;
    public final Runnable[] runnable = new Runnable[5];
    FileList result=null;
    File file;
    private  String foldername ="MondPlayerDataFiles";
     private  Integer tokenVal;

    public FileManagerUploadSpace(Drive mDriveService, Context context,Integer tokenVal)
    {
        this.mDriveService = mDriveService;

        uploadSpaceActivity = (UploadSpaceActivity)context;

        this.tokenVal=tokenVal;


        runnable[0]=new Runnable() {
            public void run() {




                uploadSpaceActivity.statU.setVisibility(View.VISIBLE);
                uploadSpaceActivity.percentage_size_u.setText( "Uploading to Spaceshare  ");
                uploadSpaceActivity.speed_download_u.setText("Started");

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Running 1");

            }
        };



        runnable[1]=new Runnable() {
            public void run() {

                uploadSpaceActivity.progress_circular_space_u.setVisibility(View.GONE);
                uploadSpaceActivity.text_id_space_u.setText("Failed  file creation");
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Running 1");

            }
        };



        runnable[2]=new Runnable() {
            public void run() {
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Running 2");
                uploadSpaceActivity.statU.setVisibility(View.GONE);
                uploadSpaceActivity.text_id_space_u.setText("Upload Finished.Open Space share");
            }


        };



        runnable[3]=new Runnable() {
            public void run() {
                // UI code goes here


                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Running 3");

                uploadSpaceActivity.statU.setVisibility(View.GONE);

                uploadSpaceActivity.text_id_space_u.setText("Upload Failed.Try again");
                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", "Download failure   activity   existing");



            }
        };



    }


    class CustomProgressListenerU implements MediaHttpUploaderProgressListener {

        Uri uri=null;

        java.io.File file;
        double first_numBytes =0,second_numBytes=0;
        double first_time =0,second_time=0;
        double download_speed=0;


        private String fileName;
        private int position;
        private  long filesize;





        public CustomProgressListenerU(Uri uri) {
            this.uri = uri;

            file = new java.io.File(uri.getPath().toString());

            this.fileName = file.getName();



           // Config.directories.add(new DirectoryFav(fileName, filesize, filesize) );
           /// this.fileName=fileName;
         //   position = Config.directories.size();


        }


        void Progress(MediaHttpUploader uploader) throws InterruptedException
        {


            second_numBytes = uploader.getNumBytesUploaded();

            second_time = getCurrentTimeInseconds() ;

            download_speed = toMB((second_numBytes - first_numBytes)/ (second_time - first_time));

            double percent = (((double) uploader.getNumBytesUploaded() / (double) file.length()) * 100);


            double percentRoundOff = roundTwo(percent);
            double download_speedRoundOff = roundTwo(download_speed);



            Config.directories.set(position-1,new DirectoryFav(fileName+" (Upload)", filesize,  (long)percent));
            Config.bv.setBoo(Config.bv.isBoo());



            if (Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "_#download Progress :: " + file.length() + "::" + uploader.getNumBytesUploaded() + "::" + percentRoundOff + "% :: " + download_speedRoundOff);




                runnable[4]=new Runnable() {
                    public void run() {
                        // UI code goes here

                        if(Config.DUPLI_TEST)
                            Log.d("DRIVE_UPLOAD_TEST", "Running 4");

                        uploadSpaceActivity.statU.setVisibility(View.VISIBLE);
                        uploadSpaceActivity.percentage_size_u.setText(percentRoundOff + "%   (" + uploader.getNumBytesUploaded() / (1024 * 1024) + "/" + file.length() / (1024 * 1024) + " bytes)");
                        uploadSpaceActivity.speed_download_u.setText(download_speedRoundOff + " MB/second");
                    }
                };

                Config.semaphoreUpload.acquire();

                if(Config.KeyExistUpload(tokenVal.toString()))
                {
                    uploadSpaceActivity.handler.post(runnable[4]);
                }
                Config.semaphoreUpload.release();


            first_numBytes = second_numBytes;
            first_time = second_time;




        }


        @Override
        public void progressChanged(MediaHttpUploader uploader) throws IOException {
            switch (uploader.getUploadState())
            {
                case MEDIA_IN_PROGRESS:

                    try {
                        Progress(uploader);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case MEDIA_COMPLETE:
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", "_#upload Complete :: "+ "::"+uploader.getNumBytesUploaded());
                    Config.directories.set(position-1,new DirectoryFav(fileName+" (Download)", filesize,  (long)100));
                    Config.bv.setBoo(Config.bv.isBoo());


                    break;

                case INITIATION_STARTED:

                    this.filesize = (long) file.length();

                     Config.directories.add(new DirectoryFav(fileName, filesize, (long)0) );

                      position = Config.directories.size();

                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", "INITIATION_STARTED: "+ "::"+position+":+"+fileName+":+"+filesize);


                    break;

                case INITIATION_COMPLETE:
                    first_numBytes = 0;
                    first_time = getCurrentTimeInseconds();
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", "_#iNITIAL :: "+ "::"+uploader.getNumBytesUploaded());
                    break;





            }
        }
    }








    public double getCurrentTimeInseconds()
    {

        long timeInMilliSeconds = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime());



        return (double)timeInMilliSeconds;
    }

    public double roundTwo(double val)
    {


        return (double)Math.round(val * 100.0) / 100.00;
    }

    public double toMB(double val)
    {


        return (double)val/(1024*1024);
    }



////////////////////////////////////////////////////////////////////////////////////////////////////
public Task<String> uploadFilePart(String name, Uri uri, String mime_type, String folderId)
{


    return Tasks.call(mExecutor, () -> {





        Config.semaphoreUpload.acquire();
        if(Config.KeyExistUpload(tokenVal.toString())) {
            uploadSpaceActivity.handler.post(runnable[0]);
        }
        Config.semaphoreUpload.release();

        List<String> root;
        if (folderId == null) {
            root = Collections.singletonList("root");
        } else {

            root = Collections.singletonList(folderId);
        }



        File metadata = new File()
                .setParents(root)
                .setMimeType(mime_type)
                .setName(name);







        InputStream targetStream = new FileInputStream(uri.toString());
        //  InputStream targetStream = new FileInputStream(_context.getContentResolver().openFileDescriptor(uri, "r").getFileDescriptor());
        InputStreamContent inputStreamContent = new InputStreamContent(mime_type, targetStream);
        Drive.Files.Create create =  mDriveService.files().create(metadata, inputStreamContent);
        MediaHttpUploader uploader = create.getMediaHttpUploader();
        uploader.setDirectUploadEnabled(false);
        uploader.setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE);
        uploader.setProgressListener(new CustomProgressListenerU(uri));
        File googleFile = create.execute();






        if (googleFile == null)
        {

            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", "File creation in upload failed");


            Config.semaphoreUpload.acquire();

            if(Config.KeyExistUpload(tokenVal.toString()))
                uploadSpaceActivity.handler.post(runnable[1]);
            Config.semaphoreUpload.release();


            throw new IOException("Null result when requesting file creation.");
        }
        return googleFile.getId();
    }).addOnSuccessListener(new OnSuccessListener<String>() {
        @Override
        public void onSuccess(String s) {

            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", " Upload  success   :: 6 " );

            try {
                Config.semaphoreUpload.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                if(Config.KeyExistUpload(tokenVal.toString()))
                    uploadSpaceActivity.handler.post( runnable[2] );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Config.semaphoreUpload.release();

        }
    }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {

            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", " Upload  success   :: 6 " );


            try {
                Config.semaphoreUpload.acquire();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }

            try {
                if(Config.KeyExistUpload(tokenVal.toString()))
                    uploadSpaceActivity.handler.post(runnable[3]);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            Config.semaphoreUpload.release();


        }
    });
}



    public Task<String> createFolder(String name, Uri uri, String mime_type) {
    return Tasks.call(mExecutor, () -> {

        File fileMetadata = new File();
        fileMetadata.setName(foldername);
        fileMetadata.setMimeType("application/vnd.google-apps.folder");

        file = mDriveService.files().create(fileMetadata)
                .setFields("id")
                .execute();
        return file.getId();
    }).addOnSuccessListener(new OnSuccessListener<String>() {
        @Override
        public void onSuccess(String s) {


            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", " Folder creation success   :: 5 " );

            uploadFilePart(  name,   uri,   mime_type,    file.getId());
        }
    }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {



            if(Config.DUPLI_TEST)
                Log.d("DRIVE_UPLOAD_TEST", " Folder creation failed   :: 4 " );


            try {
                Config.semaphoreUpload.acquire();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }

            try {
                if(Config.KeyExistUpload(tokenVal.toString()))
                    uploadSpaceActivity.handler.post(runnable[3]);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            Config.semaphoreUpload.release();


        }
    });
}

public Task<GoogleDriveFileHolder> UploadTotalOperation_Dir_Upload(String name, Uri uri, String mime_type  )
{

        return Tasks.call(mExecutor, () -> {


            if(Config.DUPLI_TEST)
            {
                Log.d("DRIVE_UPLOAD_TEST", " Inside upload ID"+"::"+uri.toString()+"::"+mime_type+"--"+name+"::::"+foldername+"::"+mDriveService);
            }



            result=null;

            result = mDriveService.files().list()
                    .setQ("mimeType = '" + "application/vnd.google-apps.folder"+ "' and name = '" + foldername + "' ")
                    .setSpaces("drive")
                    .execute();

            GoogleDriveFileHolder googleDriveFileHolder = new GoogleDriveFileHolder();

            if (result.getFiles().size() > 0)
            {
                googleDriveFileHolder.setFile(result.getFiles().get(0));


            }
            return googleDriveFileHolder;
        }).addOnSuccessListener(new OnSuccessListener<GoogleDriveFileHolder>() {
            @Override
            public void onSuccess(GoogleDriveFileHolder googleDriveFileHolder) {

                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Success 1" );



                if (!(result.getFiles().size() > 0) )
                {
                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", " Folder already not exists   :: 2 " );

                     createFolder(  name,   uri,   mime_type);


                }
                else
                {


                    if(Config.DUPLI_TEST)
                        Log.d("DRIVE_UPLOAD_TEST", " Folder already   exists   :: 3 " );

                    uploadFilePart(  name,   uri,   mime_type,    googleDriveFileHolder.getFile().getId());

                }


            }

        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e)
            {



                if(Config.DUPLI_TEST)
                    Log.d("DRIVE_UPLOAD_TEST", " Folder creation/search failed   :: 1 "+e);


                try {
                    Config.semaphoreUpload.acquire();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }

                try {
                    if(Config.KeyExistUpload(tokenVal.toString()))
                        uploadSpaceActivity.handler.post(runnable[3]);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

                Config.semaphoreUpload.release();


            }
        });




    }






}
