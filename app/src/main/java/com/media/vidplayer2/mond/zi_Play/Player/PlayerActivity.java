package com.media.vidplayer2.mond.zi_Play.Player;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daasuu.gpuv.egl.filter.GlSepiaFilter;
import com.daasuu.gpuv.player.GPUPlayerView;
import com.daimajia.numberprogressbar.NumberProgressBar;
import com.github.pwittchen.swipe.library.rx2.Swipe;
import com.github.pwittchen.swipe.library.rx2.SwipeListener;
import com.github.rubensousa.previewseekbar.PreviewView;
import com.github.rubensousa.previewseekbar.exoplayer.PreviewTimeBar;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.C.ContentType;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.drm.UnsupportedDrmException;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException;
import com.google.android.exoplayer2.offline.DownloadHelper;
import com.google.android.exoplayer2.offline.DownloadRequest;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MergingMediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.SingleSampleMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.ads.AdsLoader;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo;
import com.google.android.exoplayer2.trackselection.RandomTrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.DebugTextViewHelper;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.za_global.DemoApplication;
import com.media.vidplayer2.mond.zb_Config.Config;

import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.zi_Play.ExoPlayerManager;

import com.media.vidplayer2.mond.zi_Play.Filter.Filter;
import com.media.vidplayer2.mond.zi_Play.Lib.MainActivity;
import com.obsez.android.lib.filechooser.ChooserDialog;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.UUID;

/** An activity that plays media using {@link SimpleExoPlayer}. */
public class PlayerActivity extends AppCompatActivity
        implements OnClickListener, PlaybackPreparer, PlayerControlView.VisibilityListener , Toolbar.OnMenuItemClickListener,
        PreviewView.OnPreviewChangeListener {
    private static final String SCREEN_BRIGHTNESS_VALUE_PREFIX = "";

    public static final String DRM_SCHEME_EXTRA = "drm_scheme";
    public static final String DRM_LICENSE_URL_EXTRA = "drm_license_url";
    public static final String DRM_KEY_REQUEST_PROPERTIES_EXTRA = "drm_key_request_properties";
    public static final String DRM_MULTI_SESSION_EXTRA = "drm_multi_session";
    public static final String PREFER_EXTENSION_DECODERS_EXTRA = "prefer_extension_decoders";

    public static final String ACTION_VIEW = "com.google.android.exoplayer.demo.action.VIEW";
    public static final String EXTENSION_EXTRA = "extension";

    public static final String ACTION_VIEW_LIST =
            "com.google.android.exoplayer.demo.action.VIEW_LIST";
    public static final String URI_LIST_EXTRA = "uri_list";
    public static final String EXTENSION_LIST_EXTRA = "extension_list";

    public static final String AD_TAG_URI_EXTRA = "ad_tag_uri";

    public static final String ABR_ALGORITHM_EXTRA = "abr_algorithm";
    public static final String ABR_ALGORITHM_DEFAULT = "default";
    public static final String ABR_ALGORITHM_RANDOM = "random";

    public static final String SPHERICAL_STEREO_MODE_EXTRA = "spherical_stereo_mode";
    public static final String SPHERICAL_STEREO_MODE_MONO = "mono";
    public static final String SPHERICAL_STEREO_MODE_TOP_BOTTOM = "top_bottom";
    public static final String SPHERICAL_STEREO_MODE_LEFT_RIGHT = "left_right";

    // For backwards compatibility only.
    private static final String DRM_SCHEME_UUID_EXTRA = "drm_scheme_uuid";

    // Saved instance state keys.
    private static final String KEY_TRACK_SELECTOR_PARAMETERS = "track_selector_parameters";
    private static final String KEY_WINDOW = "window";
    private static final String KEY_POSITION = "position";
    private static final String KEY_AUTO_PLAY = "auto_play";

    int my_default_brightness=0;

    private static final CookieManager DEFAULT_COOKIE_MANAGER;
    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    private PlayerView playerView;
    private LinearLayout debugRootView;
    private Button selectTracksButton;
    private TextView debugTextView;
    private boolean isShowingTrackSelectionDialog;

    private DataSource.Factory dataSourceFactory;
    private SimpleExoPlayer player;
    private FrameworkMediaDrm mediaDrm;
    private MediaSource mediaSource;
    private DefaultTrackSelector trackSelector;
    private DefaultTrackSelector.Parameters trackSelectorParameters;
    private DebugTextViewHelper debugViewHelper;
    private TrackGroupArray lastSeenTrackGroupArray;

    private boolean startAutoPlay;
    private int startWindow;
    private long startPosition;
    WindowManager.LayoutParams layoutParams=null;
    // Fields used only for ad playback. The ads loader is loaded via reflection.

    private AdsLoader adsLoader;
    private Uri loadedAdTagUri;
    private PreviewTimeBar previewTimeBar;

    Boolean brightness_first_time =false;
    int brightness_default=0;

    private Swipe swipe;
    private SeekBar seekBarLeft;
    private SeekBar seekBarRight;

    float currentvolume=0;
    AudioManager audioManager=null;


    public Button subtite;

    Directory_folder directory_folder;
    // Activity lifecycle
    private void updateStartPosition() {
        if (player != null) {
            startAutoPlay = player.getPlayWhenReady();
            startWindow = player.getCurrentWindowIndex();
            startPosition = Math.max(0, player.getContentPosition());
        }
    }


    public Boolean checkVideo(String filepath)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(PlayerActivity.this);

        Long track = preferences.getLong(filepath, 0);

        if(track !=0)
        {
            startPosition = track;

            return  true;

        }
        else
        {
            return  false;
        }




    }

    @SuppressLint("InlinedApi")
    private void hideSystemUiFullScreen() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        playerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        dataSourceFactory = buildDataSourceFactory();
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER)
        {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.zyc_a_player_activity);
       // setContentView(R.layout.zz_test_activity_player);
       // debugRootView = findViewById(R.id.controls_root);
       // debugTextView = findViewById(R.id.debug_text_view);
        selectTracksButton = findViewById(R.id.select_tracks_button);
        selectTracksButton.setOnClickListener(this);

        playerView = findViewById(R.id.player_view);
        playerView.setControllerVisibilityListener(this);
        playerView.setErrorMessageProvider(new PlayerErrorMessageProvider());
        playerView.requestFocus();


        if (savedInstanceState != null)
        {
            trackSelectorParameters = savedInstanceState.getParcelable(KEY_TRACK_SELECTOR_PARAMETERS);
            startAutoPlay = savedInstanceState.getBoolean(KEY_AUTO_PLAY);
            startWindow = savedInstanceState.getInt(KEY_WINDOW);
            startPosition = savedInstanceState.getLong(KEY_POSITION);
        }
        else
        {
            trackSelectorParameters = new DefaultTrackSelector.ParametersBuilder().build();
            Intent intent = getIntent();
            int index = intent.getIntExtra("index", 0);
             directory_folder =  VideoFileParser.getInstance().total_folder.get(index);
            if(!checkVideo(directory_folder.getFile_path()))
            clearStartPosition();
            startAutoPlay = true;
        }


        previewTimeBar = playerView.findViewById(R.id.exo_progress);
        previewTimeBar.addOnPreviewChangeListener(this);


        /////////////////////////////////////////////////////////////////////




        SwipeCase();

        AudioCase();

        Subtitle();


        WifiDirect();

      }
//Environment.getExternalStorageDirectory().getAbsolutePath()

    void WifiDirect()
    {
        Button wifiDirect = (Button)findViewById(R.id.wifiDirect);
        wifiDirect.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {


                Intent i = new Intent(PlayerActivity.this, com.media.vidplayer2.mond.zi_Play.Lib.PlayerActivity.class);
                i.putExtra("ID", directory_folder.getFile_path());
                i.putExtra("name", "B");
                startActivity(i);




            }
        });
    }
    void Subtitle()
      {
          subtite = (Button)findViewById(R.id.subtite);
          subtite.setOnClickListener(new OnClickListener() {
              public void onClick(View v) {
                  new ChooserDialog(PlayerActivity.this)
                          .withFilter(false, false, "srt")
                          .withStartFile(Environment.getExternalStorageDirectory().getAbsolutePath())
                          .withResources(R.string.title_choose_file, R.string.title_choose, R.string.dialog_cancel)
                          .withChosenListener(new ChooserDialog.Result() {
                              @Override
                              public void onChoosePath(String path, File pathFile) {
                                  Toast.makeText(PlayerActivity.this, "FILE: " + path, Toast.LENGTH_SHORT).show();

                                  Format textFormat = Format.createTextSampleFormat(null, MimeTypes.APPLICATION_SUBRIP,
                                          null, Format.NO_VALUE, Format.NO_VALUE, "en", null, Format.OFFSET_SAMPLE_RELATIVE);
                                  MediaSource textMediaSource = new SingleSampleMediaSource.Factory(dataSourceFactory)
                                          .createMediaSource( Uri.parse(path), textFormat, C.TIME_UNSET);


                                  MergingMediaSource  videoSource = new MergingMediaSource(mediaSource, textMediaSource);
                                  player.prepare(videoSource);
                              }
                          })
                          .build()
                          .show();
              }
          });
      }

    @Override public boolean dispatchTouchEvent(MotionEvent event) {
        swipe.dispatchTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    void AudioCase()
    {
        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
    }

      Boolean swipe_left =false;
    Boolean swipe_up =false;
    Boolean swipe_down =false;
    Boolean swipe_right =false;



    void status_swipe( String From, MotionEvent event)
    {
        Log.d("SWIPECASE", From+"--swipe_left: "+swipe_left+"swipe_right:"+swipe_right+"swipe_up:"+swipe_up+"swipe_down:"+swipe_down);



        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;


        Log.d("SWIPECASE", From+"--swipe_left: "+swipe_left+"/ swipe_right:"+swipe_right+"/ swipe_up:"+swipe_up+"/ swipe_down:"+swipe_down);
        Log.d("SWIPECASE", From+"-- width"+width+"/"+event.getX()+"-- height"+height+"/"+event.getY());

    }

    NumberProgressBar numberProgressBar;
    int increment_factor=10;
    float increment_factorvol=.01f;
      void SwipeCase()
      {

       //   numberProgressBar = (NumberProgressBar) findViewById(R.id.change_screen_brightness_seekbar);
          seekBarLeft = (SeekBar) findViewById(R.id.change_screen_brightness_seekbar);
          seekBarRight = (SeekBar) findViewById(R.id.change_sound_seekbar);

          seekBarLeft.setMax(255);

          seekBarRight.setMax(100);

          swipe = new Swipe( );

          swipe.setListener(new SwipeListener() {

              @Override
              public void onSwipingLeft(MotionEvent event) {
                  swipe_left=true;
                  swipe_right=false;




                  status_swipe("onSwipingLeft()",event);
              }

              @Override
              public boolean onSwipedLeft(MotionEvent event) {

                  swipe_left=false;
                  status_swipe("onSwipedLeft()",event);



                  return false;
              }

              @Override
              public void onSwipingRight(MotionEvent event) {
                  swipe_left=false;
                  swipe_right=true;
                  status_swipe("onSwipingRight()",event);
              }

              @Override
              public boolean onSwipedRight(MotionEvent event) {


                  swipe_right=false;

                  status_swipe("onSwipedRight()",event);
                  return false;
              }



              @Override
              public void onSwipingUp(MotionEvent event)
              {

                  swipe_up=true;
                  swipe_down=false;


                  Display display = getWindowManager().getDefaultDisplay();
                  Point size = new Point();
                  display.getSize(size);
                  int width = size.x;
                  int height = size.y;

                  if(event.getX()<width/3)
                  {
                      seekBarLeft.setVisibility(View.VISIBLE);
                      int brightness =
                              Settings.System.getInt(getContentResolver(),
                                      Settings.System.SCREEN_BRIGHTNESS, 0);

                      brightness = brightness+increment_factor;
                      if(brightness>=255)
                          brightness=255;
                      changeScreenBrightness(PlayerActivity.this,brightness);
                      seekBarLeft.setProgress(brightness);
                  }
                  else
                  {


                      if(event.getX()>width/1.5f)
                      {


                          float sound =player.getVolume()+ increment_factorvol;

                          if(sound>1.0f)
                              sound =1.0f;
                          player.setVolume(sound);

                          seekBarRight.setVisibility(View.VISIBLE);

                          seekBarRight.setProgress((int)(sound*100.0f));

                          if(Config.DUPLI_TEST)
                              Log.d("AUDIO_CASE", "Volume: " + sound+"::"+player.getVolume());
                      }





                  }





                  status_swipe("onSwipingUp()",event);
              }




              @Override
              public boolean onSwipedUp(MotionEvent event) {
                  swipe_up=false;
                 seekBarLeft.setVisibility(View.GONE);
                  status_swipe("onSwipedUp()",event);


                  seekBarRight.setVisibility(View.GONE);


                  return false;
              }

              @Override
              public void onSwipingDown(MotionEvent event) {
                  swipe_up=false;
                  swipe_down=true;
                  status_swipe("onSwipingDown()",event);



                  Display display = getWindowManager().getDefaultDisplay();
                  Point size = new Point();
                  display.getSize(size);
                  int width = size.x;
                  int height = size.y;

                  if(event.getX()<width/3)
                  {
                     seekBarLeft.setVisibility(View.VISIBLE);

                      int brightness =
                              Settings.System.getInt(getContentResolver(),
                                      Settings.System.SCREEN_BRIGHTNESS, 0);

                      brightness = brightness-increment_factor;
                      if(brightness<=0)
                          brightness=0;
                      changeScreenBrightness(PlayerActivity.this,brightness);
                      seekBarLeft.setProgress(brightness);
                  }
                  else
                  {
                      if(event.getX()>width/1.5f)
                      {
                          float sound =player.getVolume()- increment_factorvol;
                          if(sound<=0)
                              sound=0;
                          player.setVolume(sound);


                          seekBarRight.setVisibility(View.VISIBLE);

                          seekBarRight.setProgress((int)(sound*100.0f));

                          if(Config.DUPLI_TEST)
                              Log.d("AUDIO_CASE", "Volume: " + sound+"::"+player.getVolume());
                      }


                  }
              }

              @Override
              public boolean onSwipedDown(MotionEvent event) {
                  swipe_down=false;
                  status_swipe("onSwipedDown()",event);
                  seekBarLeft.setVisibility(View.GONE);

                  seekBarRight.setVisibility(View.GONE);


                  return false;
              }
          });
      }



    @Override
    public void onWindowAttributesChanged(WindowManager.LayoutParams params) {
        layoutParams = params;
        super.onWindowAttributesChanged(params);
    }
    // Check whether this app has android write settings permission.
    private boolean hasWriteSettingsPermission(Context context)
    {
        boolean ret = true;
        // Get the result from below code.
        ret = Settings.System.canWrite(context);
        return ret;
    }

    // Start can modify system settings panel to let user change the write settings permission.
    private void changeWriteSettingsPermission(Context context)
    {
        //Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);


        Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
        intent.setData(Uri.parse("package:" +  getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void changeScreenBrightness(Context context, int screenBrightnessValue)
    {
        // Change the screen brightness change mode to manual.
      //  Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
        // Apply the screen brightness value to the system, this will change the value in Settings ---> Display ---> Brightness level.
        // It will also change the screen brightness for the device.
     //   Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, screenBrightnessValue);



/*
        ContentResolver cResolver = getContentResolver();
        Window window = getWindow();
        int brightness=0;
        try
        {
            Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            brightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS);



            if(brightness_first_time)
            {
                brightness_first_time=false;
                brightness_default=brightness;

            }
        }
        catch (Settings.SettingNotFoundException e)
        {
            e.printStackTrace();
            Log.i("__TAG", "error");
        }

*/

/*
        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, (screenBrightnessValue));
        WindowManager.LayoutParams layoutpars = window.getAttributes();
        layoutpars.screenBrightness = screenBrightnessValue / (float) 255;
        window.setAttributes(layoutpars);


        if(Config.DUPLI_TEST)
            Log.d("BRIGHTNESS_CASE",  "::"+layoutpars.screenBrightness);
*/
        // Change the screen brightness change mode to manual.

        if(hasWriteSettingsPermission(PlayerActivity.this))
        {
            Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            // Apply the screen brightness value to the system, this will change the value in Settings ---> Display ---> Brightness level.
            // It will also change the screen brightness for the device.
            Settings.System.putInt(context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, screenBrightnessValue);
            if(Config.DUPLI_TEST)
                Log.d("BRIGHTNESS_CASE",  "::"+screenBrightnessValue);

        }
        else
        {
            changeWriteSettingsPermission(PlayerActivity.this);
        }

    }

    @Override
    public void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        releasePlayer();
        releaseAdsLoader();
        clearStartPosition();
        setIntent(intent);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        if (Util.SDK_INT > 23)
        {
            initializePlayer();
            if (playerView != null)
            {
                playerView.onResume();
            }
        }


    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (Util.SDK_INT <= 23 || player == null) {
            initializePlayer();
            if (playerView != null)
            {
                playerView.onResume();
            }
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (Util.SDK_INT <= 23)
        {
            if (playerView != null)
            {
                playerView.onPause();
            }
            releasePlayer();
        }
    }



        @Override
    public void onStop()
    {
        super.onStop();
        if (Util.SDK_INT > 23) {
            if (playerView != null) {
                playerView.onPause();
            }
            releasePlayer();
        }


        Intent intent = getIntent();
        int index = intent.getIntExtra("index", 0);
        Directory_folder directory_folder =  VideoFileParser.getInstance().total_folder.get(index);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(PlayerActivity.this);
        // first time
        SharedPreferences.Editor editor = preferences.edit();

        editor.putLong(directory_folder.getFile_path(), startPosition);
        editor.commit();


    }

    @Override
    public void onDestroy() {


        super.onDestroy();
        releaseAdsLoader();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (grantResults.length == 0) {
            // Empty results are triggered if a permission is requested while another request was already
            // pending and can be safely ignored in this case.
            return;
        }
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initializePlayer();
        } else {
            showToast(R.string.storage_permission_denied);
            finish();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        updateTrackSelectorParameters();
        updateStartPosition();
        outState.putParcelable(KEY_TRACK_SELECTOR_PARAMETERS, trackSelectorParameters);
        outState.putBoolean(KEY_AUTO_PLAY, startAutoPlay);
        outState.putInt(KEY_WINDOW, startWindow);
        outState.putLong(KEY_POSITION, startPosition);
    }

    // Activity input

    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        // See whether the player view wants to handle media or DPAD keys events.
        return playerView.dispatchKeyEvent(event) || super.dispatchKeyEvent(event);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    // OnClickListener methods

    @Override
    public void onClick(View view)
    {
        if (view == selectTracksButton
                && !isShowingTrackSelectionDialog
                && TrackSelectionDialog.willHaveContent(trackSelector)) {
            isShowingTrackSelectionDialog = true;
            TrackSelectionDialog trackSelectionDialog =
                    TrackSelectionDialog.createForTrackSelector(
                            trackSelector,
                            /* onDismissListener= */ dismissedDialog -> isShowingTrackSelectionDialog = false);
            trackSelectionDialog.show(getSupportFragmentManager(), /* tag= */ null);
        }
    }

    // PlaybackControlView.PlaybackPreparer implementation

    @Override
    public void preparePlayback() {
        player.retry();
    }

    // PlaybackControlView.VisibilityListener implementation

    @Override
    public void onVisibilityChange(int visibility) {
        //debugRootView.setVisibility(visibility);
    }

    // Internal methods

    private void initializePlayer()
    {
        Uri[] uris = new  Uri[1];
        String[] extensions = new String[1];
        if (player == null)
        {
            Intent intent = getIntent();
            String action = intent.getAction();



            int index = intent.getIntExtra("index", 0);

            Directory_folder directory_folder =  VideoFileParser.getInstance().total_folder.get(index);
            // videoUri = paracelable_folder_videoPlayer.getText1();
            uris[0] = Uri.parse(directory_folder.getFile_path()) ;
            extensions[0] =null;


            if (ACTION_VIEW.equals(action))
            {
                /*
                uris = new Uri[] {intent.getData()};
                extensions = new String[] {intent.getStringExtra(EXTENSION_EXTRA)};

                 */
            }

            else if (ACTION_VIEW_LIST.equals(action))
            {
                /*
                String[] uriStrings = intent.getStringArrayExtra(URI_LIST_EXTRA);
                uris = new Uri[uriStrings.length];
                for (int i = 0; i < uriStrings.length; i++) {
                    uris[i] = Uri.parse(uriStrings[i]);
                }
                extensions = intent.getStringArrayExtra(EXTENSION_LIST_EXTRA);
                if (extensions == null) {
                    extensions = new String[uriStrings.length];
                }*/
            }
            else
            {

               // showToast(getString(R.string.unexpected_intent_action, action));
             //   finish();
              //  return;


            }



/*
            if (!Util.checkCleartextTrafficPermitted(uris)) {
                showToast(R.string.error_cleartext_not_permitted);
                return;
            }
            if (Util.maybeRequestReadExternalStoragePermission(  this, uris)) {

                return;
            }
*/
            DefaultDrmSessionManager<FrameworkMediaCrypto> drmSessionManager = null;
            if (intent.hasExtra(DRM_SCHEME_EXTRA) || intent.hasExtra(DRM_SCHEME_UUID_EXTRA))
            {
                String drmLicenseUrl = intent.getStringExtra(DRM_LICENSE_URL_EXTRA);
                String[] keyRequestPropertiesArray =
                        intent.getStringArrayExtra(DRM_KEY_REQUEST_PROPERTIES_EXTRA);
                boolean multiSession = intent.getBooleanExtra(DRM_MULTI_SESSION_EXTRA, false);
                int errorStringId = R.string.error_drm_unknown;
                if (Util.SDK_INT < 18)
                {
                    errorStringId = R.string.error_drm_not_supported;
                }
                else
                {
                    try {
                        String drmSchemeExtra = intent.hasExtra(DRM_SCHEME_EXTRA) ? DRM_SCHEME_EXTRA
                                : DRM_SCHEME_UUID_EXTRA;
                        UUID drmSchemeUuid = Util.getDrmUuid(intent.getStringExtra(drmSchemeExtra));
                        if (drmSchemeUuid == null) {
                            errorStringId = R.string.error_drm_unsupported_scheme;
                        } else {
                            drmSessionManager =
                                    buildDrmSessionManagerV18(
                                            drmSchemeUuid, drmLicenseUrl, keyRequestPropertiesArray, multiSession);
                        }
                    } catch (UnsupportedDrmException e) {
                        errorStringId = e.reason == UnsupportedDrmException.REASON_UNSUPPORTED_SCHEME
                                ? R.string.error_drm_unsupported_scheme : R.string.error_drm_unknown;
                    }
                }
                if (drmSessionManager == null) {
                    showToast(errorStringId);
                    finish();
                    return;
                }
            }

            TrackSelection.Factory trackSelectionFactory;
            String abrAlgorithm = intent.getStringExtra(ABR_ALGORITHM_EXTRA);
            if (abrAlgorithm == null || ABR_ALGORITHM_DEFAULT.equals(abrAlgorithm)) {
                trackSelectionFactory = new AdaptiveTrackSelection.Factory();
            } else if (ABR_ALGORITHM_RANDOM.equals(abrAlgorithm)) {
                trackSelectionFactory = new RandomTrackSelection.Factory();
            } else {
                showToast(R.string.error_unrecognized_abr_algorithm);
                finish();
                return;
            }

            boolean preferExtensionDecoders =
                    intent.getBooleanExtra(PREFER_EXTENSION_DECODERS_EXTRA, false);
            RenderersFactory renderersFactory =
                    ((DemoApplication) getApplication()).buildRenderersFactory(preferExtensionDecoders);

            trackSelector = new DefaultTrackSelector(trackSelectionFactory);
            trackSelector.setParameters(trackSelectorParameters);
            lastSeenTrackGroupArray = null;

            player =
                    ExoPlayerFactory.newSimpleInstance(
                            /* context= */ this, renderersFactory, trackSelector, drmSessionManager);
            player.addListener(new PlayerEventListener());
            player.setPlayWhenReady(startAutoPlay);
            player.addAnalyticsListener(new EventLogger(trackSelector));
            playerView.setPlayer(player);
            playerView.setPlaybackPreparer(this);
       //     debugViewHelper = new DebugTextViewHelper(player, debugTextView);
//            debugViewHelper.start();

            MediaSource[] mediaSources = new MediaSource[uris.length];
            for (int i = 0; i < uris.length; i++) {
                mediaSources[i] = buildMediaSource(uris[i], extensions[i]);
            }
            mediaSource =
                    mediaSources.length == 1 ? mediaSources[0] : new ConcatenatingMediaSource(mediaSources);
            String adTagUriString = intent.getStringExtra(AD_TAG_URI_EXTRA);
            if (adTagUriString != null) {
                Uri adTagUri = Uri.parse(adTagUriString);
                if (!adTagUri.equals(loadedAdTagUri)) {
                    releaseAdsLoader();
                    loadedAdTagUri = adTagUri;
                }
                MediaSource adsMediaSource = createAdsMediaSource(mediaSource, Uri.parse(adTagUriString));
                if (adsMediaSource != null) {
                    mediaSource = adsMediaSource;
                } else {
                    showToast(R.string.ima_not_loaded);
                }
            } else {
                releaseAdsLoader();
            }
        }
        boolean haveStartPosition = startWindow != C.INDEX_UNSET;
        if (haveStartPosition)
        {
            player.seekTo(startWindow, startPosition);
        }
        player.prepare(mediaSource, !haveStartPosition, false);
        updateButtonVisibility();

        ExoPlayerManager exoPlayerManager = new ExoPlayerManager(playerView, previewTimeBar,
                (ImageView) findViewById(R.id.imageView), uris[0].toString(),player,PlayerActivity.this);

        previewTimeBar.setPreviewLoader(exoPlayerManager);




    }

    private MediaSource buildMediaSource(Uri uri) {
        return buildMediaSource(uri, null);
    }

    private MediaSource buildMediaSource(Uri uri, @Nullable String overrideExtension) {
        DownloadRequest downloadRequest =
                ((DemoApplication) getApplication()).getDownloadTracker().getDownloadRequest(uri);
        if (downloadRequest != null) {
            return DownloadHelper.createMediaSource(downloadRequest, dataSourceFactory);
        }
        @ContentType int type = Util.inferContentType(uri, overrideExtension);
        switch (type) {
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_HLS:
                return new HlsMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
            default:
                throw new IllegalStateException("Unsupported type: " + type);
        }
    }

    private DefaultDrmSessionManager<FrameworkMediaCrypto> buildDrmSessionManagerV18(
            UUID uuid, String licenseUrl, String[] keyRequestPropertiesArray, boolean multiSession)
            throws UnsupportedDrmException {
        HttpDataSource.Factory licenseDataSourceFactory =
                ((DemoApplication) getApplication()).buildHttpDataSourceFactory();
        HttpMediaDrmCallback drmCallback =
                new HttpMediaDrmCallback(licenseUrl, licenseDataSourceFactory);
        if (keyRequestPropertiesArray != null) {
            for (int i = 0; i < keyRequestPropertiesArray.length - 1; i += 2) {
                drmCallback.setKeyRequestProperty(keyRequestPropertiesArray[i],
                        keyRequestPropertiesArray[i + 1]);
            }
        }
        releaseMediaDrm();
        mediaDrm = FrameworkMediaDrm.newInstance(uuid);
        return new DefaultDrmSessionManager<>(uuid, mediaDrm, drmCallback, null, multiSession);
    }

    private void releasePlayer() {
        if (player != null) {
            updateTrackSelectorParameters();
            updateStartPosition();
//            debugViewHelper.stop();
        //    debugViewHelper = null;
            player.release();
            player = null;
            mediaSource = null;
            trackSelector = null;
        }
        if (adsLoader != null) {
            adsLoader.setPlayer(null);
        }
        releaseMediaDrm();
    }

    private void releaseMediaDrm() {
        if (mediaDrm != null) {
            mediaDrm.release();
            mediaDrm = null;
        }
    }

    private void releaseAdsLoader() {
        if (adsLoader != null) {
            adsLoader.release();
            adsLoader = null;
            loadedAdTagUri = null;
            playerView.getOverlayFrameLayout().removeAllViews();
        }
    }

    private void updateTrackSelectorParameters() {
        if (trackSelector != null) {
            trackSelectorParameters = trackSelector.getParameters();
        }
    }


    private void clearStartPosition() {
        startAutoPlay = true;
        startWindow = C.INDEX_UNSET;
        startPosition = C.TIME_UNSET;
    }

    /** Returns a new DataSource factory. */
    private DataSource.Factory buildDataSourceFactory() {
        return ((DemoApplication) getApplication()).buildDataSourceFactory();
    }

    /** Returns an ads media source, reusing the ads loader if one exists. */
    private @Nullable MediaSource createAdsMediaSource(MediaSource mediaSource, Uri adTagUri) {
        // Load the extension source using reflection so the demo app doesn't have to depend on it.
        // The ads loader is reused for multiple playbacks, so that ad playback can resume.
        try {
            Class<?> loaderClass = Class.forName("com.google.android.exoplayer2.ext.ima.ImaAdsLoader");
            if (adsLoader == null) {
                // Full class names used so the LINT.IfChange rule triggers should any of the classes move.
                // LINT.IfChange
                Constructor<? extends AdsLoader> loaderConstructor =
                        loaderClass
                                .asSubclass(AdsLoader.class)
                                .getConstructor(android.content.Context.class, android.net.Uri.class);
                // LINT.ThenChange(../../../../../../../../proguard-rules.txt)
                adsLoader = loaderConstructor.newInstance(this, adTagUri);
            }
            adsLoader.setPlayer(player);
            AdsMediaSource.MediaSourceFactory adMediaSourceFactory =
                    new AdsMediaSource.MediaSourceFactory() {
                        @Override
                        public MediaSource createMediaSource(Uri uri) {
                            return PlayerActivity.this.buildMediaSource(uri);
                        }

                        @Override
                        public int[] getSupportedTypes() {
                            return new int[] {C.TYPE_DASH, C.TYPE_SS, C.TYPE_HLS, C.TYPE_OTHER};
                        }
                    };
            return new AdsMediaSource(mediaSource, adMediaSourceFactory, adsLoader, playerView);
        } catch (ClassNotFoundException e) {
            // IMA extension not loaded.
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // User controls

    private void updateButtonVisibility() {
        selectTracksButton.setEnabled(
                player != null && TrackSelectionDialog.willHaveContent(trackSelector));
    }

    private void showControls() {
//        debugRootView.setVisibility(View.VISIBLE);
    }

    private void showToast(int messageId) {
        showToast(getString(messageId));
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }

    @Override
    public void onStartPreview(PreviewView previewView, int progress) {

    }

    @Override
    public void onStopPreview(PreviewView previewView, int progress) {

    }

    @Override
    public void onPreview(PreviewView previewView, int progress, boolean fromUser) {

    }

    private class PlayerEventListener implements Player.EventListener {

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (playbackState == Player.STATE_ENDED) {
                showControls();
            }
            updateButtonVisibility();
        }

        @Override
        public void onPlayerError(ExoPlaybackException e) {
            if (isBehindLiveWindow(e)) {
                clearStartPosition();
                initializePlayer();
            } else {
                updateButtonVisibility();
                showControls();
            }
        }

        @Override
        @SuppressWarnings("ReferenceEquality")
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            updateButtonVisibility();
            if (trackGroups != lastSeenTrackGroupArray) {
                MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                            == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        showToast(R.string.error_unsupported_video);
                    }
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_AUDIO)
                            == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        showToast(R.string.error_unsupported_audio);
                    }
                }
                lastSeenTrackGroupArray = trackGroups;
            }
        }
    }

    private class PlayerErrorMessageProvider implements ErrorMessageProvider<ExoPlaybackException> {

        @Override
        public Pair<Integer, String> getErrorMessage(ExoPlaybackException e) {
            String errorString = getString(R.string.error_generic);
            if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                Exception cause = e.getRendererException();
                if (cause instanceof DecoderInitializationException) {
                    // Special case for decoder initialization failures.
                    DecoderInitializationException decoderInitializationException =
                            (DecoderInitializationException) cause;
                    if (decoderInitializationException.decoderName == null) {
                        if (decoderInitializationException.getCause() instanceof DecoderQueryException) {
                            errorString = getString(R.string.error_querying_decoders);
                        } else if (decoderInitializationException.secureDecoderRequired) {
                            errorString =
                                    getString(
                                            R.string.error_no_secure_decoder, decoderInitializationException.mimeType);
                        } else {
                            errorString =
                                    getString(R.string.error_no_decoder, decoderInitializationException.mimeType);
                        }
                    } else {
                        errorString =
                                getString(
                                        R.string.error_instantiating_decoder,
                                        decoderInitializationException.decoderName);
                    }
                }
            }
            return Pair.create(0, errorString);
        }
    }
/////////////////////////////////////////////////////////////////////////
}
