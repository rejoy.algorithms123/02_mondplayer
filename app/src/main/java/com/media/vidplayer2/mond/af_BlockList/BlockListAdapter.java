package com.media.vidplayer2.mond.af_BlockList;


import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments.ab_bottomsheet.BottomSheetF;
import com.media.vidplayer2.mond.af_BlockList.ab_bottomsheet.BottomSheetB;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_b_chat_group.GetTimeAgo;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;
import com.nex3z.notificationbadge.NotificationBadge;

public class BlockListAdapter extends FirestoreRecyclerAdapter<ModelBlocklist, BlockListAdapter.NoteHolder> {

    DocumentReference docRef;
    FutureStudioAppGlideModule futureStudioAppGlideModule ;
    BlockMembersActivity blockMembersActivity;


    public String searchText;
   //public  ListenerRegistration listenerRegistration;

   // public  ListenerRegistration listenerRegistrationMessage;

    public static String current_id ;
    public static String current_name ;
    public static String current_image;
    public static String current_email;

    public   String current_uid ;


    public BlockListAdapter(@NonNull FirestoreRecyclerOptions<ModelBlocklist> options, BlockMembersActivity blockMembersActivity) {
        super(options);
        futureStudioAppGlideModule = new FutureStudioAppGlideModule();
        this.blockMembersActivity = blockMembersActivity;

        this.current_uid = FirebaseAuth.getInstance().getUid();

    }

    @Override
    protected void onBindViewHolder(@NonNull NoteHolder holder, int position, @NonNull ModelBlocklist model)
    {
        //holder.textViewTitle.setText(model.getId());
     //   holder.textViewDescription.setText(model.getDescription());
      //  holder.textViewPriority.setText(String.valueOf(model.getPriority()));



        if(Config.DUPLI_TEST)
            Log.d("GRP_TEST_CASE_A", "onEvent: 1 ");


        if(blockMembersActivity.groupListFileManager.searchfriends)
        {

            if(Config.DUPLI_TEST)
                Log.d("YES_CASE", "onEvent: 1 "+ blockMembersActivity.groupListFileManager.searchfriends);

            boolean isFound = (model.getName().toLowerCase().toString()).indexOf(searchText.toLowerCase()) !=-1? true: false; //true


            if(isFound || searchText.equals("") )
            {
                holder.itemView.setVisibility(View.VISIBLE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE_X", "onEvent: 2 "+model.getName()+"::"+searchText);

            }

            else
            {
                holder.itemView.setVisibility(View.GONE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));

                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE_X", "onEvent: 3 "+model.getName()+"::"+searchText);

            }

        }
        else
            {


            if(Config.DUPLI_TEST)
                Log.d("GROUP_ISSUE_X", "onEvent: 4 "+model.getName()+"::"+searchText);



            holder.itemView.setVisibility(View.VISIBLE);
            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }


        holder.other_user_id= model.getId();

        holder.user_single_name.setText(model.getName());

        holder.user_single_online_icon.setVisibility(View.GONE);


        holder.statusfrd.setVisibility(View.GONE);
        holder.user_single_image.setVisibility(View.GONE);
        holder.badge.setVisibility(View.GONE);
        holder.time_left.setVisibility(View.GONE);





     holder.slice.setOnClickListener(new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            if(model.getGroup())
            {
                BottomSheetB dialog = new BottomSheetB( blockMembersActivity,model,true,holder);
                 dialog.show();
            }
            else
            {
               BottomSheetB dialog = new BottomSheetB(blockMembersActivity,model,false,holder);
               dialog.show();
            }


        }
    });





    }












    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.am_a_groupfriendlist_item,
                parent, false);
        return new NoteHolder(v);
    }

   public  class NoteHolder extends RecyclerView.ViewHolder {

        ImageView user_single_image;
        TextView user_single_name;
        TextView statusfrd;
        TextView noti_newmsg;

        NotificationBadge badge;

        LinearLayout slice;

        LinearLayout recycler_item_frdlist;
        ImageView user_single_online_icon;
        ImageView more;
        ImageView chatF;
        TextView time_left;
        public DocumentReference FriendListReference_1 ;
        public  DocumentReference FriendListReference_2  ;


        public   String other_user_id;
        public   String other_user_name;
        public   String other_user_image;
        public   String other_user_email;


        public NoteHolder(View itemView) {
            super(itemView);
            user_single_image = itemView.findViewById(R.id.user_single_imageg);

            time_left = itemView.findViewById(R.id.time_leftg);

            user_single_online_icon = itemView.findViewById(R.id.user_single_online_icong);
            user_single_name = itemView.findViewById(R.id.user_single_nameg);
            statusfrd = itemView.findViewById(R.id.statusfrdg);
            badge = itemView.findViewById(R.id.badgeg);


            slice = itemView.findViewById(R.id.recycler_item_frdlistg);
            recycler_item_frdlist = itemView.findViewById(R.id.recycler_item_frdlistg);
        }


    }

}



