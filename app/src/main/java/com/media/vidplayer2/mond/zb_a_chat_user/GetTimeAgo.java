package com.media.vidplayer2.mond.zb_a_chat_user;

import android.app.Application;
import android.content.Context;

import com.media.vidplayer2.mond.zb_Config.Config;

/**
 * Created by AkshayeJH on 16/07/17.
 */

public class GetTimeAgo extends Application{

    /*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    private static final double SECOND_MILLIS = 1000;
    private static final double MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final double HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final double DAY_MILLIS = 24 * HOUR_MILLIS;


    public static String getTimeAgo(long time, Context ctx)
    {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

       // long now = System.currentTimeMillis();
        double now = System.currentTimeMillis();

        /*
        if (time > now || time <= 0) {

            if(zz_Config.CHAT_TEST)
            {
                Log.d("ONLINE_TEST", "3 now "+ now+"time"+time);
            }

            return null;
        }
        */

        // TODO: localize
        final double diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return Config.roundTwo(diff / MINUTE_MILLIS) + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return Config.roundTwo(diff / HOUR_MILLIS) + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return Config.roundTwo(diff / DAY_MILLIS) + " days ago";
        }
    }

}
