package com.media.vidplayer2.mond.af_BlockList;

public class ModelBlocklist {


    public ModelBlocklist() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    Boolean group;

    public ModelBlocklist(Boolean group, String name, String id) {
        this.group = group;
        this.name = name;
        this.id = id;
    }

    String name;
    String id;


}
