package com.media.vidplayer2.mond.zc_Glide;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.ViewHolder_folder;




@GlideModule
public class FutureStudioAppGlideModule extends AppGlideModule {


    public void  add_image_to_view(final Context context, ViewHolder_folder viewHolderFolder, Uri uri)
    {



        final Boolean[] errorF = {false};


        GlideApp.with(context)

                .load(uri)
               // .apply(new RequestOptions().override(100, 70))

                .thumbnail(0.1f)
                .error(GlideApp.with(context).load(R.drawable.af_thumb_image_missing))
               // .override(150, 150) // resizes the image to these dimensions (in pixel)
            //    .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.

                .into(viewHolderFolder.video_ref);



      //  if(errorF[0])
      //  GlideApp.with(context).load(R.drawable.af_thumb_image_missing);



       // Log.d("#FILE_ERROR", "add_image_to_view: "+viewHolderFolder.video_thumb.getBackground().equals(R.drawable.af_thumb_image_missing));



    }


    public void add_image_to_view_linkS(final Context context, ImageView imageView, Uri uri)
    {



        final Boolean[] errorF = {false};


        GlideApp.with(context)

                .load(uri)
                // .apply(new RequestOptions().override(100, 70))

                .thumbnail(0.1f)
                .error(GlideApp.with(context).load(R.drawable.af_thumb_image_missing))
                // .override(150, 150) // resizes the image to these dimensions (in pixel)
                //    .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.

                .into(imageView);



        //  if(errorF[0])
        //  GlideApp.with(context).load(R.drawable.af_thumb_image_missing);



        // Log.d("#FILE_ERROR", "add_image_to_view: "+viewHolderFolder.video_thumb.getBackground().equals(R.drawable.af_thumb_image_missing));



    }
    public void add_image_to_view_link(final Context context, ImageView imageView, String uri)
    {



        final Boolean[] errorF = {false};


        GlideApp.with(context)

                .load(uri)
                // .apply(new RequestOptions().override(100, 70))

                .thumbnail(0.1f)
                .error(GlideApp.with(context).load(R.drawable.af_thumb_image_missing))
                // .override(150, 150) // resizes the image to these dimensions (in pixel)
                //    .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.

                .into(imageView);



        //  if(errorF[0])
        //  GlideApp.with(context).load(R.drawable.af_thumb_image_missing);



        // Log.d("#FILE_ERROR", "add_image_to_view: "+viewHolderFolder.video_thumb.getBackground().equals(R.drawable.af_thumb_image_missing));



    }


    public void  showThumbnail(final Context context, ImageView imageView, String weblink)
    {



        final Boolean[] errorF = {false};


        GlideApp.with(context)

                .load(weblink)
                // .apply(new RequestOptions().override(100, 70))

                .thumbnail(0.1f)
                .error(GlideApp.with(context).load(R.drawable.af_thumb_image_missing))
                // .override(150, 150) // resizes the image to these dimensions (in pixel)
                //    .centerCrop() // this cropping technique scales the image so that it fills the requested bounds and then crops the extra.

                .into(imageView);



        //  if(errorF[0])
        //  GlideApp.with(context).load(R.drawable.af_thumb_image_missing);



        // Log.d("#FILE_ERROR", "add_image_to_view: "+viewHolderFolder.video_thumb.getBackground().equals(R.drawable.af_thumb_image_missing));



    }




}