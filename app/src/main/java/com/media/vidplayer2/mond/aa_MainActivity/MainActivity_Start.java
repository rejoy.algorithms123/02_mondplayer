package com.media.vidplayer2.mond.aa_MainActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.ze_LeakManager.IMMLeaks;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity_Start extends AppCompatActivity {



    Unbinder                                                                          unbinder=null;
    public static MainActivity_Start mainActivityStartS = null;
    public FileManagerMainActivity _fileManager_mainActivity =null;


     @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        _fileManager_mainActivity = new FileManagerMainActivity();
        Config.IS_BACK=false;

        IMMLeaks.fixFocusedViewLeak(getApplication());


    }

    @Override
    public void onResume()
    {
        super.onResume();

        _fileManager_mainActivity.TOKEN=0;

        if(Config.IS_BACK)
        {
            onBackPressed();
        }
        else
        {

            MainActivity_Start.mainActivityStartS =this;

            if(Config.TEST)
            {
                Log.d("SEMAPHORE", "@@ SIGNATURE_HASH_TOKEN::SET "+ _fileManager_mainActivity.TOKEN+"::"+ _fileManager_mainActivity.INTENT_FIRST_DONE);
            }


            if(_fileManager_mainActivity.isFirstTime(this))
            {


                setContentView(R.layout.aa_activity_main_activity_first_time);
                _fileManager_mainActivity.firstTimeThread.start();
                Config.FIRST_TIME_RUN =true;
            }
            else
            {
                setContentView(R.layout.ab_activity_main_activity_splash);
             //   _fileManager_mainActivity.ParseVideoOnSecondTime("aa_activity_StartUp");

              //  new  com.media.vidplayer2.mond.zc_VideoFileParser.Util();
               // com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_secondTime(this);


                new  com.media.vidplayer2.mond.zc_VideoFileParser.Util();
                com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_secondTime(this);



                Config.FIRST_TIME_RUN =false;
            }

        }






    }



    @Override
    public void onStart()
    {
        super.onStart();

         if(Config.TEST)
            Log.d("MyApp", "onStart From main Activity");

        unbinder= ButterKnife.bind(this);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        unbinder.unbind();

        if(Config.TEST)
            Log.d("MyApp", "onStop From main Activity");

    }

    @Override
    public void onBackPressed()
    {        // to prevent irritating accidental logouts



        try
        {
           _fileManager_mainActivity.semaphore.acquire();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }



         if(Config.TEST)
         {
             Log.d("SEMAPHORE", "SIGNATURE_HASH_TOKEN::SET 1"+ _fileManager_mainActivity.TOKEN+"::"+ _fileManager_mainActivity.INTENT_FIRST_DONE);
         }

         if(_fileManager_mainActivity.TOKEN!= _fileManager_mainActivity.INTENT_FIRST_DONE)
         {
             // clean up
             super.onBackPressed();
             _fileManager_mainActivity.TOKEN= _fileManager_mainActivity.ONBACK_FIRST_DONE;
             if(Config.TEST)
             {
                 Log.d("SEMAPHORE", "SIGNATURE_HASH_TOKEN::SET 2"+ _fileManager_mainActivity.TOKEN+"::"+ _fileManager_mainActivity.INTENT_FIRST_DONE);
             }

         }

        _fileManager_mainActivity.semaphore.release();

       // _fileManager_mainActivity.

        if(Config.TEST)
        {
            Log.d("SEMAPHORE", "SIGNATURE_HASH_TOKEN::SET 3"+ _fileManager_mainActivity.TOKEN+"::"+ _fileManager_mainActivity.INTENT_FIRST_DONE);
        }

    }




    //////////////////Test For task//////////

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_P:
            {
                //your Action code

                if(Config.TEST)
                {
                   // Log.d("MyApp", "------------------------------My Stack------------------------------");
                    Log.d("MyApp",  Config.getAppTaskState(getApplicationContext()));
                  //  Log.d("MyApp", "--------------------------------------------------------------------");
                }

                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}
