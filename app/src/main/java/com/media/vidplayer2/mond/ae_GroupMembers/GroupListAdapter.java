package com.media.vidplayer2.mond.ae_GroupMembers;


import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ae_GroupMembers.ab_bottomsheet.BottomSheetG;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_b_chat_group.GetTimeAgo;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;

import com.nex3z.notificationbadge.NotificationBadge;

public class GroupListAdapter extends FirestoreRecyclerAdapter<ModelGrouplist, GroupListAdapter.NoteHolder> {

    DocumentReference docRef;
    FutureStudioAppGlideModule futureStudioAppGlideModule ;
    GroupMembersActivity groupMembersActivity;


    public String searchText;
   //public  ListenerRegistration listenerRegistration;

   // public  ListenerRegistration listenerRegistrationMessage;

    public static String current_id ;
    public static String current_name ;
    public static String current_image;
    public static String current_email;

    public   String current_uid ;


    public GroupListAdapter(@NonNull FirestoreRecyclerOptions<ModelGrouplist> options, GroupMembersActivity groupMembersActivity) {
        super(options);
        futureStudioAppGlideModule = new FutureStudioAppGlideModule();
        this.groupMembersActivity = groupMembersActivity;

        this.current_uid = FirebaseAuth.getInstance().getUid();

    }

    @Override
    protected void onBindViewHolder(@NonNull NoteHolder holder, int position, @NonNull ModelGrouplist model)
    {
        //holder.textViewTitle.setText(model.getId());
     //   holder.textViewDescription.setText(model.getDescription());
      //  holder.textViewPriority.setText(String.valueOf(model.getPriority()));



        if(Config.DUPLI_TEST)
            Log.d("GRP_TEST_CASE_A", "onEvent: 1 ");


        if(groupMembersActivity.groupListFileManager.searchfriends)
        {

            if(Config.DUPLI_TEST)
                Log.d("YES_CASE", "onEvent: 1 "+groupMembersActivity.groupListFileManager.searchfriends);

            boolean isFound = (model.getName().toLowerCase().toString()).indexOf(searchText.toLowerCase()) !=-1? true: false; //true


            if(isFound || searchText.equals("") )
            {
                holder.itemView.setVisibility(View.VISIBLE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE_X", "onEvent: 2 "+model.getName()+"::"+searchText);

            }

            else
            {
                holder.itemView.setVisibility(View.GONE);
                holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));

                if(Config.DUPLI_TEST)
                    Log.d("GROUP_ISSUE_X", "onEvent: 3 "+model.getName()+"::"+searchText);

            }

        }
        else {


            if(Config.DUPLI_TEST)
                Log.d("GROUP_ISSUE_X", "onEvent: 4 "+model.getName()+"::"+searchText);



            holder.itemView.setVisibility(View.VISIBLE);
            holder.itemView.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }





        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
        {
            if(Config.DUPLI_TEST)
                Log.d("GRP_TEST_CASE_A", model.admin +"::"+model.getId()+"::"+model.getCreator()+"::"+model.getWeight());




            listeners_users(holder,position,model);



            holder.slice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    BottomSheetG dialog = new BottomSheetG( groupMembersActivity,model);
                    dialog.show();







                }
            });







        }







    }

    void listeners_users(GroupListAdapter.NoteHolder holder, int position, @NonNull ModelGrouplist model)
    {

        ListenerRegistration listenerRegistration;
        listenerRegistration= FirebaseFirestore.getInstance().collection("UsersList").document(model.getId()).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }




                if(FirebaseAuth.getInstance().getCurrentUser().getUid().equals(model.getId()) )
                {
                    //  holder.recycler_item_frdlist.setVisibility(View.GONE);

                    GroupListAdapter.current_id =FirebaseAuth.getInstance().getCurrentUser().getUid();
                    GroupListAdapter.current_name= documentSnapshot.getString("name");
                    GroupListAdapter.current_image= documentSnapshot.getString("thumb_image");
                    GroupListAdapter.current_email= documentSnapshot.getString("email");

                }
                else
                {

                    holder.other_user_id=documentSnapshot.getString("id");
                    holder.other_user_name=documentSnapshot.getString("name");
                    holder.other_user_image=documentSnapshot.getString("thumb_image");
                    holder.other_user_email=documentSnapshot.getString("email");
                }





                String id = documentSnapshot.getString("id");
                String status = documentSnapshot.getString("status");
                String thumb_image =  documentSnapshot.getString("thumb_image");
                String name =  documentSnapshot.getString("name");
                Boolean online =  documentSnapshot.getBoolean("online");
                Timestamp timestamp = documentSnapshot.getTimestamp("time_stamp");






                holder.user_single_name.setText(name );


                if(model.getCreator())
                holder.statusfrd.setText("Admin/Owner" );
                else
                {
                    if(model.getAdmin())
                    {
                        holder.statusfrd.setText("Admin" );
                    }
                    else
                    {
                        holder.statusfrd.setText("user" );
                    }
                }



                if(model.isBlocked())
                {
                    holder.statusfrd.setText("Blocked" );
                    holder.statusfrd.setTextColor(Color.RED);
                }








                if(!thumb_image.equals("default"))
                {
                    futureStudioAppGlideModule.add_image_to_view_link(groupMembersActivity,holder.user_single_image,thumb_image);

                }





                if( online )
                {
                    holder.user_single_online_icon.setVisibility(View.VISIBLE);
                    holder.time_left.setText("Online");
                }
                else
                {
                    holder.user_single_online_icon.setVisibility(View.GONE);
                    holder.user_single_online_icon.setVisibility(View.GONE);
                    GetTimeAgo getTimeAgo = new GetTimeAgo();
                    holder.time_left.setText("Online");
                    String lastSeenTime=null;
                    if(timestamp!=null)
                        lastSeenTime = getTimeAgo.getTimeAgo(timestamp.toDate().getTime(), groupMembersActivity);
                    holder.time_left.setText(lastSeenTime);

                }



                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A", "status "+status+"thumb_image "+thumb_image+"name :: "+name);

                if(Config.DUPLI_TEST)
                    Log.d("DOWNLOAD_LINK_A", "error "+ e);









            }
        });
        if(groupMembersActivity.listenersUsers.size()>this.getItemCount())
        {
            groupMembersActivity.listenersUsers.get(position).remove();
        }
        groupMembersActivity.listenersUsers.add(listenerRegistration);

    }




    void listeners_groups(GroupListAdapter.NoteHolder holder, int position, @NonNull ModelGrouplist model)
    {



        ListenerRegistration listenerRegistration;
        listenerRegistration=FirebaseFirestore.getInstance().collection("GroupLists").document(groupMembersActivity.CreaterId).collection("GroupNames").document(groupMembersActivity.GroupName).addSnapshotListener(MetadataChanges.INCLUDE, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot,
                                @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    System.err.println("Listen failed:" + e);
                    return;
                }









                String creator = documentSnapshot.getString("creator");
                String name = documentSnapshot.getString("name");
                String thumb_image =  documentSnapshot.getString("thumb_image");
                String status =  documentSnapshot.getString("status");


                try{



                    Timestamp timestamp    = documentSnapshot.getTimestamp("someonemessagedyou");



                    if(Config.DUPLI_TEST)
                        Log.d("GRP_TEST_CASE_B", "onEvent: "+name+"::"+thumb_image+"::"+status+"::"+timestamp+"::"+current_id);


                    if(timestamp!=null)
                    {

                    }














                }catch (Exception e1) {
                    e1.printStackTrace();
                }









            }
        });

        if(groupMembersActivity.listenersUsers.size()>this.getItemCount())
        {
            groupMembersActivity.listenersUsers.get(position).remove();
        }
        groupMembersActivity.listenersUsers.add(listenerRegistration);

    }






    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.am_a_groupfriendlist_item,
                parent, false);
        return new NoteHolder(v);
    }

    class NoteHolder extends RecyclerView.ViewHolder {

        ImageView user_single_image;
        TextView user_single_name;
        TextView statusfrd;
        TextView noti_newmsg;

        NotificationBadge badge;

        LinearLayout slice;

        LinearLayout recycler_item_frdlist;
        ImageView user_single_online_icon;
        ImageView more;
        ImageView chatF;
        TextView time_left;
        public DocumentReference FriendListReference_1 ;
        public  DocumentReference FriendListReference_2  ;


        public   String other_user_id;
        public   String other_user_name;
        public   String other_user_image;
        public   String other_user_email;


        public NoteHolder(View itemView) {
            super(itemView);
            user_single_image = itemView.findViewById(R.id.user_single_imageg);

            time_left = itemView.findViewById(R.id.time_leftg);

            user_single_online_icon = itemView.findViewById(R.id.user_single_online_icong);
            user_single_name = itemView.findViewById(R.id.user_single_nameg);
            statusfrd = itemView.findViewById(R.id.statusfrdg);
            badge = itemView.findViewById(R.id.badgeg);


            slice = itemView.findViewById(R.id.recycler_item_frdlistg);
            recycler_item_frdlist = itemView.findViewById(R.id.recycler_item_frdlistg);
        }


    }

}



