package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.ab_bottomsheet.BottomSheet;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;

import java.io.File;
import java.util.List;

public class RecyclerAdapterFav extends RecyclerView.Adapter<ViewHolderFav> {

    public List<DirectoryFav> directories;
    private Context context;
    HomeActivity homeActivity;







    public RecyclerAdapterFav(List<DirectoryFav> directories, Context context)
    {

        this.directories = directories;
        this.context = context;
        this.homeActivity = (HomeActivity)context;

        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER",  "1");

        }




    }

    public void setRecyclerAdapter(List<DirectoryFav> directories, Context context)
    {
        this.directories = directories;
        this.context = context;

        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER",  "2");

        }

    }

    @NonNull
    @Override
    public ViewHolderFav onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ac_c_fragment_favourite_slice_recycler_item, viewGroup, false);

        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER",  "3");

        }

        return new ViewHolderFav(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderFav viewHolderFav, final int i)
    {




        DirectoryFav directoryFav = this.directories.get(i);
        directoryFav.index=i;
        viewHolderFav.video_count.setText(directoryFav.video_count);
        HomeActivity aa_activity_home_screen = ((HomeActivity)context);
        viewHolderFav.dir_name.setText(directoryFav.dir_name+"_"+i);



        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER",  "4");

        }


        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER", directoryFav.dir_name+"::"+directoryFav.ID +"::"+directoryFav.video_count);

        }


   ////     List<String> values = new ArrayList<String>();
       // values = aa_activity_home_screen._global_StartUP.ab_fileManagerStart.mapUpload.get(aa_activity_home_screen._global_StartUP.ab_fileManagerStart.names_fav_MEM.get(i));

        Integer i5 = new Integer(i);
        Integer i6 = new Integer(0);
        if(i==0)
        {


            viewHolderFav.dir_name.setTextColor(Color.parseColor("#008000"));
          //   Log.d("#__INDEX", aa_activity_home_screen._global_StartUP.ab_fileManagerStart.names_fav_MEM.get(i)+" : "+i+":"+ values.get(0)+"  "+i5+":"+i6+";"+i5.equals(i6));

        }
        else
        {

            viewHolderFav.dir_name.setTextColor(Color.parseColor("#000000"));
        }




        viewHolderFav.slice.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {



                DirectoryFav directoryFav = VideoFileParser.getInstance().total_SD_MEM.get(i);

                if(Config.DUPLI_TEST)
                {
                    Toast.makeText(context, "Item " + (i ) + " clicked  :: "+directoryFav.ID,
                            Toast.LENGTH_SHORT).show();

                    //  Log.d("_##YES", "onClick: "+values.get(0));


                }

                File file = new File(directoryFav.ID);
                ( (HomeActivity)context).fileManager_homeActivity.disable_tab(1,2);


               //  ( (HomeActivity)context).fileManager_homeActivity.folderFileManager.ReadDirByThread_folder(file);







                if(Config.DUPLI_TEST)
                    Log.d("SEARCH_F_CASE", "onClick: "+directoryFav.getFile()+"::d"+directoryFav.getFile_name());
                com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_fromFolder(context,homeActivity,directoryFav.getID(),directoryFav.getFile(),directoryFav.getFile_name());




            }
        });



        viewHolderFav.slice1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {




                DirectoryFav directoryFav =  VideoFileParser.getInstance().total_SD_MEM.get(i);

                if(Config.TEST)
                {
                    Toast.makeText(context, "Item " + (i ) + " clicked  :: "+directoryFav.ID,
                            Toast.LENGTH_SHORT).show();

                    //  Log.d("_##YES", "onClick: "+values.get(0));


                }

                File file = new File(directoryFav.ID);

                ( (HomeActivity)context).fileManager_homeActivity.disable_tab(1,2);

               // ( (HomeActivity)context).fileManager_homeActivity.folderFileManager.ReadDirByThread_folder(file);






               com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_fromFolder(context,homeActivity,directoryFav.getID(),directoryFav.getFile(),directoryFav.getFile_name());




            }
        });





        viewHolderFav.slice.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {


                    BottomSheet dialog = new BottomSheet(context,null,directoryFav,1);
                    dialog.show();










                return true;
            }
        });




        viewHolderFav.slice1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {





                BottomSheet dialog = new BottomSheet(context,null,directoryFav,1);
                dialog.show();








                return true;
            }
        });




    }





    @Override
    public int getItemCount() {

        if (Config.TEST)
        {
            Log.d("ITEMS_RECYCLER",  "4 size"+ directories.size());

        }

        return directories.size();
    }


}
