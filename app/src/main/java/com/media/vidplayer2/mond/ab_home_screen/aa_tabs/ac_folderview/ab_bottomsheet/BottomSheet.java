package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.ab_bottomsheet;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler.DirectoryFav;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.media.vidplayer2.mond.zb_Config.Config;

import com.media.vidplayer2.mond.zc_VideoFileParser.VideoFileParser;
import com.media.vidplayer2.mond.zf_space_share.uploadSpace.UploadSpaceActivity;
import com.media.vidplayer2.mond.zi_Play.Player.PlayerActivity;

import com.snatik.storage.Storage;

import java.io.File;
import java.util.ArrayList;

public class BottomSheet extends BottomSheetDialog {

    private Context context;

    BottomSheet bottomSheet;
    int TYPE=0;

    Directory_folder directory_folder=null;
     DirectoryFav directoryFav=null;

    public BottomSheet(@NonNull Context context, Directory_folder directory_folder,    DirectoryFav directoryFav, int TYPE) {
        super(context);
        this.context =context;
        this.directory_folder=directory_folder;
        this.directoryFav=directoryFav;
        bottomSheet =this;
        this.TYPE=TYPE;
    }

    public BottomSheet(@NonNull Context context, int theme) {
        super(context, theme);
    }

    protected BottomSheet(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    String getContentURI(Uri uri)
    {
        String filePath = null;
        long fileSize = 0;
        String displayName = null;
        Cursor c = context.getContentResolver().query(uri, new String[]{MediaStore.MediaColumns.DATA,
                MediaStore.MediaColumns.MIME_TYPE,
                MediaStore.MediaColumns.DISPLAY_NAME,
                MediaStore.MediaColumns.SIZE
        }, null, null, null);
        if (c != null && c.moveToFirst())
        {
            int id = c.getColumnIndex(MediaStore.Images.Media.DATA);
            if (id != -1)
            {
                filePath = c.getString(id);
            }
            displayName = c.getString(2);
            fileSize = c.getLong(3);
        }

        if (c != null && !c.isClosed()) {
            c.close();
        }

        return filePath;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = getLayoutInflater().inflate(R.layout.ag_bottom_sheet, null);
        setContentView(view);

        ArrayList<Item> items=new ArrayList<>();

        if(TYPE==0)
        {
            items.add( new Item(R.drawable.aa_crown, "Delete") );
            items.add( new Item(R.drawable.aa_crown, "Rename") );
            items.add( new Item(R.drawable.aa_crown, "Send to Space Share") );
            items.add( new Item(R.drawable.aa_crown, "Play Video") );


            items.add(new Item(R.drawable.aa_crown, "Share"));


        }
        else
        {


            items.add( new Item(R.drawable.aa_crown, "Delete") );
            items.add( new Item(R.drawable.aa_crown, "Rename") );
            items.add(new Item(R.drawable.aa_crown, "Lock"));
        }


        ItemAdapter adapter = new ItemAdapter( this.context, items );

        //ListView for the items
       ListView listView = (ListView) view.findViewById(R.id.list_items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               // Intent it = null;

                switch(position){
                    case 2:

                          if(Config.DUPLI_TEST)
                          {
                              Log.d("SPACE_SHARE", "onItemClick: ");
                          }

                        Intent i = new Intent(context, UploadSpaceActivity.class);
                        i.putExtra("ID", directory_folder.file_path);
                        i.putExtra("name", directory_folder.file_name);
                        context.startActivity(i);

                        break;
                    case 3:


                        Intent i1 = new Intent(context, PlayerActivity.class);
                        i1.putExtra("index", directory_folder.getIndex());

                        context.startActivity( i1);

                        break;
                    case 0:
                        bottomSheet.dismiss();

                        if(TYPE==0)
                        {

                            /*
                            new Runnable() {
                                @Override
                                public void run()
                                {
                                    ContentResolver contentResolver = context.getContentResolver();
                                    Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);

                                }
                            }
                             */


                            ((HomeActivity)context).fileManager_homeActivity.folderFileManager.Folder_Frag_directory_handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    ContentResolver contentResolver = context.getContentResolver();
                                    Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, directory_folder.getID());
                                    if(Config.DUPLI_TEST)
                                        Log.d("CONTENT_URI", "run: "+deleteUri +"::"+directory_folder.getID());

                                    contentResolver.delete(deleteUri, null, null);
                                }
                            });


                           // new File(directory_folder.file_path).getAbsoluteFile().delete();
                           // context.getContentResolver().delete(Uri.parse(directory_folder.file_path), null, null);


                        //




                            //Storage storage = new Storage(context.getApplicationContext());

                           // storage.deleteFile(directory_folder.file_path);




                            /*
                            File photoLcl = new File(directory_folder.file_path);
                            Uri imageUriLcl = FileProvider.getUriForFile(context,
                                    context.getApplicationContext().getPackageName() +
                                            ".provider", photoLcl);



                            if(Config.DUPLI_TEST)
                            {
                                Log.d("NEW_URI", "onItemClick: "+imageUriLcl);
                            }
                            ContentResolver contentResolver = context.getContentResolver();
                            contentResolver.delete(imageUriLcl, null, null);



*/
/*
https://www.dev2qa.com/android-content-provider-and-contentresolver-example/
https://stackoverflow.com/questions/24659704/how-do-i-delete-files-programmatically-on-android
                       Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);

 */





                              if(Config.DUPLI_TEST)
                              {
                                  Log.d("FILE_PATH_CONTENT", "Content Path: "+getContentURI(Uri.parse(directory_folder.getFile_path()))+"::"+directory_folder.getFile_path()+"::"+Uri.parse(directory_folder.getFile_path()));
                              }





                           VideoFileParser.getInstance().total_folder.remove(directory_folder.index);
                          ( (HomeActivity)context).fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerAdapter.notifyDataSetChanged();


                        }
                        else
                        {



                            ((HomeActivity)context).fileManager_homeActivity.favFileManager.Fav_Frag_directory_handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    ContentResolver contentResolver = context.getContentResolver();
                                    Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, directoryFav.getIDI());
                                    if(Config.DUPLI_TEST)
                                        Log.d("CONTENT_URI", "run: "+deleteUri +"::"+directoryFav.getIDI());

                                    //contentResolver.delete(deleteUri, null, null);

                                    //Storage storage = new Storage(context.getApplicationContext());

                                  //  storage.deleteDirectory(deleteUri.toString());



                                }
                            });

                            /*

                            Storage storage = new Storage(context.getApplicationContext());

                            storage.deleteDirectory(directoryFav.getID());
                            */

                            ( (HomeActivity)context).fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter.directories.remove(directoryFav.getIndex());

                          //  VideoFileParser.getInstance().fa.remove(directory_folder.index);
                            ( (HomeActivity)context).fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter.notifyDataSetChanged();

                        }
                        break;

                    case 1:
                        bottomSheet.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("New Name");

// Set up the input
                        final EditText input = new EditText(context);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                        input.setInputType(InputType.TYPE_CLASS_TEXT );
                        builder.setView(input);

// Set up the buttons
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                               // m_Text = input.getText().toString();


                               if(TYPE ==0)
                               {
                                   TYPE_0(input,position);                               }
                               else
                               {
                                   TYPE_1(input,position);
                               }

                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();

                        break;
                    case 4:

                        bottomSheet.dismiss();

                        ArrayList<Uri> imageUris = new ArrayList<Uri>();
                        imageUris.add(Uri.parse( directory_folder.getFile_path())); // Add your image URIs here

                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
                        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris );
                        shareIntent.setType("video/*");
                        context.startActivity(Intent.createChooser(shareIntent, "Share video to.."));



                        /*
                        CharSequence options[] = new CharSequence[] {"Watsup", "Dropbox", "Google+"};

                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                        builder1.setCancelable(false);
                        builder1.setTitle("Select your option:");
                        builder1.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                // switch statement
                                switch(which)
                                {
                                    // case statements
                                    // values must be of same type of expression
                                    case 0 :
                                        // Statements





                                        break; // break is optional

                                    case 1 :
                                        // Statements
                                        break; // break is optional

                                    // We can have any number of case statements
                                    // below is default statement, used when none of the cases is true.
                                    // No break is needed in the default case.
                                    default :
                                        // Statements
                                }
                                // the user clicked on options[which]
                            }
                        });
                        builder1.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //the user clicked on Cancel
                            }
                        });
                        builder1.show();
                        */

                        break;
                    case 6:

                        break;
                }

              //  context.startActivity(it);
            }
        });

        //GridView for the items
        /*GridView gridView = (GridView) view.findViewById(R.id.grid_items);
        gridView.setAdapter( adapter );
        gridView.setNumColumns(2);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent it = null;

                switch(position){
                    case 0:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.whatsapp.com"));
                        break;
                    case 1:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.facebook.com"));
                        break;
                    case 2:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://plus.google.com"));
                        break;
                    case 3:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.twitter.com"));
                        break;
                    case 4:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.youtube.com"));
                        break;
                    case 5:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.instagram.com"));
                        break;
                    case 6:
                        it = new Intent(Intent.ACTION_VIEW);
                        it.setData(Uri.parse("http://www.stackoverflow.com"));
                        break;
                }

                context.startActivity(it);
            }
        });*/

    }

    void TYPE_0(EditText input,int position)
    {

        String[] parts = directory_folder.file_path.split("/");
        String pathD = directory_folder.file_path;
        String name = parts[parts.length-1];
        String  extDC = name.replace( directory_folder.file_name.substring(0, directory_folder.file_name.lastIndexOf(".")),"");

        if(Config.DUPLI_TEST)
            Log.d("WIFI__TEST", "ID: "+ pathD +"::"+parts[parts.length-1]+"::"+ name +"::"+extDC);










        String newpath = pathD.replace("/"+parts[parts.length-1],"")+"/"+input.getText().toString()+extDC;



        boolean success =false;


        if(Config.DUPLI_TEST){
            Log.d("WIFI__TEST", position+" ID:"+newpath  );
            for(int i=0;i< VideoFileParser.getInstance().total_folder.size();i++)
            {
                Directory_folder directory_folder = VideoFileParser.getInstance().total_folder.get(i);
                Log.d("WIFI__TEST", "pos :"+i + "::" + directory_folder.file_path);
            }
        }

        if (success)
        {
             System.out.println("Couldn't rename file!");

            if(Config.DUPLI_TEST)
                Log.d("CONTENT_URI", "run rename failed: "+  "::"+directory_folder.getID());



        }
        else
        {

            ((HomeActivity)context).fileManager_homeActivity.folderFileManager.Folder_Frag_directory_handler.post(new Runnable() {
                @Override
                public void run()
                {
                    ContentResolver contentResolver = context.getContentResolver();
                    Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, directory_folder.getID());
                    if(Config.DUPLI_TEST)
                        Log.d("CONTENT_URI", "run rename: "+deleteUri +"::"+directory_folder.getID());

                    //  Uri contentUri = Uri.parse("content://....");

                    /*

                    ContentValues contentValues = new ContentValues();

                    contentValues.put("name", input.getText().toString()+extDC);

                    contentResolver.notifyChange(deleteUri, null);

*/


// Defines selection criteria for the rows you want to update
                    String selectionClause =MediaStore.Video.Media.DISPLAY_NAME +  " LIKE ?";
                    String[] selectionArgs = {input.getText().toString()};

                    ContentValues newValues = new ContentValues();
                    int rowsUpdated = 0;
                    /*
                     * Sets the values of each column and inserts the word. The arguments to the "put"
                     * method are "column name" and "value"
                     */


                    newValues.put(MediaStore.Video.Media.DISPLAY_NAME, input.getText().toString()+extDC);



                    rowsUpdated = contentResolver.update(
                            deleteUri,   // the user dictionary content URI
                            newValues,                      // the columns to update
                            null,                   // the column to select on
                            null                      // the value to compare to
                    );


                    if(Config.DUPLI_TEST)

                        Log.d("RENAME_FILE", "uri :"+deleteUri + "::" + rowsUpdated);
                }
            });




            System.out.println("File renamed successfully!");
            Directory_folder directory_folder1 = VideoFileParser.getInstance().total_folder.get(directory_folder.index);

            directory_folder1.setFile_name(input.getText().toString()+extDC);


            //  VideoFileParser.getInstance().total_folder.remove(di );

            VideoFileParser.getInstance().total_folder.set(directory_folder.index,directory_folder1 );
            ( (HomeActivity)context).fileManager_homeActivity.folderFileManager.Folder_Frag_recyclerAdapter.notifyDataSetChanged();

            if(Config.DUPLI_TEST){

                for(int i=0;i< VideoFileParser.getInstance().total_folder.size();i++)
                {
                    Directory_folder directory_folder = VideoFileParser.getInstance().total_folder.get(i);
                    Log.d("WIFI__TEST", "pos :"+directory_folder.index + "::" + directory_folder.file_path);
                }
            }




        }

    }
    void TYPE_1(EditText input,int position)
    {


        Storage storage = new Storage(context.getApplicationContext());

        //storage.rename(directoryFav.getID(),);



        String[] parts = directoryFav.getID().split("/");
        String pathD = directoryFav.getID();

        String newpath = pathD.replace("/"+parts[parts.length-1],"")+"/"+input.getText().toString();


        storage.rename(directoryFav.getID(),newpath);

        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(newpath))));

        DirectoryFav directoryFav1 =  ( (HomeActivity)context).fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter.directories.get(directoryFav.getIndex());

        directoryFav1.dir_name= input.getText().toString();
        directoryFav1.setID(newpath);
        ( (HomeActivity)context).fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter.directories.set(directoryFav.getIndex(),directoryFav);

        //  VideoFileParser.getInstance().fa.remove(directory_folder.index);
        ( (HomeActivity)context).fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter.notifyDataSetChanged();





        if(Config.DUPLI_TEST){

            for(int i=0;i<  ( (HomeActivity)context).fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter.directories.size();i++)
            {
                DirectoryFav directoryFav = ( (HomeActivity)context).fileManager_homeActivity.favFileManager.Fav_Frag_recyclerAdapter.directories.get(i);
                Log.d("WIFI__TEST", "pos :"+directoryFav.getIndex() + "::" + directoryFav.getID());
            }
        }



    }


    private long getRawContactIdByName(String givenName, String familyName)
    {
        ContentResolver contentResolver = context.getContentResolver();

        // Query raw_contacts table by display name field ( given_name family_name ) to get raw contact id.

        // Create query column array.
        String queryColumnArr[] = {ContactsContract.RawContacts._ID,ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY};

        // Create where condition clause.
        String displayName = givenName + " " + familyName;
       // String whereClause = ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY + " = '" + displayName + "'";
        String whereClause =null;
        // Query raw contact id through RawContacts uri.
        Uri rawContactUri = ContactsContract.RawContacts.CONTENT_URI;

        // Return the query cursor.
        Cursor cursor = contentResolver.query(rawContactUri, queryColumnArr, whereClause, null, null);

        long rawContactId = -1;

        if(cursor!=null)
        {
            // Get contact count that has same display name, generally it should be one.
            int queryResultCount = cursor.getCount();
            // This check is used to avoid cursor index out of bounds exception. android.database.CursorIndexOutOfBoundsException
            if(queryResultCount > 0)
            {
                // Move to the first row in the result cursor.
                cursor.moveToFirst();
                // Get raw_contact_id.
                rawContactId = cursor.getLong(cursor.getColumnIndex(ContactsContract.RawContacts._ID));
                String name  =cursor.getString(cursor.getColumnIndex(ContactsContract.RawContacts.DISPLAY_NAME_PRIMARY));

                if(Config.DUPLI_TEST)
                    Log.d("CONTACT_PLAN", "getRawContactIdByName: "+name+"::"+rawContactId);
            }
        }

        return rawContactId;
    }
}
