package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments.ab_bottomsheet;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.MetadataChanges;
import com.google.firebase.firestore.WriteBatch;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ab_home_screen.HomeActivity;
import com.media.vidplayer2.mond.ab_home_screen.aa_tabs.aa_FriendAndGroupFragments.FriendListAdapter;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zb_a_chat_user.FireStoreChat.FirestoreChatActivity;
import com.media.vidplayer2.mond.zb_b_chat_group.FireStoreChat.FirestoreGroupActivity;
import com.media.vidplayer2.mond.zh_friendlist.ModelFriendlist;

import java.util.ArrayList;
import java.util.HashMap;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

public class BottomSheetF extends BottomSheetDialog {

    private Context homeactivity;

    FriendListAdapter.NoteHolder  holder;
    BottomSheetF bottomSheetF;

    Boolean group=false;

    ModelFriendlist memberdata=null;


    //((HomeActivity)context)


    public BottomSheetF(@NonNull Context homeactivity, ModelFriendlist memberdata, Boolean group,FriendListAdapter.NoteHolder holder) {
        super(homeactivity);
        this.homeactivity = homeactivity;
        this.memberdata=memberdata;
        this.group = group;
        this.holder=holder;
        bottomSheetF =this;



    }

    public BottomSheetF(@NonNull Context context, int theme) {
        super(context, theme);
    }

    protected BottomSheetF(@NonNull Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    public void leavegroup(  )
    {



        WriteBatch batch =  FirebaseFirestore.getInstance().batch();





        // Name, email address, and profile photo Url
        String name  =null;
        String email =null ;
        Uri photoUrl =null ;


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
        {
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)
                String providerId = profile.getProviderId();

                // UID specific to the provider
                String uid = profile.getUid();


                // Name, email address, and profile photo Url
                name = profile.getDisplayName();
                email = profile.getEmail();
                photoUrl = profile.getPhotoUrl();



            }
        }




        /*

         Intent chatIntent = new Intent(homeactivity, FirestoreGroupActivity.class);
                            chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                            chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                            chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                            chatIntent.putExtra("current_email", FriendListAdapter.current_email);



                            chatIntent.putExtra("GroupName",memberdata.getName());
                            chatIntent.putExtra("CreaterId",memberdata.getId());
         */




        ////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////


        DocumentReference GroupAdminNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(memberdata.getId()).collection("GroupMembList-"+memberdata.getName()).document(FriendListAdapter.current_id);
        batch.delete(GroupAdminNamesReference);


        //////////////////////////////////////////////////////////////////////





        DocumentReference GroupFriendlistReference = FirebaseFirestore.getInstance().collection("Friends").document(FriendListAdapter.current_id).collection("MyFriends").document(memberdata.getName());;
        //batch.set(GroupFriendlistReference, FrduserMap);

        batch.delete(GroupFriendlistReference);







        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {


                CharSequence text = "Group Removed";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(homeactivity, text, duration);
                toast.show();



            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



            }
        });




    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = getLayoutInflater().inflate(R.layout.ag_bottom_sheet, null);
        setContentView(view);

        ArrayList<Item> items=new ArrayList<>();
        items.add( new Item(R.drawable.aa_crown, "Chat") );
        if(group)
        {


            if(memberdata.getId().equals(FriendListAdapter.current_id))
            {
                items.add(new Item(R.drawable.aa_crown, "Freeze Group"));
            }
            else
            {
                items.add(new Item(R.drawable.aa_crown, "Leave Group"));
            }


        }
        else
        {
            items.add(new Item(R.drawable.aa_crown, "Block User"));
        }









        ItemAdapter adapter = new ItemAdapter(homeactivity, items );

        //ListView for the items
       ListView listView = (ListView) view.findViewById(R.id.list_items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               // Intent it = null;

                switch(position){




                    case 0:
                        bottomSheetF.dismiss();


                        if(group)
                        {



                            Intent chatIntent = new Intent(homeactivity, FirestoreGroupActivity.class);
                            chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                            chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                            chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                            chatIntent.putExtra("current_email", FriendListAdapter.current_email);



                            chatIntent.putExtra("GroupName",memberdata.getName());
                            chatIntent.putExtra("CreaterId",memberdata.getId());

                            homeactivity.startActivity(chatIntent);







                        }
                        else
                        {


                            if(holder.other_user_id == null ||holder.other_user_name == null ||holder.other_user_image == null ||holder.other_user_email == null )
                            {

                                CharSequence text = "Loading info please wait";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(homeactivity, text, duration);
                                toast.show();
                            }
                            else
                            {
                                //  Intent chatIntent = new Intent(FriendListsAcitivty.this, ChatActivity.class);
                                //Intent chatIntent = new Intent(friendListFragment.getActivity(), FirestoreGroupActivity.class);

                                Intent chatIntent = new Intent(homeactivity, FirestoreChatActivity.class);
                                chatIntent.putExtra("current_id", FriendListAdapter.current_id);
                                chatIntent.putExtra("current_name", FriendListAdapter.current_name);
                                chatIntent.putExtra("current_image", FriendListAdapter.current_image);
                                chatIntent.putExtra("current_email", FriendListAdapter.current_email);

                                chatIntent.putExtra("other_user_id", holder.other_user_id);
                                chatIntent.putExtra("other_user_name",holder.other_user_name);
                                chatIntent.putExtra("other_user_image", holder.other_user_image);
                                chatIntent.putExtra("other_user_email", holder.other_user_email);
                                if(Config.DUPLI_TEST)
                                    Log.d("NULL_TEST_CONF", "onClick: "+holder.other_user_id+"::"+holder.other_user_name);


                                homeactivity.startActivity(chatIntent);



                            }







                        }






                        break;

                    case 1:
                        bottomSheetF.dismiss();


                        if(group) {


                            if(memberdata.getId().equals(FriendListAdapter.current_id))
                            {

                                HashMap<String, Object> groupdata = new HashMap<>();

                                //creator id

                                groupdata.put("freeze",  true);


                                WriteBatch batch =  FirebaseFirestore.getInstance().batch();

                                DocumentReference GroupNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(FriendListAdapter.current_id).collection("GroupNames").document(memberdata.getName());
                                batch.update(GroupNamesReference, groupdata);


                                HashMap<String, Object> FrduserMap = new HashMap<>();

                                FrduserMap.put("block",  true);




                                DocumentReference GroupFriendlistReference = FirebaseFirestore.getInstance().collection("Friends").document(FriendListAdapter.current_id).collection("MyFriends").document(memberdata.getName());;
                                batch.update(GroupFriendlistReference, FrduserMap);



                                HashMap<String, Object> FMap = new HashMap<>();

                                FMap.put("group",  true);
                                FMap.put("id",  "groupid");
                                FMap.put("name",  memberdata.getName());

                                DocumentReference Freze = FirebaseFirestore.getInstance().collection("GroupLists").document(FriendListAdapter.current_id).collection("Group_User_Freeze_List").document(memberdata.getName());

                                batch.set(Freze, FMap);


                                batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid)
                                    {


                                        CharSequence text = "Group is freezed. Open Freeze list to unfreeze it again";
                                        int duration = Toast.LENGTH_SHORT;

                                        Toast toast = Toast.makeText(homeactivity, text, duration);
                                        toast.show();



                                    }


                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {



                                    }
                                });


                            }
                            else
                            {
                               leavegroup();
                            }


                        }
                        else
                        {

                            HashMap<String, Object> FrduserMap_1 = new HashMap<>();




                            DocumentReference FriendListReference_1 = FirebaseFirestore.getInstance().collection("Friends").document(FriendListAdapter.current_id).collection("MyFriends").document( holder.other_user_id);

                            DocumentReference FriendListReference_2 = FirebaseFirestore.getInstance().collection("Friends").document( holder.other_user_id).collection("MyFriends").document(FriendListAdapter.current_id);



                            FrduserMap_1.put("block", true);







                            WriteBatch batch =  FirebaseFirestore.getInstance().batch();
                            batch.update(FriendListReference_1, FrduserMap_1);
                            batch.update(FriendListReference_2, FrduserMap_1);



                            HashMap<String, Object> FMap = new HashMap<>();


                            FMap.put("name", FirebaseAuth.getInstance().getCurrentUser().getDisplayName() );



                            FMap.put("group",  false);
                            FMap.put("id",   holder.other_user_id);
                            FMap.put("name",  memberdata.getName());

                            DocumentReference Freze = FirebaseFirestore.getInstance().collection("GroupLists").document(FriendListAdapter.current_id).collection("Group_User_Freeze_List").document(memberdata.getName());

                            batch.set(Freze, FMap);



                            batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid)
                                {


                                    CharSequence text = "User Blocked Visit Blocked list to unblock";
                                    int duration = Toast.LENGTH_SHORT;

                                    Toast toast = Toast.makeText(homeactivity, text, duration);
                                    toast.show();



                                }


                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {



                                }
                            });

                        }



                        break;
                    case 2:

                        bottomSheetF.dismiss();




                        break;

                }


            }
        });



    }

    private void BlockManagement(boolean b) {


        ////////////////////////////////////////////////////////
        HashMap<String, Object> groupmembdata = new HashMap<>();

        groupmembdata.put("blocked",  b);

        WriteBatch batch =  FirebaseFirestore.getInstance().batch();

/*
        DocumentReference GroupAdminNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(memberdata.getCreatorid()).collection("GroupMembList-"+ homeactivity.GroupName).document(memberdata.getId());
        batch.update(GroupAdminNamesReference, groupmembdata);



        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {

                if(b)
                Toasty.error(homeactivity, "User is blocked" ).show();
                else
                    Toasty.success(homeactivity, "User is unblocked" ).show();






            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



            }
        });
*/
    }



    private void AdminPrivilageUpgradtion(boolean b) {

      //  Toasty.success(homeactivity, "1" ).show();

        ////////////////////////////////////////////////////////
        HashMap<String, Object> groupmembdata = new HashMap<>();

        groupmembdata.put("admin",  b);

        WriteBatch batch =  FirebaseFirestore.getInstance().batch();

/*
        DocumentReference GroupAdminNamesReference = FirebaseFirestore.getInstance().collection("GroupLists").document(memberdata.getCreatorid()).collection("GroupMembList-"+ homeactivity.GroupName).document(memberdata.getId());
        batch.update(GroupAdminNamesReference, groupmembdata);



        batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid)
            {

                if(b)
                    Toasty.error(homeactivity, "User is changed to admin" ).show();
                else
                    Toasty.success(homeactivity, "User admin power removed" ).show();






            }


        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {



            }
        });
*/
    }


    public void SendtoSettingsActivity(){





    }

}
