package com.media.vidplayer2.mond.ac_Selector_Reg_or_Login;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.api.services.drive.DriveScopes;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.WriteBatch;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.media.vidplayer2.mond.ad_a_user_Profile.MyUserProfileActivity;
import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.HashMap;



import static android.app.Activity.RESULT_OK;
import static com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1.MY_PERMISSIONS_REQUEST_SEND_SMS;

public class FileManagerLoginAcitivity1 {

    Context context = null;

    LoginActivity1 loginActivity1 =null;

    ListenerRegistration ListenerForFireBaseSignIn=null;


    public String smsMessage = null;

    private DocumentReference UserListReference;
    private   DocumentReference FriendListReference;

    public FileManagerLoginAcitivity1(Context context) {
        this.context = context;

        loginActivity1 =  ((LoginActivity1) context);


    }

    public  void UIAfterLogout(){

        loginActivity1.phno_ver.setVisibility(View.GONE);
        loginActivity1.linearLayout.setVisibility(View.VISIBLE);
        loginActivity1.login.setVisibility(View.VISIBLE);
        loginActivity1.logout.setVisibility(View.GONE);
        loginActivity1.text.setText(Config.LogoutMessage);
    }


    public  void AfterPhoneUI()
    {
        loginActivity1.phno_ver.setVisibility(View.GONE);
        loginActivity1.linearLayout.setVisibility(View.VISIBLE);
        loginActivity1.text.setText("Verified Code and please sign In");
    }

    public  void setToAfterLoginFailedGoogleUI(){

        loginActivity1.phno_ver.setVisibility(View.VISIBLE);
        loginActivity1.linearLayout.setVisibility(View.GONE);
      loginActivity1.progressBar.setVisibility(View.GONE);

 ;
        loginActivity1.text.setText("Google login failed ");
    }



    public  void setToAfterLoginFailurewithPhone(){



        if(Config.DUPLI_TEST)
            Log.d("GOOGLES_16", "Already existing account"+"::"+ loginActivity1.code.getText().toString());

        Config.LogoutMessage = "Your phone is already registered with another gmail account";
        loginActivity1.mAuth.signOut();

        loginActivity1.mGoogleSignInClient.signOut().addOnCompleteListener(loginActivity1,
                task -> UIAfterLogout());
    }


    public  void setToAfterLoginSuccesswithPhoneUI(){

        loginActivity1.phno_ver.setVisibility(View.GONE);
        loginActivity1.linearLayout.setVisibility(View.VISIBLE);
        loginActivity1.progressBar.setVisibility(View.GONE);
        loginActivity1.login.setVisibility(View.GONE);
        loginActivity1.logout.setVisibility(View.VISIBLE);
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(loginActivity1);
        loginActivity1.text.setText("You are login as " +acct.getDisplayName()+ " with " +acct.getEmail());



            Intent intent = new Intent((LoginActivity1)context, MyUserProfileActivity.class);
            ((LoginActivity1)context).startActivity(intent);






        //loginActivity1.onBackPressed();
    }


    void send_sms()
    {


        int random = LoginActivity1.getRandomNumberInRange(1000,9999);
          smsMessage = Integer.toString(random);
        // Set the service center address if needed, otherwise null.
        String scAddress = null;
        // Set pending intents to broadcast
        // when an_b_message sent and when delivered, or set to null.
        PendingIntent sentIntent = null, deliveryIntent = null;
        // Use SmsManager.
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage
                (loginActivity1.ccp.getNumber().toString(), scAddress, smsMessage,
                        sentIntent, deliveryIntent);
    }


    void verify_sms()
    {

        if(smsMessage.equals(loginActivity1.code.getText().toString()))
        {

            AfterPhoneUI();

         //  loginActivity1.googleSignIn();
        }
        else
        {
            loginActivity1.text.setText("Wrong Verified Code");
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {


     //   if(!RESET_PROCESS)
      //  {
            switch (requestCode)
            {
                case LoginActivity1.RC_SIGN_IN:

                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    try
                    {
                        GoogleSignInAccount account = task.getResult(ApiException.class);



                        if (account != null)
                        {
                            if(Config.DUPLI_TEST)
                                Log.d("GOOGLES_1", "sign in name ::"+ account.getDisplayName().toString());

                            firebaseAuthWithGoogle(account);
                        }




                    }
                    catch (ApiException e)
                    {
                        if(Config.DUPLI_TEST)
                            Log.d("GOOGLES_2", "Google sign from google not from FB", e);

                        setToAfterLoginFailedGoogleUI();
                    }

                    break;

                case LoginActivity1.RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION:

                    if (resultCode == RESULT_OK)
                    {
                        if(Config.DUPLI_TEST)
                            Log.d("GOOGLES_7", "drive login success");

                        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(loginActivity1);
                        afterLogInwithDrivePermission(acct);

                    }
                    else
                    {

                        AfterFailedDrive();
                    }
                    break;



            }
       // }







    }






    public void firebaseAuthWithGoogle(GoogleSignInAccount acct)
    {

        if(Config.DUPLI_TEST)
        Log.d("GOOGLES_3", "firebaseAuthWithGoogle id:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

         loginActivity1.mAuth.signInWithCredential(credential)
                .addOnCompleteListener(loginActivity1, task ->
                {
                    if (task.isSuccessful())
                    {
                        loginActivity1.progressBar.setVisibility(View.INVISIBLE);

                        if(Config.DUPLI_TEST)
                        Log.d("GOOGLES_4", "Firebase signInWithCredential:success");



                        if (!GoogleSignIn.hasPermissions(
                                GoogleSignIn.getLastSignedInAccount(loginActivity1),new Scope(DriveScopes.DRIVE_FILE),new Scope(DriveScopes.DRIVE_APPDATA))) {

                            if(Config.DUPLI_TEST)
                                   Log.d("GCASE_5", "Requesting sign-in -- requestSignIn()");


                            GoogleSignIn.requestPermissions(
                                    loginActivity1,
                                    LoginActivity1.RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION,
                                    GoogleSignIn.getLastSignedInAccount(loginActivity1),
                                    new Scope(DriveScopes.DRIVE_FILE),new Scope(DriveScopes.DRIVE_FILE));


                        }
                        else
                        {
                            afterLogInwithDrivePermission(acct);
                        }
                    }
                    else
                    {
                        loginActivity1.progressBar.setVisibility(View.INVISIBLE);

                        if(Config.DUPLI_TEST)
                          Log.d("GOOGLES_6", "signInWithCredential:failure", task.getException());

                         Config.LogoutMessage="Firebase login failed";
                         Toast.makeText(loginActivity1, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                         loginActivity1.logout(null);

                    }
                });

    }


    void afterLogInwithDrivePermission ( GoogleSignInAccount acct)
    {


        if(Config.DUPLI_TEST)
            Log.d("GOOGLES_9", "Drive SignIn Success");


        loginActivity1.mFirestore.collection("UsersList").document(loginActivity1.mAuth.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists())
                    {




                        String mob = documentSnapshot.getString("mob");

                        if(mob.equals(loginActivity1.ccp.getNumber().toString()))
                        {
                            if(Config.DUPLI_TEST)
                                Log.d("GOOGLES_14", "Already existing account");


                            setToAfterLoginSuccesswithPhoneUI();
                        }
                        else
                        {
                            if(Config.DUPLI_TEST)
                                Log.d("GOOGLES_15", "Already existing account"+mob+"::"+ loginActivity1.code.getText().toString());
                            setToAfterLoginFailurewithPhone();
                        }




                    }
                    else
                    {
                        if(Config.DUPLI_TEST)
                            Log.d("GOOGLES_10", "firestore has no data for gmail id so checking number next");



                        loginActivity1.mFirestore.collection("UsersList").whereEqualTo("mob", loginActivity1.ccp.getNumber().toString()).get()
                                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                    @Override
                                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {


                                        if (queryDocumentSnapshots.size() > 0) {


                                            if(Config.DUPLI_TEST)
                                                Log.d("GOOGLES_11", "Firestore has data with same number");

                                            // do something with the data

                                            setToAfterLoginFailurewithPhone();
                                        }
                                        else
                                        {




                                            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                                @Override
                                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                                    if (!task.isSuccessful()) {

                                                        if(Config.DUPLI_TEST)
                                                            Log.d("IDCASE", "getInstanceId failed", task.getException());
                                                        return;
                                                    }

                                                    // Get new Instance ID token
                                                    String  token = task.getResult().getToken();


                                                    HashMap<String, Object> userMap = new HashMap<>();

                                                    userMap.put("mob",  loginActivity1.ccp.getNumber().toString());
                                                    userMap.put("id",  loginActivity1.mAuth.getCurrentUser().getUid());
                                                    userMap.put("status", "Hi there I'm using Mond App.");
                                                    userMap.put("thumb_image", "default");
                                                    userMap.put("online", true);
                                                    userMap.put("time_stamp",   FieldValue.serverTimestamp());
                                                    userMap.put("device_token", token);
                                                    userMap.put("email", loginActivity1.mAuth.getCurrentUser().getEmail());
                                                    userMap.put("name", loginActivity1.mAuth.getCurrentUser().getDisplayName());


                                                    HashMap<String, Object> FrduserMap = new HashMap<>();


                                                    FrduserMap.put("id",  loginActivity1.mAuth.getCurrentUser().getUid());
                                                    FrduserMap.put("block",  false);




                                                    String user_id = loginActivity1.mAuth.getCurrentUser().getUid();

                                                    WriteBatch batch =  FirebaseFirestore.getInstance().batch();
                                                    UserListReference = loginActivity1.mFirestore.collection("UsersList").document(user_id);
                                                     FriendListReference = loginActivity1.mFirestore.collection("Friends").document(user_id).collection("MyFriends").document(user_id);


                                                    batch.set(UserListReference, userMap);
                                                     batch.set(FriendListReference, FrduserMap);

                                                    batch.commit().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            setToAfterLoginSuccesswithPhoneUI();

                                                        }


                                                    }).addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {


                                                            setToAfterLoginFailurewithPhone();


                                                        }
                                                    });



                                                    /*






                                                    loginActivity1.mFirestore.collection("UsersList").document(user_id)
                                                            .set(userMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {



                                                            if(Config.DUPLI_TEST)
                                                                Log.d("GOOGLES_12", "New phone added for new gmail in firebase");


                                                            setToAfterLoginSuccesswithPhoneUI();


                                                        }
                                                    });
                                                    */


                                                }
                                            });




                                        }


                                        // ...
                                    }
                                });



                    }
                }
                else
                {
                    if(Config.TEST)
                        Log.d("GSINGIN", "Failed with: ", task.getException());
                }
            }
        });



    }

    void AfterFailedDrive ()
    {
        loginActivity1.mAuth.signOut();

        if(Config.DUPLI_TEST)
            Log.d("GOOGLES_8", "drive login failed");

        Config.LogoutMessage="Drive Login failed";


        loginActivity1.mGoogleSignInClient.signOut().addOnCompleteListener(loginActivity1,
                task -> UIAfterLogout());
    }



    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_SEND_SMS:
            {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(loginActivity1, "SMS sent.",
                            Toast.LENGTH_LONG).show();

                    send_sms();
                } else {
                    Toast.makeText(loginActivity1,
                            "SMS faild, please try again.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }

    }














}
