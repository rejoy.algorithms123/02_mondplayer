package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ab_favorites.aa_Recycler;

public class DirectoryFav {

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    String file_name="directory";
    public Boolean getFile() {
        return file;
    }

    public void setFile(Boolean file) {
        this.file = file;
    }

    public Boolean file =false;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    int index;

    public String dir_name;

    public DirectoryFav(String dir_name, String ID, String video_count) {
        this.dir_name = dir_name;
        this.ID = ID;
        this.video_count = video_count;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String ID;

    public int getIDI() {
        return IDI;
    }

    public void setIDI(int IDI) {
        this.IDI = IDI;
    }

    public int IDI;

    public String getDir_name() {
        return dir_name;
    }

    public void setDir_name(String dir_name) {
        this.dir_name = dir_name;
    }

    public String getVideo_count() {
        return video_count;
    }

    public void setVideo_count(String video_count) {
        this.video_count = video_count;
    }

    public String video_count;
}
