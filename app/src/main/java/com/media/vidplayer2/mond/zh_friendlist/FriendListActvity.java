package com.media.vidplayer2.mond.zh_friendlist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;

import java.util.ArrayList;

public class FriendListActvity extends AppCompatActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
   // private CollectionReference notebookRef = db.collection("Notebook");
   private CollectionReference notebookRef = db.collection("Friends").document(FirebaseAuth.getInstance().getUid()).collection("MyFriends");
    public FriendListAdapter adapter;


   public    boolean searchfriends;
    public ArrayList<ListenerRegistration> listenersUsers;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.al_activity_friendlist);
        listenersUsers = new ArrayList<ListenerRegistration>();







    }


    @Override
    protected void onStop()
    {

        super.onStop();  // Always call the superclass method first

        adapter.stopListening();
        for(int i=0;i<listenersUsers.size();i++)
            if(listenersUsers.get(i)!=null)
                listenersUsers.get(i).remove();


        listenersUsers.clear();





    }

    // friendListActvity.listenersUsers.set(position,listenerRegistration);

    private void setUpRecyclerView() {
        //Query query = notebookRef.orderBy("priority", Query.Direction.DESCENDING);
        Query query = notebookRef.orderBy("time_stamp", Query.Direction.DESCENDING);
        FirestoreRecyclerOptions<ModelFriendlist> options = new FirestoreRecyclerOptions.Builder<ModelFriendlist>()
                .setQuery(query, ModelFriendlist.class)
                .build();

        adapter = new FriendListAdapter(options,this);

        RecyclerView recyclerView = findViewById(R.id.recycler_viewF);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);


/*
          RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int  totalItemCount = layoutManager.getItemCount();
                   firstVisibleItemPosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
                   lasttVisibleItemPosition = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
                int  lastVisibleItem = firstVisibleItemPosition +visibleItemCount;
               /// Toast.makeText(RecyclerViewActivity5.this, "Visible Item Total:"+String.valueOf(visibleItemCount), Toast.LENGTH_SHORT).show();
            }
        };

        recyclerView.addOnScrollListener(onScrollListener);
        */
    }

    @Override
    protected void onStart() {
        super.onStart();

        setUpRecyclerView();
        adapter.startListening();



        EditText edtSearchText = (EditText) findViewById(R.id.edt_search_textf);
        edtSearchText.setVisibility(View.VISIBLE);






        /*search btn clicked*/
        edtSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return false;
            }
        });

        /*hide/show clear button in search view*/



        TextWatcher searchViewTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {



                if(s.toString().trim().length()==0){
                    //  ivClearText.setVisibility(View.GONE);
                } else {
                    //ivClearText.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {



                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if(Config.DUPLI_TEST)
                    Log.d("SEARCH_WISE", "onTextChanged: "+s);
                new  com.media.vidplayer2.mond.zc_VideoFileParser.Util();



                if(s.toString().equals(""))
                {
                     searchfriends=false;
                    if(Config.DUPLI_TEST)
                        Log.d("SEARCH_WISEG", "onTextChanged 1 : "+s);


                    // com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_firstTime(homeActivity,homeActivity);

                }
                else
                {

                    adapter.searchText =s.toString();
                     searchfriends=true;

                    //  com.media.vidplayer2.mond.zc_VideoFileParser.AppKt.fetchImageVideoFolders_firstTimeSearch(homeActivity,homeActivity,s.toString());

                }




                adapter.notifyDataSetChanged();




                //homeActivity.fileManager_homeActivity.folderFileManager.Folder_Frag_updateUIRecyclerK();


            }
        };

        edtSearchText.addTextChangedListener(searchViewTextWatcher);






    }


}
