package com.media.vidplayer2.mond.ab_home_screen.aa_tabs.ac_folderview.aa_Recycler;

import android.net.Uri;

public class Directory_folder {


    public String file_path;
    public String size;
    public int index;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int ID;
    public Boolean getFrom_search() {
        return from_search;
    }

    public void setFrom_search(Boolean from_search) {
        this.from_search = from_search;
    }

    Boolean from_search =false;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    Uri uri;

    public Boolean getHide() {
        return hide;
    }

    public void setHide(Boolean hide) {
        this.hide = hide;
    }

    public Boolean hide =false;

    public Directory_folder(String file_path, String size, int index, String file_name) {
        this.file_path = file_path;
        this.size = size;
        this.index = index;
        this.file_name = file_name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String file_name;

    public Directory_folder(String file_path, String size, String file_name) {
        this.file_path = file_path;
        this.size = size;
        this.file_name = file_name;
    }



}
