package com.media.vidplayer2.mond.zj_Queue.aa_Recycler;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.media.vidplayer2.mond.R;


public class ViewHolderFav extends RecyclerView.ViewHolder {


    public TextView name;
    public TextView file_size;
    public TextView file_progress;

    public LinearLayout slice;
    public LinearLayout slice1;

    public ViewHolderFav(@NonNull View itemView)
    {
        super(itemView);

        name = itemView.findViewById(R.id.name);
        file_size = itemView.findViewById(R.id.file_size);
        file_progress = itemView.findViewById(R.id.file_progress);
        slice  = itemView.findViewById(R.id.slice);
        slice1  = itemView.findViewById(R.id.slice1);
    }
}
