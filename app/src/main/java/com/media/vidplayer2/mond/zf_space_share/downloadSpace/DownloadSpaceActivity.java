package com.media.vidplayer2.mond.zf_space_share.downloadSpace;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.material.snackbar.Snackbar;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.ac_Selector_Reg_or_Login.LoginActivity1;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.ze_LeakManager.IMMLeaks;


import java.util.Collections;
import java.util.Date;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;
import com.media.vidplayer2.mond.zf_space_share.spaceList.ab_bottomsheet.PassFileContainer;

public class DownloadSpaceActivity extends AppCompatActivity {
    public final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 20;
    // 0 -- default  1--direct  2 -- indirect
    public int STATUS_TOKEN =0;

  //  public  static  DownloadSpaceActivity downloadSpaceActivityS=null;



    Boolean insert_Random(Integer val )
    {

        Integer integer=val;

        while(true &&  Config.mapDownload.size()>=0)
        {

            for (Map.Entry<String, Integer> entry : Config.mapDownload.entrySet())
            {

                if(entry.getValue()==integer)
                    return true;
            }

            return  false;

        }
        return  false;

    }

    public double first_time =0;
    @BindView(R.id.text_id_space_d)
    public TextView text_id_space_d;

    @BindView(R.id.progress_circular_space_d)
    public ProgressBar progress_circular_space_d;

    @BindView(R.id.video_thumb_d)
    public ImageView video_thumb_d;

    @BindView(R.id.speed_download)
    public TextView speed_download;

    @BindView(R.id.percentage_size)
    public TextView percentage_size;

    @BindView(R.id.text_id_msg)
    public TextView text_id_msg;



    @BindView(R.id.statD)
    public LinearLayout statD;


    String mime_type;
    File file_to_download=null;

    FileManagerDownloadSpace mDriveServiceHelper;

    public Handler handler;
    @OnClick(R.id.go_home_d)




    public void go_home(View view) {


        onBackPressed();

    }

    Integer tokenVal;

    @BindView(R.id.download_mode_d)
    public CheckBox download_mode_d;

    Boolean direct_download=false;



    @OnClick(R.id.download_d)
    public void download_d(View view)
    {


        getPermission();



    }


    public void spaceshareDownload(File file)
    {
        download_mode_d.setVisibility(View.VISIBLE);

        if( STATUS_TOKEN ==0)
        {
             //ViewCase3_directDownload();
            ViewCase3_IndirectDownload();
        }

        first_time = (double) (new Date().getTime()/1000)%60;
        mDriveServiceHelper.download_file_Case(file,direct_download);



    }



    public void ViewCase1_prior( )
    {

         statD.setVisibility(View.GONE);
        download_mode_d.setChecked(false); //to check


    }


    public void ViewCase2_directDownload( )
    {

        statD.setVisibility(View.VISIBLE);
        speed_download.setVisibility(View.GONE);
        percentage_size.setVisibility(View.GONE);
        text_id_msg.setVisibility(View.GONE);



    }


    public void ViewCase3_IndirectDownload( )
    {

        statD.setVisibility(View.VISIBLE);
        speed_download.setVisibility(View.VISIBLE);
        percentage_size.setVisibility(View.VISIBLE);
        text_id_msg.setVisibility(View.VISIBLE);



    }





    public void getPermission()
    {


        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            spaceshareDownload(file_to_download);

            if(Config.DUPLI_TEST)
            Log.d("_DOWNLOAD_SAMPLE", "1a");

        } else {

            if(Config.DUPLI_TEST)
            Log.d("_DOWNLOAD_SAMPLE", "1b");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

        }






    }

    @Override
    public void onBackPressed() {


        try {
            Config.semaphoreDownload.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        handler.removeCallbacksAndMessages(null);

        Config.deleteHashDownload(tokenVal.toString());
        mDriveServiceHelper.downloadSpaceActivity=null;
        Config.semaphoreDownload.release();



        super.onBackPressed();
    }

    /*


     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.af_activity_download_space);




/*
        PassFileContainer passFileContainer=(PassFileContainer)getIntent().getSerializableExtra("PassFileContainer");

        file_to_download=passFileContainer.getFileSize();
        if(Config.DUPLI_TEST)
            Log.d("_DOWNLOAD_SAMPLE", "Name file"+file_to_download.getName());

*/


        tokenVal =  LoginActivity1.getRandomNumberInRange( 0,100000);
        while (insert_Random(tokenVal))
        {
            tokenVal =  LoginActivity1.getRandomNumberInRange( 0,100000);
        }

        Config.mapDownload.put(tokenVal.toString(),tokenVal);





        ButterKnife.bind(this);





        GoogleAccountCredential credential =
                GoogleAccountCredential.usingOAuth2(
                        getApplicationContext(), Collections.singleton(DriveScopes.DRIVE_FILE));

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());

        credential.setSelectedAccount(acct.getAccount());
        Drive googleDriveService =
                new Drive.Builder(
                        AndroidHttp.newCompatibleTransport(),
                        new GsonFactory(),
                        credential)
                        .setApplicationName("Drive API Migration")
                        .build();



        // The DriveServiceHelper encapsulates all REST API and SAF functionality.
        // Its instantiation is required before handling any onClick actions.
        mDriveServiceHelper = new FileManagerDownloadSpace(googleDriveService, DownloadSpaceActivity.this,tokenVal);



        Looper looper = Looper.getMainLooper();
        handler= new Handler(looper);

        FutureStudioAppGlideModule futureStudioAppGlideModule = new FutureStudioAppGlideModule();


        ViewCase1_prior();




        IMMLeaks.fixFocusedViewLeak(getApplication());




        download_mode_d.setVisibility(View.GONE);


        if(Config.file_id)
        {

            Intent myIntent = getIntent(); // gets the previously created intent
            String image = myIntent.getStringExtra("image");
            String fileid = myIntent.getStringExtra("fileid");
            String filename = myIntent.getStringExtra("filename");
            futureStudioAppGlideModule.showThumbnail(DownloadSpaceActivity.this,video_thumb_d, image);
            text_id_space_d.setText(filename);

            download_mode_d.setVisibility(View.GONE);



            //pass

        }
        else
        {

            file_to_download =   Config.file_parm;

            futureStudioAppGlideModule.showThumbnail(DownloadSpaceActivity.this,video_thumb_d, file_to_download.getWebContentLink());
            text_id_space_d.setText(file_to_download.getName());
            mime_type =file_to_download.getMimeType();
        }


    }





    public void permission_snack_message()
    {


        Snackbar.make(findViewById(android.R.id.content),
                "Needs permission to make inbox folder for downloading from spaceshare",
                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ActivityCompat.requestPermissions(DownloadSpaceActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                        if (Config.DUPLI_TEST) {
                            Log.d("_DOWNLOAD_SAMPLE", "5");
                        }

                    }
                }).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        if(Config.DUPLI_TEST)
            Log.d("_DOWNLOAD_SAMPLE", "onRequestPermissionsResult: HomeActivity -- A");

        switch (requestCode) {


            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:


                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //                    grantResult[0] means it will check for the first_numBytes postion permission which is READ_EXTERNAL_STORAGE
                    //                    grantResult[1] means it will check for the Second postion permission which is CAMERA
                    Toast.makeText(DownloadSpaceActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();

                    if (Config.TEST) {
                        Log.d("_DOWNLOAD_SAMPLE", "6");
                    }




                    spaceshareDownload(file_to_download);

                    if (Config.TEST) {
                        Log.d("_DOWNLOAD_SAMPLE", "7");
                    }


                } else {
                    permission_snack_message();

                }

                break;







        }


        if(Config.DUPLI_TEST)
            Log.d("_DOWNLOAD_SAMPLE", "8");



    }

    public void update_progress_view(double percentRoundOff,double download_speedRoundOff, MediaHttpDownloader downloader,File file)
    {

/*
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), "Message Lol", Toast.LENGTH_SHORT).show();
            }
        });

*/
    }





}
