package com.media.vidplayer2.mond;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.media.vidplayer2.mond.zb_Config.Config;



import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;


public class AppSignatureHelper extends ContextWrapper {

    private static final String TAG = "AppSignatureHelper";
    private static final String HASH_TYPE = "SHA-256";
    public static final int NUM_HASHED_BYTES = 9;
    public static final int NUM_BASE64_CHAR = 11;
    Signature[] signatures;
    PackageInfo packageInfo;
    public AppSignatureHelper(Context context) {
        super(context);
    }




    /**
     * Get all the app signatures for the current package
     */
    //   public ArrayList<String> getAppSignatures_1() {

    public ArrayList<String> getAppSignatures() {
        ArrayList<String> appCodes = new ArrayList<>();

        try {
            // Get all package signatures for the current package
            String packageName = getPackageName();
            PackageManager packageManager = getPackageManager();

/*

            Signature[] signatures = packageManager.getPackageInfo(packageName,
                                      PackageManager.GET_SIGNING_CERTIFICATES).signingInfo.getSigningCertificateHistory();


   */                 //    PackageManager.GET_SIGNATURES).signatures;


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            {
                packageInfo =  packageManager.getPackageInfo(packageName,PackageManager.GET_SIGNING_CERTIFICATES);

                signatures   = packageInfo.signingInfo.getSigningCertificateHistory();
            }
            else
            {
                packageInfo =  packageManager.getPackageInfo(packageName,PackageManager.GET_SIGNATURES);

                signatures   = packageInfo.signatures;
            }
          //  Signature[] signatures = packageInfo.signingInfo.getSigningCertificateHistory();


            // For each signature create a compatible hash
            for (Signature signature : signatures) {
                String hash = hash(packageName, signature.toCharsString());
                if (hash != null) {
                    if(Config.DUPLI_TEST)
                        Log.d("SMS_TEST", "getAppSignatures: "+signature.toByteArray());
                     appCodes.add(String.format("%s", hash));
                   // appCodes.add(String.format("%s", "308201dd30820146020101300d06092a864886f70d010105050030373116301406035504030c0d416e64726f69642044656275673110300e060355040a0c07416e64726f6964310b3009060355040613025553301e170d3139303732323132353631365a170d3439303731343132353631365a30373116301406035504030c0d416e64726f69642044656275673110300e060355040a0c07416e64726f6964310b300906035504061302555330819f300d06092a864886f70d010101050003818d00308189028181008343d8cbd7850b0f4bf150ca54be8235973c6aee0c6f24543a3ad7e3fab58c7b30fea8f9cd6286245d31bae7027eb633fd8b44ec8fa1e062050740237ae5a57369be3e4809445fd6610f9afe7ee1e0cb263fa04e4e8d442a107416367d6642da9140669632950eb6201cdd659225eb7f1d81f504252b8308636829295293fdf90203010001300d06092a864886f70d01010505000381810001a144a5819d2aea985229306545ea6b1b38a9c0736ac80940181eac5125a95f7106012254a75ad5cfc3c74738d102635ecb9ed5ec41aad150f3380080e257cb38e9d5597890bd857b87f75e50d2bc91c1160f194f71dd7ce1961cd79ccbc13e025bc64e2322dbe2d80491597311c6d58cbb0c4066c35539c4a1ae4b50418e9e"));
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Unable to find package to obtain hash.", e);
        }
        return appCodes;
    }

// public ArrayList<String> getAppSignatures()
    public ArrayList<String> getAppSignatures1()
    {
        ArrayList<String> appCodes = new ArrayList<>();

        try {

                final PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNING_CERTIFICATES);
                final Signature[] signatures = packageInfo.signingInfo.getApkContentsSigners();
                final MessageDigest md = MessageDigest.getInstance("SHA");
                for (Signature signature : signatures)
                {
                    md.update(signature.toByteArray());
                    final String signatureBase64 = new String(Base64.encode(md.digest(), Base64.DEFAULT));
                    if (signatureBase64 != null) {
                        appCodes.add(String.format("%s", signatureBase64));
                    }
                    Log.d("Signature Base64", signatureBase64);
                }

        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return appCodes;
    }
    public ArrayList<String> getAppSignatures2()
   // fun getApplicationSignature(packageName: String = context.packageName): List<String>
    {
        ArrayList<String> signatureList = new ArrayList<>();


        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
            {
                PackageManager packageManager = getPackageManager();
                String packageName = getPackageName();
                // New signature
                try {
                    Signature sig[] = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNING_CERTIFICATES).signingInfo.getApkContentsSigners();

                    MessageDigest md;
                    try {
                        md = MessageDigest.getInstance("SHA");
                        for (Signature signature : sig) {
                            md.update(signature.toByteArray());
                            final String signatureBase64 = new String(Base64.encode(md.digest(), Base64.DEFAULT));
                            if (signatureBase64 != null) {
                                signatureList.add(String.format("%s", signatureBase64));
                            }
                            if (Config.DUPLI_TEST)
                                Log.d("Signature Base64", signatureBase64);
                        }
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }


                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                return signatureList;
            }
            else
            {

                PackageManager packageManager = getPackageManager();
                String packageName = getPackageName();
                // New signature
                try {
                    Signature sig[] = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;

                    MessageDigest md;
                    try {
                        md = MessageDigest.getInstance("SHA");
                        for (Signature signature : sig) {
                            md.update(signature.toByteArray());
                            final String signatureBase64 = new String(Base64.encode(md.digest(), Base64.DEFAULT));
                            if (signatureBase64 != null) {
                                signatureList.add(String.format("%s", signatureBase64));
                            }
                            if (Config.DUPLI_TEST)
                                Log.d("Signature Base64", signatureBase64);
                        }
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }


                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }



                return signatureList;
            }



          /*

                signatureList = if (sig.hasMultipleSigners()) {
                    // Send all with apkContentsSigners
                    sig.apkContentsSigners.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        bytesToHex(digest.digest())
                    }
                } else {
                    // Send one with signingCertificateHistory
                    sig.signingCertificateHistory.map {
                        val digest = MessageDigest.getInstance("SHA")
                        digest.update(it.toByteArray())
                        bytesToHex(digest.digest())
                    }
                }
            } else {
                val sig = context.packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures
                signatureList = sig.map {
                    val digest = MessageDigest.getInstance("SHA")
                    digest.update(it.toByteArray())
                    bytesToHex(digest.digest())
                }
            }


        } catch (e: Exception) {
            // Handle error
        }
        return emptyList()
        */

        }
        catch ( Exception e) {
        // Handle error
         }

        return null;
    }

    private static String hash(String packageName, String signature) {
        String appInfo = packageName + " " + signature;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(HASH_TYPE);
            messageDigest.update(appInfo.getBytes(StandardCharsets.UTF_8));
            byte[] hashSignature = messageDigest.digest();

            // truncated into NUM_HASHED_BYTES
            hashSignature = Arrays.copyOfRange(hashSignature, 0, NUM_HASHED_BYTES);
            // encode into Base64
            String base64Hash = Base64.encodeToString(hashSignature, Base64.NO_PADDING | Base64.NO_WRAP);
            base64Hash = base64Hash.substring(0, NUM_BASE64_CHAR);

            Log.d(TAG, String.format("pkg: %s -- hash: %s", packageName, base64Hash));
            return base64Hash;
        } catch (NoSuchAlgorithmException e) {
            Log.e(TAG, "hash:NoSuchAlgorithm", e);
        }
        return null;
    }
}