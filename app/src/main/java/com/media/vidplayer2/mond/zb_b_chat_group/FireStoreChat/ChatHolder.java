package com.media.vidplayer2.mond.zb_b_chat_group.FireStoreChat;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RotateDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.media.vidplayer2.mond.R;
import com.media.vidplayer2.mond.zb_Config.Config;
import com.media.vidplayer2.mond.zc_Glide.FutureStudioAppGlideModule;
import com.media.vidplayer2.mond.zc_Glide.GlideApp;
import com.media.vidplayer2.mond.zj_Queue.TestCase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import es.dmoral.toasty.Toasty;

public class ChatHolder extends RecyclerView.ViewHolder {
    private final TextView mNameField;
    private final TextView timeview;
    private final TextView mTextField;
    private final FrameLayout mLeftArrow;
    private final FrameLayout mRightArrow;
    private final RelativeLayout mMessageContainer;
    private final LinearLayout mMessage;
    private final int mGreen300;
    private final int mGray300;
    private ImageView imageView;

    public FirestoreGroupActivity firestoreGroupActivity, firestoreGroupActivity1;
    long filesize=0;

    MaterialButton add_to_queue;
    MaterialButton go_to_queue;
    View itemView;
    public ChatHolder(@NonNull View itemView ) {
        super(itemView);





        mNameField = itemView.findViewById(R.id.name_text);
        mTextField = itemView.findViewById(R.id.message_text);
        timeview = itemView.findViewById(R.id.time_chat);

        mLeftArrow = itemView.findViewById(R.id.left_arrow);
        mRightArrow = itemView.findViewById(R.id.right_arrow);
        imageView   = itemView.findViewById(R.id.mimage_chat);


        add_to_queue = itemView.findViewById(R.id.add_to_queue);
        go_to_queue = itemView.findViewById(R.id.go_to_queue);
        mMessageContainer = itemView.findViewById(R.id.message_container);
        mMessage = itemView.findViewById(R.id.message);
        mGreen300 = ContextCompat.getColor(itemView.getContext(), R.color.material_green_300);
        mGray300 = ContextCompat.getColor(itemView.getContext(), R.color.material_gray_300);

        this.itemView =itemView;

    }



    public void bind(@NonNull AbstractChat chat)
    {
        setName(chat.getName());

        String pattern = "yyyy-MM-dd HH:mm:ss a";
        SimpleDateFormat simpleDateFormat =new SimpleDateFormat(pattern);
        setMessage(chat.getMessage());





        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();


        if(Config.DUPLI_TEST)
        {
            Log.d("ONLINE_TEST", "position :: " +chat.getMessage());
        }



        if(chat.getTimestamp()!=null)
        {

            DateFormat inputFormatter1 = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = inputFormatter1.parse("2015-2-22");

                DateFormat outputFormatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                String output1 = outputFormatter1.format(chat.getTimestamp()); //
              setTime(output1);

                Log.d("OBJECT_CHAT_NO_NULL", output1+"::::"+String.valueOf(chat));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(chat.getMimage_chat()!=null)
            {


                FutureStudioAppGlideModule futureStudioAppGlideModule = new FutureStudioAppGlideModule();





                  if(Config.DUPLI_TEST)
                      Log.d("CHAT_SOLVER", "firestoreGroupActivity: " + firestoreGroupActivity);


                GlideApp.with(firestoreGroupActivity).load(chat.getMimage_chat()).thumbnail(0.1f)
                        .error(GlideApp.with(firestoreGroupActivity).load(R.drawable.af_thumb_image_missing))
                    .centerCrop()
                    .into(imageView);




                imageView.setVisibility(View.VISIBLE);
                go_to_queue.setVisibility(View.VISIBLE);
                add_to_queue.setVisibility(View.VISIBLE);


                add_to_queue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //your code for the particular button

                        filesize = chat.getFileSize();


                        Config.task_downloadfromGroup(chat);


                        Toasty.success(firestoreGroupActivity, "added to download queue" ).show();






                    }
                });

                go_to_queue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //your code for the particular button

                         filesize = chat.getFileSize();


                        Config.task_downloadfromGroup(chat);


                       Toasty.success(firestoreGroupActivity, "added to download queue and redirect to download queue" ).show();




                        SendtoQueueActivity();

                    }
                });



            }
            else
            {
                imageView.setVisibility(View.GONE);
                go_to_queue.setVisibility(View.GONE);
                add_to_queue.setVisibility(View.GONE);
            }





        }
        else
        {
            Log.d("OBJECT_CHAT_NULL", String.valueOf(chat));

            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_WEEK);
            int year = calendar.get(Calendar.YEAR);
            String[] monthName = {"January", "February",
                    "March", "April", "May", "June", "July",
                    "August", "September", "October", "November",
                    "December"};

            Calendar cal = Calendar.getInstance();
            String month = monthName[cal.get(Calendar.MONTH)];


            String day_year ="";

            switch (day) {
                case Calendar.SUNDAY:
                    // Current day is Sunday
                              //  2019/05/09 08:19:32
                    day_year = "Sunday" +"/"+ year+"/"+"month";
                    break;
                case Calendar.MONDAY:
                    // Current day is Monday
                    day_year = "Monday" +"/"+ year+"/"+"month";
                    break;
                case Calendar.TUESDAY:
                    day_year = "Tuesday" +"/"+ year+"/"+"month";
                    // etc.
                    break;

                case Calendar.WEDNESDAY:
                    day_year = "Wednesday" +"/"+ year+"/"+"month";
                    // etc.
                    break;

                case Calendar.THURSDAY:
                    day_year = "Thursday" +"/"+ year+"/"+"month";
                    // etc.
                    break;

                case Calendar.FRIDAY:
                    day_year = "Friday"+"/"+ year+"/"+"month";
                    // etc.
                    break;

                case Calendar.SATURDAY:
                    day_year = "Saturday"+"/"+ year+"/"+"month";
                    // etc.
                    break;
            }

            Date date=new Date();


            DateFormat outputFormatter1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            String output1 = outputFormatter1.format(date); //
            setTime(output1);



        }

      setIsSender(currentUser != null && chat.getUid().equals(currentUser.getUid()),false);







    }

    private void setName(@Nullable String name) {
        mNameField.setText(name);
    }
    private void setTime(@Nullable String time) {
        timeview.setText(time);
        timeview.setAlpha(1.0f);
    }

    private void setMessage(@Nullable String text) {
        mTextField.setText(text);
    }

    private void setIsSender(boolean isSender,Boolean go_both) {
          int color = mGreen300;

        if(!go_both){


            if (isSender) {

                mLeftArrow.setVisibility(View.GONE);

                if(!go_both)
                    mRightArrow.setVisibility(View.VISIBLE);
                else
                    mRightArrow.setVisibility(View.GONE);

                mMessageContainer.setGravity(Gravity.END);
            } else {
                color = mGray300;

                if(!go_both)
                    mLeftArrow.setVisibility(View.VISIBLE);
                else
                    mLeftArrow.setVisibility(View.GONE);


                mRightArrow.setVisibility(View.GONE);
                mMessageContainer.setGravity(Gravity.START);
            }





        }



        ((GradientDrawable) mMessage.getBackground()).setColor(color);
        ((RotateDrawable) mLeftArrow.getBackground()).getDrawable()
                .setColorFilter(color, PorterDuff.Mode.SRC);
        ((RotateDrawable) mRightArrow.getBackground()).getDrawable()
                .setColorFilter(color, PorterDuff.Mode.SRC);

    }

    public void SendtoQueueActivity(){

        if(Config.DUPLI_TEST)
            Log.d("FLIST", "Friend list 1");


        //  Intent intent = new Intent((HomeActivity)context, FriendListsAcitivty.class);
        Intent intent = new Intent(firestoreGroupActivity, TestCase.class);
        firestoreGroupActivity.startActivity(intent);



    }

}
